# Amplify

Before anything else or after, please install this dependency for Angular to work:

`npm install -g @angular/cli`

This is an [Angular](https://angular.io) Project Version 5.2.10 and was generated with [Angular CLI](https://github.com/angular/angular-cli) Version 1.7.4.

## Installation

Clone the repository anywhere and follow these commands to get started:

`cd Amplify`

`npm install`

`npm update`

If it doesn'nt work try to remove the node modules and install a clean copy of all the modules:

`cd Amplify`

`rm -rf node_modules/`

`npm install`

`npm update`

And you're now good to go and start the development!! God Bless!!

## Development Server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/` (If this port is used already, Angular will automatically generate a random port for you).

**RECOMMENDED:** Run `ng-serve --aot` for better error-handling while doing the development.

The app will automatically reload if you change any of the source files.

## Code Scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

For more information, refer to the documentation [Angular CLI Tools](https://github.com/angular/angular-cli)

## Build/Production

Run `ng build --prod` to build the project for production. The build artifacts will be stored in the `dist/` directory.

**RECOMMENDED:** Run `ng build --prod --build-optimizer` to further reduce bundle sizes of files and optimized more.

For more info, refer to the documentation [Angular Deployment](https://angular.io/guide/deployment)

## Running Unit Tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running End-to-End Tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further Help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

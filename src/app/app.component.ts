import { Component, OnInit } from '@angular/core';

import { UserService } from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private _user: UserService) { }

  ngOnInit() {

  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
// App
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
// LAYOUTS
import { LayoutsModule } from './layouts/layouts.module';
// Guards
import { AuthGuard, NoAuthGuard } from './guards';
// Services
import { ServicesModule } from './services/services.module';
// Modals
import { ModalsModule } from './modals/modals.module';

// On-demand-loaded Module
import { LoginModule } from './pages/login/login.module';
import { AppLoadService } from './services/app-load.service';


export function initApp(appLoad: AppLoadService) {
  return () => appLoad.initApp();
}

// HTTP Interceptors
import { HttpInterceptorProviders } from './http-interceptors';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    ServicesModule,
    LoginModule,
    ModalsModule,
    LayoutsModule
  ],
  providers: [
    // AppLoadService,
    // { provide: APP_INITIALIZER, useFactory: initApp, deps: [AppLoadService], multi: true },
    AuthGuard,
    NoAuthGuard,
    HttpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

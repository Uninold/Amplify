import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, NoAuthGuard } from './guards';
import { LANDING_ROUTES } from './routes/landing.routes';
import { COMMON_ROUTES } from './routes/common.routes';
import { LoginComponent } from './pages/login/login.component';

// APP ROUTES
export const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  LANDING_ROUTES,
  COMMON_ROUTES,
  { path: 'login', component: LoginComponent, canActivate: [NoAuthGuard] },
  { path: '**', redirectTo: '' }
];

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportChartComponent } from './report-chart/report-chart.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ReportChartComponent
  ],
  exports: [
    ReportChartComponent
  ]
})
export class ChartsModule { }

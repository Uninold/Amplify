import {
  Component,
  AfterViewInit,
  ViewEncapsulation,
  Input,
  Output,
  ViewChild,
  ElementRef,
  OnChanges,
  EventEmitter,
  SimpleChanges
} from '@angular/core';
import { chart } from 'highcharts';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-report-chart',
  template: `<div #reportChartTarget></div>`,
  styles: [],
  encapsulation: ViewEncapsulation.Emulated
})
export class ReportChartComponent implements AfterViewInit, OnChanges {

  chart: Highcharts.ChartObject;
  options: Object;

  @ViewChild('reportChartTarget') reportChart: ElementRef;
  @Input() height: number;
  @Input() month: string;
  @Input() requestMedicines;
  @Input() releasedMedicines;
  temp: any;
  colors = [ '#1abc9c', '#2ecc71', '#3498db', '#34495e', '#f1c40f', '#e67e22', '#e74c3c', '#22a6b3', '#ff7979' ];

  constructor() { }

  ngAfterViewInit() {
    this.initializeChart();
  }

  initializeChart(): void {
    if (typeof this.chart === 'undefined') {
      this.options = {
        noData: 'No data',
        chart: {
          type: 'spline',
          backgroundColor: 'transparent',
          plotBorderColor: '#ff0000',
          height: this.height
        },
        credits: { enabled: false },
        legend: { enabled: false },
        exporting: { enabled: false },
        tooltip: {
          crosshairs: false,
          split: true
        },
        title: {
          text: ' '
        },
        subtitle: {
          text: ''
        },
        xAxis: {
          title: {
          },
          categories: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
            19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
        },
        yAxis: {
          title: {
            text: ''
          },
          gridLineColor: 'rgba(100,100,100,.3)',
        },
        plotOptions: {
          spline: {
            dataLabels: {
              enabled: false
            },
            marker: {
              fillColor: '#ffffff',
              lineColor: '#cff381',
              lineWidth: 2,
              radius: 3
            },
            enableMouseTracking: true
          }
        },
      //   series: [{
      //     name: 'Views',
      //     connectNulls: true,
      //     color: '#00cc99',
      //     marker: {
      //       lineColor: '#00cc99'
      //     },
      //     // data: [this.requestMedicines]
      //     data : [
      //       [1112832000000, 43.56],
      //     [1112918400000, 43.74],
      //     [1113177600000, 41.92],
      //     [1113264000000, null],
      //     [1113350400000, null],
      //     [1113436800000, 37.26],
      //     [1113523200000, 35.35],
      //     [1113782400000, 35.62],
      //     [1113868800000, 37.09],
      //     [1113955200000, 35.51],
      //     [1114041600000, 37.18],
      //     [1114128000000, 35.50],
      //     [1114387200000, 36.98],
      //     [1114473600000, 36.19],
      //     [1114560000000, 35.95],
      //     [1114646400000, 35.54],
      //             [1114732800000, 36.06]
      //             ],
      //             tooltip: {
      //               valueDecimals: 2
      //           }
      //   }
      //   // , {
      //   //   name: 'Likes',
      //   //   connectNulls: true,
      //   //   color: 'rgb(0, 191, 199)',
      //   //   marker: {
      //   //     lineColor: 'rgb(0, 191, 199)',
      //   //     symbol : 'circle'
      //   //   },
      //   //   data: [this.releasedMedicines]
      //   // }
      // ]
      };
      Highcharts.setOptions({
        lang: {
            thousandsSep: ',',
            noData: 'No data to display'
        }
    });
      this.chart = chart(this.reportChart.nativeElement, this.options);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('month', this.month);
    this.initializeChart();
    for (const prop in changes) {
      if (changes.hasOwnProperty(prop)) {
        const change = changes[prop];
        if (change.previousValue !== change.currentValue) {
          switch (prop) {
            case 'height':
              break;
            case 'month':
              break;
            case 'requestMedicines':
              this.temp = JSON.parse(this.requestMedicines);
              console.log('REPORT CHART: ', this.requestMedicines);
              while (this.chart.series.length > 0) {
                this.chart.series[0].remove(true);
              }
              if (this.temp.length !== 0) {
                for (const tem of this.temp) {
                  this.chart.addSeries({
                    name: 'Youtuber: <b>' + tem.youtuber  + '</b> Views',
                     connectNulls: true,
                    // color: '#00cc99',
                    color: tem.color,

                    marker: {
                      // lineColor: '#00cc99'
                      lineColor:  tem.color
                    },
                    data: tem.count
                  });
                }
                this.temp = JSON.parse(this.releasedMedicines);
                // this.chart.addSeries({
                //   name: 'Likes',
                //   color: 'rgb(0, 191, 199)',
                //   marker: {
                //     lineColor: 'rgb(0, 191, 199)'
                //   },
                //   data: this.temp
                // });
              }

              break;
            case 'releasedMedicines':
              // this.temp = JSON.parse(this.diastolic);
              // if (this.temp.length !== 0) {
              //   this.chart.series[1].setData(this.temp, true);
              // }
              // console.log('TEMP: ', this.temp);
              break;
            default:
              break;
          }
        }
      }
    }
  }

}

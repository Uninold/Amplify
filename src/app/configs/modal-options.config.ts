import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

export const REGISTRATION_OPTIONS: NgbModalOptions = {
  windowClass: 'registration-modal bs-modal-lg animated slideInUp'
};

export const MEDIAKIT_OPTIONS: NgbModalOptions = {
  windowClass: 'mediakit-modal bs-modal-lg animated slideInUp'
};

export const BUSINESSMAN_RATING_OPTIONS: NgbModalOptions = {
  windowClass: 'businessman-rating-modal bs-modal-lg animated slideInUp'
};

export const VIEW_PROFILE_OPTIONS: NgbModalOptions = {
  windowClass: 'view-profile-modal bs-modal-lg animated slideInUp'
};

export const INTEREST_MESSAGE_OPTIONS: NgbModalOptions = {
  windowClass: 'interest-message-modal bs-modal-lg animated slideInUp'
};

export const YOUTUBER_VIEW_PROFILE_OPTIONS: NgbModalOptions = {
  windowClass: 'mediakit-modal bs-modal-lg animated slideInUp'
};

export const REOPEN_CAMPAIGN_OPTIONS: NgbModalOptions = {
  windowClass: 'reopen-campaign-modal bs-modal-lg animated slideInUp'
};

export const CONFIGURE_VIDEO_WEIGHT_OPTIONS: NgbModalOptions = {
  windowClass: 'configure-video-weight-modal bs-modal-lg animated slideInUp'
};

export const CONFIGURE_BUSINESSMAN_RATING_WEIGHT_OPTIONS: NgbModalOptions = {
  windowClass: 'configure-businessman-rating-weight-modal bs-modal-lg animated slideInUp'
};

export const ADD_CAMPAIGN_OPTIONS: NgbModalOptions = {
  windowClass: 'add-campaign-modal bs-modal-lg animated slideInUp',
  size: 'lg'
};

export const EDIT_CAMPAIGN_OPTIONS: NgbModalOptions = {
  windowClass: 'edit-campaign-modal bs-modal-lg animated slideInUp',
  size: 'lg'
};

export const CAMPAIGN_DETAILS_OPTIONS: NgbModalOptions = {
  windowClass: 'campaign-details-modal bs-modal-lg animated slideInUp',
  size: 'lg'
};

export const SUBMIT_CAMPAIGN_OPTIONS: NgbModalOptions = {
  windowClass: 'submit-campaign-modal bs-modal-lg animated slideInUp',
  size: 'lg'
};

export const OPEN_VIDEO_OPTIONS: NgbModalOptions = {
  windowClass: 'open-video-modal bs-modal-lg animated slideInUp',
  size: 'lg'
};

export const OPEN_VIDEO_PROFILE_OPTIONS: NgbModalOptions = {
  windowClass: 'open-video-profile-modal bs-modal-lg animated slideInUp',
  size: 'lg'
};

export const MEDIAKIT_MODAL_OPTIONS: NgbModalOptions = {
  windowClass: 'submit-campaign-modal bs-modal-lg animated slideInUp',
  size: 'lg',
  backdrop : 'static',
  keyboard : false
};
export const REFERAL_OPTIONS: NgbModalOptions = {
  windowClass: 'referal-modal bs-modal-lg animated slideInUp'
};

export class ServerConfig {
    public static get API(): string {
        // return 'http://localhost/Amplify-API/api/';
        return 'http://amplify.smartstart.us/api/';
    }
    public static get YoutubeDataAPI(): string {
        return 'https://www.googleapis.com/youtube/v3/';
    }
}


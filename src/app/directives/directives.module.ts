import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputFocusDirective } from './index';

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    InputFocusDirective
  ],
  declarations: [
    InputFocusDirective
  ]
})
export class DirectivesModule { }

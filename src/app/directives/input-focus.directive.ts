import { Directive, ElementRef, HostListener } from '@angular/core';

declare var $: any;

@Directive({
  selector: '[appInputFocus]'
})
export class InputFocusDirective {

  constructor(private el: ElementRef) { }

  @HostListener('focus') onFocus() {
    $(this.el.nativeElement).closest('.form-group').addClass('input-has-value');
  }

  @HostListener('blur') onBlur() {
    if (this.el.nativeElement.value.length > 0) {
      $(this.el.nativeElement).closest('.form-group').addClass('input-has-value');
    } else {
      $(this.el.nativeElement).closest('.form-group').removeClass('input-has-value');
    }
  }

}

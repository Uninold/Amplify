import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthServiceL } from '../services';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private _auth: AuthServiceL,
    private _router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const isUserAuthorized = this._auth.isUserAuthorized();
    if (!isUserAuthorized) {
      this._router.navigate(['/login']);
    }
    return isUserAuthorized;
  }

}

import * as storage from 'store';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthServiceL } from '../services';

@Injectable()
export class NoAuthGuard {
  // implements CanActivate
  constructor(
    private _auth: AuthServiceL,
    private _router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const notAuthorized = !this._auth.isUserAuthorized();
    if (!notAuthorized) {
      const authData = storage.get('authData');
      if(authData.user === 'admin') {
        this._router.navigate(['/admin']);
      } else if(authData.user === 'youtuber') { // If youtuber
        this._router.navigate(['/youtuber-dashboard']);
      } else { // If businessman
        this._router.navigate(['/dashboard']);
      }
    }
    return notAuthorized;
  }
}

import { HttpRequest } from '@angular/common/http';

export class CacheHelper {

  public static isCachable(req: HttpRequest<any>): boolean {
    return req.method === 'GET';
  }

}

import { HttpParams } from '@angular/common/http';

export class RequestHelper {

  public static Format(data: any): string {
    const params = new HttpParams();
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        if (typeof (data[key]) === 'object') {
          data[key] = JSON.stringify(data[key]);
        }
        params.set(key, data[key]);
      }
    }
    const param = params.toString();
    return param;
  }

}

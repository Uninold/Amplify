import { ReflectiveInjector } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

import { ResponseCallbackModel } from '../models';

export class ResponseHelper {

  public static Format(response: HttpResponse<any>, callback: any): void {
    let _response: ResponseCallbackModel = {
      error: true,
      data: null,
      message: null
    };
    try {
      if (response.status === 200) {
        if (typeof response.body.data !== 'undefined') {
          _response = <ResponseCallbackModel>{
            error: !!response.body.error,
            data: response.body.data,
            message: response.body.message,
          };
        } else {
          _response = <ResponseCallbackModel>{
            error: false,
            data: response.body,
            message: '',
          };
        }
      }
    } catch (e) {
      _response.message = 'Snap! Server Error.';
    }
    callback(_response);
  }

  public static Log(log: any): void {
    console.log(log);
  }

}

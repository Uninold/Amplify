import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AuthServiceL } from '../services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private _auth: AuthServiceL) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.headers.get('noauth') === 'true') {
      req.headers.delete('noauth');
      return next.handle(req);
    }
    let headers = new HttpHeaders();
    if (this._auth.isUserAuthorized()) {
      headers = new HttpHeaders({
        'X-Requested-With': 'XMLHttpRequest', 
        'Authorization': `Bearer ${this._auth.token}`
      });
    } else {
      headers = new HttpHeaders({
        'X-Requested-With': 'XMLHttpRequest',
        // tslint:disable-next-line:max-line-length
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMSIsInVzZXJfZ3JvdXBfaWQiOiIxIiwiaWRlbnRpdHkiOiJzbWFydF9hZG1pbiIsImlhdCI6MTUxNzQ2MDU5NywibmJmIjoxNTE3NDYwNTk3fQ.gZmAty2lIvOSzHy5G3-w825wjiuWwHc0azbIS5wxJX8'
      });
    }
    // Clone the request and replace the original headers with cloned headers.
    const authReq = req.clone({ headers });
    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }
}

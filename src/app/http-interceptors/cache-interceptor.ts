import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/util/pipe';
import { startWith, tap } from 'rxjs/operators';
import { HttpCacheService } from '../services';
import { CacheHelper } from '../helpers';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {

  constructor(
    private _cache: HttpCacheService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!CacheHelper.isCachable(req)) {
      return next.handle(req);
    }
    const cachedResponse = this._cache.get(req) || null;
    const results = next.handle(req).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          this._cache.put(req, event);
        }
      })
    );
    return cachedResponse ? results.pipe(startWith(cachedResponse)) : results;
  }

  /**
  * Get server response observable by sending request to `next()`.
  * Will add the response to the cache on the way out.
  */
  private _sendRequest(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap(event => {
        // There may be other events besides the response.
        if (event instanceof HttpResponse) {
          this._cache.put(req, event); // Update the cache.
        }
      })
    );
  }


}

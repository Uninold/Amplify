import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import { NotificationService } from '../services';

@Injectable()
export class NotificationInterceptor implements HttpInterceptor {

  constructor(private _notification: NotificationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (req.method !== 'GET') {
            switch (event.body.error) {
              case 0:
                this._notification.success(event.body.message);
                break;
              case 1:
                this._notification.error(event.body.message);
                break;
              default:
                break;
            }
          }
          if (req.method !== 'GET' && event.body.error === 1) {
            this._notification.error(event.body.message);
          }
        }
        return event;
      })
      .catch((err: any, caught) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 403) {
            this._notification.error('Oops! The page you requested is forbidden.');
          } else if (err.status === 500) {
            this._notification.error('Snap! Server error.');
          } else {
            this._notification.error(err.message);
          }
          return Observable.throw(err);
        }
      });
  }
}

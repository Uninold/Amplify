import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';

import { CommonHeaderComponent } from './header/header.component';
import { CommonFooterComponent } from './footer/footer.component';

import { CommonLayoutComponent } from './common.layout';
import { YoutuberDashboardModule } from '../../pages/home/youtuber-dashboard/youtuber-dashboard.module';
import { HotCampaignsModule } from '../../pages/home/hot-campaigns/hot-campaigns.module';
import { MyCampaignsModule } from '../../pages/home/my-campaigns/my-campaigns.module';
import { YoutuberReportsModule } from '../../pages/home/reports/reports.module';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    YoutuberDashboardModule,
    HotCampaignsModule,
    MyCampaignsModule,
    YoutuberReportsModule,
    PerfectScrollbarModule
  ],
  exports: [
    PerfectScrollbarModule
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  declarations: [
    CommonHeaderComponent,
    CommonFooterComponent,
    CommonLayoutComponent,
  ]
})
export class CommonLayoutModule { }

import * as storage from 'store';
import { Component, OnInit, NgModule, AfterViewInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ResponseCallbackModel } from '../../models/response.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewProfileModal, AddCampaignModal } from './../../modals/businessman';
import { VIEW_PROFILE_OPTIONS, YOUTUBER_VIEW_PROFILE_OPTIONS } from '../../../app/configs/modal-options.config';
import { ADD_CAMPAIGN_OPTIONS } from '../../../app/configs/modal-options.config';

import { Router, RouterLink } from '@angular/router';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { AuthServiceL } from '../../services/auth.service';
import { SocialUser } from 'angularx-social-login';

import { UserBusinessman, NotificationModel } from '../../models';
import { UserService } from '../../services/user.service';
import { CampaignService } from '../../services/campaign';
import { getLocaleMonthNames } from '@angular/common';
import { YoutubeDataAPI } from '../../services/youtube_data_api';
import { YoutuberService } from '../../services/youtuber';
import { NotificationActivityService } from '../../services/notification-activity/notification-activity.service';
import { YoutuberViewProfileModal } from '../../modals/youtuber';

import { CampaignModal } from '../../modals/businessman';
import { CampaignDetailsReferModal, InterestMessageModal } from '../../modals/youtuber';
import { CAMPAIGN_DETAILS_OPTIONS, INTEREST_MESSAGE_OPTIONS } from '../../configs/modal-options.config';
declare var $: any;
declare var moment: any;


@Component({
  selector: 'app-common-layout',
  templateUrl: './common.layout.html',
  styleUrls: ['./common.layout.css']
})
export class CommonLayoutComponent implements OnInit, AfterViewInit {

  currentUser: any;
  currentPage: any;
  google: any;
  menuLists: any;
  page_selected: any;
  user_businessman: UserBusinessman;
  youtuberInfo: any;
  page: any;
  notifnotseen: number;
  notificationList: Array<NotificationModel> = [];
  get_complete_name: string; // User Businessman First name and Last name
  show: boolean;
  constructor(config: NgbDropdownConfig,
    private authService: AuthService,
    private _router: Router,
    private _auth: AuthServiceL,
    private _user: UserService,
    private _campaign: CampaignService,
    private _youtube_data_api: YoutubeDataAPI,
    private _youtuber: YoutuberService,
    private modal: NgbModal,
    private _notificationActivity: NotificationActivityService) {
    this.user_businessman = new UserBusinessman();
  }

  ngOnInit() {
    this.notifnotseen = 0;
    this.show = false;
    this.currentPage = "Dashboard";
    let authData = storage.get('authData');
    if (authData.user === 'youtuber') { // youtuber
      this.currentUser = authData.user;
      this.google = storage.get('current_youtuber');
      this._youtuber.getYoutuberInfoById(this.google.id, (responseMediaKit: ResponseCallbackModel) => {
        if (!responseMediaKit.error && responseMediaKit.data) {
          this.youtuberInfo = responseMediaKit.data;
          this._notificationActivity.getNotification(this.google.id.toString(),
            (responseGet: ResponseCallbackModel) => {
              this.notificationList = responseGet.data;
              console.log('esd', this.notificationList);
              console.log('esd2', responseGet.data);
              for (let notification of this.notificationList) {
                if (notification.seen === '0') {
                  this.notifnotseen++;
                }
                this._campaign.getCampaignDetails(notification.notif_subject, (response: ResponseCallbackModel) => {
                  notification.notif_subject_name = response.data[0].project_name;
                });
              }
            });
        }
      });
    } else if (authData.user === 'businessman') { // businessman
      this.currentUser = authData.user;
      this._user.getCurrent((response: ResponseCallbackModel) => {
        this.user_businessman.profile_picture = response.data.profile_picture;
        this.get_complete_name = response.data.first_name + ' ' + response.data.last_name;
        this.user_businessman.email_address = response.data.email_address;
        this.user_businessman.business_name = response.data.business_name;
        this.user_businessman.businessman_id = response.data.businessman_id;
        storage.set('current_businessman', this.user_businessman);
        this._campaign.getAllOnGoingCampaignOfBusinessman(this.user_businessman.businessman_id.toString(), (responseGet: ResponseCallbackModel) => {
          const campaign_deadline = moment(responseGet.data[1].campaign_deadline).format('LL');
          const month = Number(moment(campaign_deadline).month()) + 1;
          const day = Number(moment(campaign_deadline).date());
          const year = Number(moment(campaign_deadline).year());
        });
        this._notificationActivity.getNotification(this.user_businessman.businessman_id.toString(),
          (responseGet: ResponseCallbackModel) => {
            this.notificationList = responseGet.data;
            for (let notification of this.notificationList) {
              if (notification.seen === '0') {
                this.notifnotseen++;
              }
              this._campaign.getCampaignDetails(notification.notif_subject, (response: ResponseCallbackModel) => {
                notification.notif_subject_name = response.data[0].project_name;
              });
            }
          });
      });
    } else if (authData.user == 'admin') {
      this.currentUser = authData.user;
    }
  }
  ngAfterViewInit() {
  }
  /* Modals */
  openNotif(notification) {
    console.log('notification', notification);
    this._notificationActivity.updateNotification(notification.notif_id);
    if (notification.notif_msg == 'recommended you') {
      this._notificationActivity.getNotification(this.google.id.toString(),
        (responseGet: ResponseCallbackModel) => {
          this.notificationList = responseGet.data;
          console.log('esd', this.notificationList);
          console.log('esd2', responseGet.data);
          this.notifnotseen = 0;
          for (let notification of this.notificationList) {
            if (notification.seen === '0') {
              this.notifnotseen++;
            }
            this._campaign.getCampaignDetails(notification.notif_subject, (response: ResponseCallbackModel) => {
              notification.notif_subject_name = response.data[0].project_name;

            });
          }
        });
      const tempModal = this.modal.open(CampaignDetailsReferModal, CAMPAIGN_DETAILS_OPTIONS);
      tempModal.componentInstance.campaign_id = notification.notif_subject;
    }
    if (notification.notif_msg == 'referred you') {
      this._notificationActivity.getNotification(this.google.id.toString(),
        (responseGet: ResponseCallbackModel) => {
          this.notificationList = responseGet.data;
          console.log('esd', this.notificationList);
          console.log('esd2', responseGet.data);
          this.notifnotseen = 0;
          for (let notification of this.notificationList) {
            if (notification.seen === '0') {
              this.notifnotseen++;
            }
            this._campaign.getCampaignDetails(notification.notif_subject, (response: ResponseCallbackModel) => {
              notification.notif_subject_name = response.data[0].project_name;

            });
          }
        });
      const tempModal = this.modal.open(CampaignDetailsReferModal, CAMPAIGN_DETAILS_OPTIONS);
      tempModal.componentInstance.campaign_id = notification.notif_subject;
    }
    if (notification.notif_msg == 'is interested') {
      this._notificationActivity.getNotification(this.user_businessman.businessman_id.toString(),
        (responseGet: ResponseCallbackModel) => {
          this.notificationList = responseGet.data;
          this.notifnotseen = 0;
          for (let notification of this.notificationList) {
            if (notification.seen === '0') {
              this.notifnotseen++;
            }
            this._campaign.getCampaignDetails(notification.notif_subject, (response: ResponseCallbackModel) => {
              notification.notif_subject_name = response.data[0].project_name;

            });
          }
        });
      let modal = this.modal.open(CampaignModal, ADD_CAMPAIGN_OPTIONS);
      console.log('notif', notification);
      modal.componentInstance.campaign_id = notification.notif_subject;
    }
  }
  openProfile() {
    this.modal.open(ViewProfileModal, VIEW_PROFILE_OPTIONS);
  }

  openYoutuberProfile() {
    let current_youtuber = storage.get('current_youtuber');
    let modal = this.modal.open(YoutuberViewProfileModal, YOUTUBER_VIEW_PROFILE_OPTIONS);
    modal.componentInstance.youtuber_id = current_youtuber.id;
  }

  addCampaign() {
    this.modal.open(AddCampaignModal, ADD_CAMPAIGN_OPTIONS);
  }

  openModal(notification) {
    let modal = this.modal.open(CampaignModal, ADD_CAMPAIGN_OPTIONS);
    console.log('heas', notification.notif_subject);
    modal.componentInstance.campaign_id = notification.notif_subject;
  }

  /* ------ */

  /* Functions */

  goTo(selected) { // For Businessman
    switch (selected) {
      case 'Dashboard': {
        this.currentPage = selected;
        break;
      }
      case 'My Campaigns': {
        // Implementation here
        this.currentPage = selected;
        break;
      }
      case 'Reports': {
        // Implementation here
        this.currentPage = selected;
        break;
      }
      case 'World Rank': {
        // Implementation here
        this.currentPage = selected;
        break;
      }
      case 'Settings': {
        // Implementation here
        this.currentPage = selected;
        break;
      }
      case 'Logout': {
        this._auth.logOut((response: ResponseCallbackModel) => {
          this._router.navigate(['/login']);
        });
        break;
      }
    }
  }

  youtuberGoTo(selected) {
    switch (selected) {
      case 'Dashboard': {
        this.page_selected = selected;
        this.currentPage = selected;
        break;
      }
      case 'Hot Campaigns': {
        this.page_selected = selected;
        this.currentPage = selected;
        break;
      }
      case 'My Campaigns': {
        // Implementation here
        this.page_selected = selected;
        this.currentPage = selected;
        break;
      }
      case 'Reports': {
        // Implementation here
        this.page_selected = selected;
        this.currentPage = selected;
        break;
      }
      case 'World Rank': {
        // Implementation here
        this.page_selected = selected;
        this.currentPage = selected;
        break;
      }
      case 'Settings': {
        // Implementation here
        this.page_selected = selected;
        break;
      }
      case 'Logout': {
        this._auth.logOut((response: ResponseCallbackModel) => {
          this.authService.signOut().then(res => {
            storage.clearAll();
          });
          this._router.navigate(['/login']);
        });
        break;
      }
    }
  }

  /* --------- */
}

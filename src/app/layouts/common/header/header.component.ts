import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserModel } from '../../../models/user.model';
import { AuthServiceL } from '../../../services';
import { ResponseCallbackModel } from '../../../models/response.model';

declare var $: any;

@Component({
  selector: 'app-common-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class CommonHeaderComponent {

  constructor(
  ) { }

}

import { LandingLayoutModule } from './landing.layout.module';

describe('LandingLayoutModule', () => {
  let landingModule: LandingLayoutModule;

  beforeEach(() => {
    landingModule = new LandingLayoutModule();
  });

  it('should create an instance', () => {
    expect(landingModule).toBeTruthy();
  });
});

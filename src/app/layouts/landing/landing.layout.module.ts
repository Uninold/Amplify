import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';

import { LandingHeaderComponent } from './header/header.component';
import { LandingFooterComponent } from './footer/footer.component';
import { LandingLayoutComponent } from './landing.layout';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    LandingHeaderComponent,
    LandingFooterComponent,
    LandingLayoutComponent
  ]
})
export class LandingLayoutModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-layout',
  templateUrl: './landing.layout.html',
  styleUrls: ['./landing.layout.css']
})
export class LandingLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

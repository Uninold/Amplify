import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfigureBusinessmanRatingWeightModal } from './configure-businessman-rating-weight-modal.component';


describe('ConfigureBusinessmanRatingWeightModal', () => {
  let component: ConfigureBusinessmanRatingWeightModal;
  let fixture: ComponentFixture<ConfigureBusinessmanRatingWeightModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureBusinessmanRatingWeightModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureBusinessmanRatingWeightModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

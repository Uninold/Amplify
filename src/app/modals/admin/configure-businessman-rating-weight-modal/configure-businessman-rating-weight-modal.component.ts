import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { UserBusinessman } from '../../../models/businessman.model';
import { BusinessmanService } from '../../../services/businessman';
import { AuthService } from 'angularx-social-login';
import { Router, RouterLink } from '@angular/router';
import { ResponseCallbackModel } from '../../../models';
import { CampaignModel, AdvertisingDetailModel, CampaignPhotoModel } from '../../../models/campaign.model';

import { UserService } from '../../../services/user.service';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
import { AuthServiceL } from '../../../services/auth.service';
import { CampaignService } from '../../../services/campaign';
import { NotificationService } from '../../../services';

declare var $: any;
declare var moment: any;

@Component({
    selector: 'app-configure-businessman-rating-weight-modal',
    templateUrl: './configure-businessman-rating-weight-modal.component.html',
    styleUrls: ['./configure-businessman-rating-weight-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class ConfigureBusinessmanRatingWeightModal implements OnInit {

    engaging: any;
    credibility: any;
    impression: any;
    action_oriented: any;
    significance: any;
    integrated: any;
    brand_service: any;
    brand_innovation: any;
    brand_quality: any;
    criteria: any;
    total: number;

    constructor(
        config: NgbDropdownConfig,
        private authService: AuthService,
        private _notification: NotificationService,
        private _router: Router,
        private _youtuber: YoutuberService,
        private _campaign: CampaignService,
        private _businessman: BusinessmanService,
        private _auth: AuthServiceL,
        private _user: UserService,
        public activeModal: NgbActiveModal) {

    }

    ngOnInit() {
        this._businessman.getAllWeight((responseAllCriteria: ResponseCallbackModel) => {
            this.criteria = responseAllCriteria.data;
            for (let i = 0; i < this.criteria.length; i++) {
                switch (this.criteria[i].criteria_name) {
                    case 'Engaging': this.engaging = +this.criteria[i].criteria_percentage * 100; break;
                    case 'Credibility': this.credibility = +this.criteria[i].criteria_percentage * 100; break;
                    case 'Impression': this.impression = +this.criteria[i].criteria_percentage * 100; break;
                    case 'Action Oriented': this.action_oriented = +this.criteria[i].criteria_percentage * 100; break;
                    case 'Significance': this.significance = +this.criteria[i].criteria_percentage * 100; break;
                    case 'Integrated': this.integrated = +this.criteria[i].criteria_percentage * 100; break;
                    case 'Brand Service': this.brand_service = +this.criteria[i].criteria_percentage * 100; break;
                    case 'Brand Innovation': this.brand_innovation = +this.criteria[i].criteria_percentage * 100; break;
                    case 'Brand Quality': this.brand_quality = +this.criteria[i].criteria_percentage * 100; break;
                }
            }
            this.total = ((this.engaging != null) ? +this.engaging : 0) + ((this.credibility != null) ? +this.credibility : 0) + ((this.impression != null) ? +this.impression : 0) + ((this.action_oriented != null) ? +this.action_oriented : 0) + ((this.significance != null) ? +this.significance : 0) + ((this.integrated != null) ? +this.integrated : 0) + ((this.brand_service != null) ? +this.brand_service : 0) + ((this.brand_quality != null) ? +this.brand_quality : 0) + ((this.brand_innovation != null) ? +this.brand_innovation : 0);
        });
    }

    autoCalculate() {
        this.total = ((this.engaging != null) ? +this.engaging : 0) + ((this.credibility != null) ? +this.credibility : 0) + ((this.impression != null) ? +this.impression : 0) + ((this.action_oriented != null) ? +this.action_oriented : 0) + ((this.significance != null) ? +this.significance : 0) + ((this.integrated != null) ? +this.integrated : 0) + ((this.brand_service != null) ? +this.brand_service : 0) + ((this.brand_quality != null) ? +this.brand_quality : 0) + ((this.brand_innovation != null) ? +this.brand_innovation : 0);
    }

    closeModal() {
        this.activeModal.close();
    }

    configureBusinessmanRatingWeight() {
        let criteria = {
            engaging: +this.engaging,
            credibility: +this.credibility,
            impression: +this.impression,
            action_oriented: +this.action_oriented,
            significance: +this.significance,
            integrated: +this.integrated,
            brand_service: +this.brand_service,
            brand_quality: +this.brand_quality,
            brand_innovation: +this.brand_innovation
        }
        if(this.total == 100) {
            this._businessman.updateWeightBusiness(criteria, (responseExisted: ResponseCallbackModel) => {
                if (responseExisted.message === 'Percentage Weight sucessfully updated!') {
                    this.activeModal.dismiss();
                }
            });
            // for (let i = 0; i < this.criteria.length; i++) {
            //     switch (this.criteria[i].criteria_name) {
            //         case 'Engaging': {
            //             let val = +this.engaging / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //         case 'Credibility': {
            //             let val = +this.credibility / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //         case 'Impression': {
            //             let val = +this.impression / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //         case 'Action Oriented': {
            //             let val = +this.action_oriented / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //         case 'Significance': {
            //             let val = +this.significance / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //         case 'Integrated': {
            //             let val = +this.integrated / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //         case 'Brand Service': {
            //             let val = +this.brand_service / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //         case 'Brand Innovation': {
            //             let val = +this.brand_innovation / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //         case 'Brand Quality': {
            //             let val = +this.brand_quality / 100;
            //             this._businessman.updateWeight(this.criteria[i].criteria_id, val, (responseExisted: ResponseCallbackModel) => {
            //                 if (responseExisted.message !== 'Percentage Weight sucessfully updated') {
            //                     // window.location.reload();
            //                 }
            //             });
            //         }; break;
            //     }

            //     if(i == this.criteria.length - 1) {
            //         this.activeModal.dismiss();
            //     }
            // }
        } else {
            this._notification.warning('Total percentage should not be less than or greater than 100%');
        }
    }
}

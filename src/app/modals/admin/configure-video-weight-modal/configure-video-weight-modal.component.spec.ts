import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureVideoWeightModal } from './configure-video-weight-modal.component';

describe('ConfigureVideoWeightModal', () => {
  let component: ConfigureVideoWeightModal;
  let fixture: ComponentFixture<ConfigureVideoWeightModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureVideoWeightModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureVideoWeightModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

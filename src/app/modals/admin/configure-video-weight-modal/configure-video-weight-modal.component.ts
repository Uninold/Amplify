import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { UserBusinessman } from '../../../models/businessman.model';
import { BusinessmanService } from '../../../services/businessman';
import { AuthService } from 'angularx-social-login';
import { Router, RouterLink } from '@angular/router';
import { ResponseCallbackModel } from '../../../models';
import { CampaignModel, AdvertisingDetailModel, CampaignPhotoModel } from '../../../models/campaign.model';

import { UserService } from '../../../services/user.service';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
import { AuthServiceL } from '../../../services/auth.service';
import { CampaignService } from '../../../services/campaign';
import { NotificationService } from '../../../services';

declare var $: any;
declare var moment: any;

@Component({
    selector: 'app-configure-video-weight-modal',
    templateUrl: './configure-video-weight-modal.component.html',
    styleUrls: ['./configure-video-weight-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class ConfigureVideoWeightModal implements OnInit {

    views_count: any;
    reactions_count: any;
    comments_count: any;
    criteria: any;

    constructor(
        config: NgbDropdownConfig,
        private authService: AuthService,
        private _notification: NotificationService,
        private _router: Router,
        private _youtuber: YoutuberService,
        private _campaign: CampaignService,
        private _businessman: BusinessmanService,
        private _auth: AuthServiceL,
        private _user: UserService,
        public activeModal: NgbActiveModal) {
    }

    ngOnInit() {
        this._businessman.getAllWeight((responseAllCriteria: ResponseCallbackModel) => {
            this.criteria = responseAllCriteria.data;
            for (let i = 0; i <= this.criteria.length; i++) {
                switch (this.criteria[i].criteria_name) {
                    case 'View Count': this.views_count = +this.criteria[i].criteria_percentage * 100; break; // Views Count
                    case 'Reaction Count': this.reactions_count = +this.criteria[i].criteria_percentage * 100; break; // Reactions Count
                    case 'Comment Count': this.comments_count = +this.criteria[i].criteria_percentage * 100; break; // Comments Count
                }
            }
        });
    }

    closeModal() {
        this.activeModal.close();
    }

    configureVideoWeight() {
        let total = +this.views_count + +this.reactions_count + +this.comments_count;
        console.log(total);
        console.log(this.criteria);
        let criteria = {
            views_count: +this.views_count,
            reactions_count: +this.reactions_count,
            comments_count: +this.comments_count
        }
        if(total == 100) {
            this._businessman.updateWeightYoutube(criteria, (responseExisted: ResponseCallbackModel) => {
                if (responseExisted.message === 'Percentage Weight sucessfully updated!') {
                    this.activeModal.dismiss();
                }
            });
        } else {
            this._notification.warning('Total percentage should not be less than or greater than 100%');
        }
    }
}

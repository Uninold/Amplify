import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCampaignModal } from './add-campaign-modal.component';

describe('AddCampaignModal', () => {
  let component: AddCampaignModal;
  let fixture: ComponentFixture<AddCampaignModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

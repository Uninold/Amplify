import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { UserBusinessman } from '../../../models/businessman.model';
import { BusinessmanService } from '../../../services/businessman';
import { AuthService } from 'angularx-social-login';
import { Router, RouterLink } from '@angular/router';
import { ResponseCallbackModel } from '../../../models';
import { CampaignModel, AdvertisingDetailModel, CampaignPhotoModel } from '../../../models/campaign.model';

import { UserService } from '../../../services/user.service';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
import { AuthServiceL } from '../../../services/auth.service';
import { CampaignService } from '../../../services/campaign';

declare var $: any;
declare var moment: any;

@Component({
    selector: 'app-add-campaign-modal',
    templateUrl: './add-campaign-modal.component.html',
    styleUrls: ['./add-campaign-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class AddCampaignModal implements OnInit {

    userBusinessman: UserBusinessman;
    campaign_category: any;
    campaign_photos: any;
    createCampaign: CampaignModel;
    currentPage: any;
    campaign_deadline: any;
    production_deadline: any;
    inputtedTTD: any;
    page = 1; // Pagination in Add Campaign Modal
    productPhoto: any;
    page_selected: any;
    advertising_details: any;

    user_businessman: UserBusinessman;
    get_complete_name: string; // User Businessman First name and Last name

    constructor(
        config: NgbDropdownConfig,
        private authService: AuthService,
        private _router: Router,
        private _youtuber: YoutuberService,
        private _campaign: CampaignService,
        private _businessman: BusinessmanService,
        private _auth: AuthServiceL,
        private _user: UserService,
        public activeModal: NgbActiveModal) {
        this.user_businessman = new UserBusinessman();
        this.createCampaign = new CampaignModel();
        this.campaign_photos = [];
        this.advertising_details = [];
    }

    ngOnInit() {
        this._user.getCurrent((response: ResponseCallbackModel) => {
            console.log(response.data);
            this.user_businessman.profile_picture = 'data:' + response.data.profile_picture;
            this.get_complete_name = response.data.first_name + ' ' + response.data.last_name;
            this.user_businessman.email_address = response.data.email_address;
            this.user_businessman.business_name = response.data.business_name;
            this.user_businessman.businessman_id = response.data.businessman_id;
        });
    }

    closeModal() {
        this.activeModal.close();
    }

    date_convert(deadline: any): string { // Convert date object into string
        const year_converted = (deadline.year).toString();
        let month_converted = deadline.month;
        if (month_converted < 10) {
            month_converted = '0' + (deadline.month).toString();
        } else {
            month_converted = (deadline.month).toString();
        }
        let day_converted = (deadline.day).toString();
        if (day_converted < 10) {
            day_converted = '0' + (deadline.day).toString();
        } else {
            day_converted = (deadline.day).toString();
        }
        return year_converted + '-' + month_converted + '-' + day_converted;
    }

    date_validation(campaign_deadline, production_deadline): boolean {
        const date_now = moment().format('LL');
        const day_now = Number(moment(date_now).date());
        const month_now = Number(moment(date_now).month()) + 1; // January starts at 0
        const year_now = Number(moment(date_now).year());

        // Condition of Date Now
        if(campaign_deadline.year >= year_now){
            if(campaign_deadline.month >= month_now){
                if(campaign_deadline.month == month_now) {
                    if(campaign_deadline.day > day_now) {
                        if (production_deadline.year >= campaign_deadline.year) {
                            if (production_deadline.month >= campaign_deadline.month) {
                                if(production_deadline.month == campaign_deadline.month) {
                                    if (production_deadline.day > campaign_deadline.day) {
                                        return true;
                                    } else {
                                        alert('Production Deadline Day should be greater than Campaign Deadline Day.');
                                        return false;
                                    }
                                } else {
                                    return true;
                                }
                            } else {
                                alert('Production Deadline Month should be greater than Campaign Deadline Month.');
                                return false;
                            }
                        } else {
                            alert('Production Deadline Year should be greater than Campaign Deadline Year.');
                            return false;
                        }
                    } else {
                        alert('Campaign Deadline Day should be greater than this day.');
                        return false;
                    }
                } else {
                    if (production_deadline.year >= campaign_deadline.year) {
                        if (production_deadline.month >= campaign_deadline.month) {
                            if(production_deadline.month == campaign_deadline.month) {
                                if (production_deadline.day > campaign_deadline.day) {
                                    return true;
                                } else {
                                    alert('Production Deadline Day should be greater than Campaign Deadline Day.');
                                    return false;
                                }
                            } else {
                                return true;
                            }
                        } else {
                            alert('Production Deadline Month should be greater than Campaign Deadline Month.');
                            return false;
                        }
                    } else {
                        alert('Production Deadline Year should be greater than Campaign Deadline Year.');
                        return false;
                    }
                }
            } else {
                alert('Month should be greater than or equal to this month.');
                return false;
            }
        } else {
            alert('Year should be greater than or equal to this year.');
            return false;
        }
    }

    addThingsToDo() {
        if (this.inputtedTTD !== undefined) {
            this.advertising_details.push(this.inputtedTTD);
            this.inputtedTTD = '';
        }
    }

    changePage(page) {
        switch (page) {
            case '1':
                $('.page-two').hide();
                $('.page-one').show();
                break;
            case '2':
                $('.page-two').show();
                $('.page-one').hide();
                break;
        }
    }

    removeThingsToDo(thingToDo: any) {
        for (let i = 0; i < this.advertising_details.length; i++) {
            if (this.advertising_details[i] === thingToDo) {
                this.advertising_details.splice(i, 1);
            }
        }
    }

    campaignCategory(): number {
        let category_id = 0;
        switch (this.campaign_category) {
            case 'Comedy': category_id = 1 ; break;
            case 'Drama': category_id = 2 ; break;
            case 'Education': category_id = 3 ; break;
            case 'Entertainment': category_id = 4 ; break;
            case 'Family': category_id = 5 ; break;
            case 'Gaming': category_id = 6 ; break;
            case 'Howto and Styles': category_id = 7 ; break;
            case 'Music': category_id = 8 ; break;
            case 'Sports': category_id = 9 ; break;
            case 'Travel and Events': category_id = 10 ; break;
            case 'People and Blogs': category_id = 11 ; break;
        }
        return category_id;
    }

    addCampaign() {
        this.createCampaign.campaign_status_id = 1; // On Going Campaign
        this.createCampaign.businessman_id = +this.user_businessman.businessman_id;
        this.createCampaign.category_id = this.campaignCategory();
        this.createCampaign.campaign_deadline = this.date_convert(this.campaign_deadline); // CAMPAIGN DEADLINE CONVERSION
        this.createCampaign.production_deadline = this.date_convert(this.production_deadline); // PRODUCTION DEADLINE CONVERSION
        const validation_campaign_and_production = this.date_validation(this.campaign_deadline, this.production_deadline);

        if (this.createCampaign.starting_budget <= this.createCampaign.ending_budget) { // Budget Validation
            if (validation_campaign_and_production) { // Date Validation
                if (this.advertising_details.length !== 0 &&
                    this.createCampaign.project_name !== undefined &&
                    this.createCampaign.project_description !== undefined &&
                    this.createCampaign.campaign_deadline !== undefined &&
                    this.createCampaign.production_deadline !== undefined &&
                    this.createCampaign.advertiser_needed !== null) { // All details validation

                    this._campaign.addCampaign(this.createCampaign, (responseAdd: ResponseCallbackModel) => {
                        if (responseAdd.data) {
                            for (let photo = 0; photo < this.campaign_photos.length; photo++) {
                                const campaignPhoto: CampaignPhotoModel = new CampaignPhotoModel();
                                campaignPhoto.campaign_id = responseAdd.data.campaign_id;
                                campaignPhoto.photo = this.campaign_photos[photo].replace('data:', '');
                                this._campaign.addCampaignPhoto(campaignPhoto, (responseAddPhoto: ResponseCallbackModel) => { });
                                
                                if(photo == this.campaign_photos.length-1) {
                                    for (let j = 0; j < this.advertising_details.length; j++) {
                                        const advertisingDetail: AdvertisingDetailModel = new AdvertisingDetailModel();
                                        advertisingDetail.campaign_id = responseAdd.data.campaign_id;
                                        advertisingDetail.detail = this.advertising_details[j];
                                        this._campaign.addAdvertisingDetail(advertisingDetail, (responseAddDetail: ResponseCallbackModel) => { });
                                    
                                        if(j < this.advertising_details.length-1) {
                                            this._businessman.getCurrentBusinessman((responseBusinessman: ResponseCallbackModel) => {
                                                if (!responseBusinessman.error && responseBusinessman.data) {
                                                    this._campaign.getYoutubersFromCampaign(responseBusinessman.data.businessman_id);
                                                }
                                            });
                                            setTimeout(() => {
                                                window.location.reload();
                                            }, 2000);
                                        }
                                    }
                                }
                            }
                            this.activeModal.dismiss();
                        }
                    });
                } else {
                    alert('Please comply all the details completely with correct validation.');
                }
            } else {
                // alert('Production Deadline should be advance to Campaign Deadline.');
            }
        } else {
            alert('Invalid input of Starting and Ending Budget.');
        }
    }

    removePhoto(photo: any) {
        for (let campaign_photo = 0; campaign_photo < this.campaign_photos.length; campaign_photo++) {
            if (this.campaign_photos[campaign_photo] === photo) {
                this.campaign_photos.splice(campaign_photo, 1);
            }
        }
    }
    /* Upload Photo */

    uploadListener($event) {
        this.readUploadedFile($event.target);
    }

    readUploadedFile(getPicture): void {
        const file: File = getPicture.files[0];
        const fileReader: FileReader = new FileReader();

        fileReader.onloadend = (e) => {
            this.campaign_photos.push(fileReader.result);
            console.log(this.campaign_photos);
        };
        fileReader.readAsDataURL(file);
    }

    /* ------ */

    onKeydown(event) {
        if(event.key === "Enter") {
            this.advertising_details.push(event.target.value);
            this.inputtedTTD = '';
        }
    }
}

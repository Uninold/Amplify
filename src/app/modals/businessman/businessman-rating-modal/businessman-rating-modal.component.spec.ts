import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessmanRatingModal } from './businessman-rating-modal.component';

describe('BusinessmanRatingModal', () => {
  let component: BusinessmanRatingModal;
  let fixture: ComponentFixture<BusinessmanRatingModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessmanRatingModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessmanRatingModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { BusinessmanService } from '../../../services/businessman';
import { Router, RouterLink } from '@angular/router';
import { ResponseCallbackModel, BusinessmanRatingModel } from '../../../models';
import { NotificationService } from '../../../services';
import { CampaignService } from '../../../services/campaign';
import { OpenVideoModal } from '../../youtuber';
import { OPEN_VIDEO_OPTIONS } from '../../../configs/modal-options.config';

declare var $: any;
declare var moment: any;

@Component({
    selector: 'app-businessman-rating-modal',
    templateUrl: './businessman-rating-modal.component.html',
    styleUrls: ['./businessman-rating-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class BusinessmanRatingModal implements OnInit {

    @Input() interested_youtuber: any;
    @Output() done_rating = new EventEmitter<string>();
    businessman_rating: BusinessmanRatingModel;
    video_details: any;

    constructor(
        config: NgbDropdownConfig,
        private _router: Router,
        private _businessman: BusinessmanService,
        private _notification: NotificationService,
        private _campaign: CampaignService,
        public activeModal: NgbActiveModal,
        private modal: NgbModal) {
            this.businessman_rating = new BusinessmanRatingModel();
    }

    ngOnInit() {
        console.log(this.interested_youtuber);
        this._campaign.getYoutuberSubmittedVideo(this.interested_youtuber.youtuber_id, +this.interested_youtuber.campaign_id, (responseVideo: ResponseCallbackModel) => {
            if(!responseVideo.error && responseVideo.data) {
                this.video_details = responseVideo.data;
            }
        });
    }

    closeModal() {
        this.activeModal.close();
    }

    submitRating() {
        this.businessman_rating.accepted_interest_id = this.interested_youtuber.accepted_interest_id;
        if(this.businessman_rating.action_oriented != 0 && this.businessman_rating.brand_innovation != 0 &&
            this.businessman_rating.brand_quality != 0 && this.businessman_rating.brand_service != 0 &&
            this.businessman_rating.credibility != 0 && this.businessman_rating.engaging != 0 &&
            this.businessman_rating.impression != 0 && this.businessman_rating.integrated != 0 &&
            this.businessman_rating.significance != 0) {
                this._businessman.addBusinessmanRating(this.businessman_rating, (responseRating: ResponseCallbackModel) => {
                    // Change status if the youtuber is rated already
                    if(!responseRating.error && responseRating.data) {
                        this._businessman.updateRatingStatus(this.interested_youtuber.accepted_interest_id, 1, (responseUpdateAI: ResponseCallbackModel) => {
                            this.done_rating.emit('Done Rating');
                            setTimeout(() => {
                                this.activeModal.dismiss();
                            }, 1000);
                        });
                    }
                });
            } else {
                this._notification.warning('Lowest Rating should be 1 and the Highest Rating should be 10.');
            }
    }

    openVideo(video_id: any) {
        let modal = this.modal.open(OpenVideoModal, OPEN_VIDEO_OPTIONS);
        modal.componentInstance.video_id = video_id;
    }

}

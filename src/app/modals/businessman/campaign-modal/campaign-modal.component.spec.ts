import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitCampaignModal } from './campaign-modal.component';

describe('SubmitCampaignModal', () => {
  let component: SubmitCampaignModal;
  let fixture: ComponentFixture<SubmitCampaignModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitCampaignModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitCampaignModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

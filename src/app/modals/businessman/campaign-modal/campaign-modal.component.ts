import * as storage from 'store';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { ResponseCallbackModel, ResponseModel, AcceptedInterestModel } from '../../../models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
import { YoutubeDataAPI } from '../../../services/youtube_data_api/youtube_data_api.service';
import { EmbedVideoService } from 'ngx-embed-video';
import { NotificationService } from '../../../services';
import { InterestService } from '../../../services/interest';

declare var $: any;
declare var moment: any;


import { EditCampaignModal } from '../../../modals/businessman';
import { SubmittedModal } from '../../../modals/businessman/submitted-modal/submitted-modal.component';
import { NotificationModel } from '../../../models/notification.model';
import { NotificationActivityService } from '../../../services/notification-activity';
import { INTEREST_MESSAGE_OPTIONS, SUBMIT_CAMPAIGN_OPTIONS, EDIT_CAMPAIGN_OPTIONS, YOUTUBER_VIEW_PROFILE_OPTIONS } from '../../../configs/modal-options.config';

import { SubmitCampaignModal, YoutuberViewProfileModal } from '../../../modals/youtuber';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'campaign-modal',
    templateUrl: './campaign-modal.component.html',
    styleUrls: ['./campaign-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class CampaignModal implements OnInit {
    campaign: any;
    @Input() campaign_id: any;
    activeTab: any;
    acceptedInterest: AcceptedInterestModel;
    acceptedAdvertisersCount: any;
    acceptedToWork: boolean;
    campaign_details: any;
    campaign_photos: any;
    google: any;
    notification = new NotificationModel();
    recommendedYoutuber: any;
    recommendedYoutuber_count: any;
    constructor(
        private modal: NgbModal,
        private _campaign: CampaignService,
        private _interest: InterestService,
        private _notification: NotificationService,
        private _notificationActivity: NotificationActivityService
    ) {
          this.acceptedToWork = false;
          this.acceptedInterest = new AcceptedInterestModel();
          this.recommendedYoutuber = [];
    }

    ngOnInit() {
        console.log('hi',this.campaign_id);
        this._campaign.getSpecificOnGoingCampaignOfBusinessmanWithAds(this.campaign_id, (responseCampaign: ResponseCallbackModel) => {
            this.campaign_details = responseCampaign.data[0];
            this.acceptedAdvertisersCount = this.campaign_details.accepted_advertiser_count;
            
            this._campaign.getRecommendedYoutuber(this.campaign_details.category_id, (responseRecommend: ResponseCallbackModel) => {
              if (responseRecommend.message === 'Recommended Youtuber') {
                console.log(responseRecommend.data);
                this.recommendedYoutuber = responseRecommend.data;
                this.recommendedYoutuber_count = this.recommendedYoutuber.length;
                this._campaign.getPhotoCampaignToFeed(this.campaign_id, (responsePhotos: ResponseCallbackModel) => {
                  this.campaign_photos = responsePhotos.data;
                  console.log(this.campaign_photos);
                });
              }
            });
          });
      }
      instance() {
        this._campaign.getSpecificOnGoingCampaignOfBusinessmanWithAds(this.campaign_id, (responseCampaign: ResponseCallbackModel) => {
          this.campaign_details = responseCampaign.data[0];
          this.acceptedAdvertisersCount = this.campaign_details.accepted_advertiser_count;
          console.log('hi',this.campaign_details);
          this._campaign.getRecommendedYoutuber(this.campaign_details.category_id, (responseRecommend: ResponseCallbackModel) => {
            if(responseRecommend.message === 'Recommended Youtuber') {
              console.log(responseRecommend.data);
              this.recommendedYoutuber = responseRecommend.data;
              this.recommendedYoutuber_count = this.recommendedYoutuber.length;
              this._campaign.getPhotoCampaignToFeed(this.campaign_id, (responsePhotos: ResponseCallbackModel) => {
                this.campaign_photos = responsePhotos.data;
                console.log(this.campaign_photos);
              });
            }
          });
        });
      }
      editCampaign() {
        const editCampaignModal = this.modal.open(EditCampaignModal, EDIT_CAMPAIGN_OPTIONS);
        editCampaignModal.componentInstance.campaign_id = this.campaign_id;
      }
      viewYoutuberProfile(youtuber_id: any) {
        console.log('asd', youtuber_id);
        let modal = this.modal.open(YoutuberViewProfileModal, YOUTUBER_VIEW_PROFILE_OPTIONS);
        modal.componentInstance.youtuber_id = youtuber_id;
      }
      addToProduction() {
        this._campaign.addToOnGoingProduction(this.campaign_id, (responseUpdateStatus: ResponseCallbackModel) => {
          if(responseUpdateStatus.data.error == 0) {
            this._notification.success('Campaign is added to On Going Production.');
            setTimeout(() => {
              window.location.reload();
            }, 1000);
          }
        });
      }
      acceptAdvertiser(advertiser, campaign, index) {
        if (this.acceptedAdvertisersCount < this.campaign_details.advertiser_needed) {
          console.log('acadvertiser', advertiser);
          console.log('accampaign', campaign);
          this.google = storage.get('current_businessman');
                    this.notification.notif_msg = 'accepted your interest';
                    this.notification.notif_from = this.google.businessman_id;
                    this.notification.notif_to =  advertiser.youtuber_id;
                    this.notification.user_img = this.google.profile_picture;
                    this.notification.name = this.google.business_name;
                    this.notification.notif_subject = campaign.campaign_id;
                    this._notificationActivity.notification(this.notification);

          this.acceptedInterest.youtuber_id = advertiser.youtuber_id;
          this.acceptedInterest.campaign_id = +campaign.campaign_id;
          this.acceptedInterest.message = advertiser.message;
          this.acceptedInterest.date_interested = advertiser.date_interested;
          this.acceptedInterest.submission_status_id = 2; // On Going Status
          this._interest.acceptedInterested(this.acceptedInterest, (responseAccept: ResponseCallbackModel) => {
            if (this.campaign_details.advertisers[index].interested === 0) {
              this.campaign_details.advertisers[index].interested = 1;
              ++this.acceptedAdvertisersCount; // Increment Current Accepted Advertisers
            } else {
              this.campaign_details.advertisers[index].interested = 0;
              --this.acceptedAdvertisersCount; // Increment Current Accepted Advertisers
            }
          });
        } else {
          this._notification.warning('You have reached maximum number of advertisers you need.');
        }
      }
      openSubmittedModal(campaign, advertiser) {
        this._campaign.getVideoWithYoutuberInfo(advertiser.youtuber_id, campaign.campaign_id, (response: ResponseCallbackModel) => {
          const submitCampaignModal = this.modal.open(SubmittedModal, SUBMIT_CAMPAIGN_OPTIONS);
          submitCampaignModal.componentInstance.campaign = response.data;
        });
      }

      removeAcceptAdvertiser(advertiser, campaign, index) {
        this.google = storage.get('current_businessman');
                    this.notification.notif_msg = 'removed your interest';
                    this.notification.notif_from = this.google.businessman_id;
                    this.notification.notif_to =  advertiser.youtuber_id;
                    this.notification.user_img = this.google.profile_picture;
                    this.notification.name = this.google.business_name;
                    this.notification.notif_subject = campaign.campaign_id;
                    this._notificationActivity.notification(this.notification);
        this.acceptedInterest.youtuber_id = advertiser.youtuber_id;
        this.acceptedInterest.campaign_id = +campaign.campaign_id;
        this.acceptedInterest.message = advertiser.message;
        this.acceptedInterest.date_interested = advertiser.date_interested;
        this.acceptedInterest.submission_status_id = 2;
        this._interest.acceptedInterested(this.acceptedInterest, (responseAccept: ResponseCallbackModel) => {
          if (this.campaign_details.advertisers[index].interested === 0) {
            this.campaign_details.advertisers[index].interested = 1;
            ++this.acceptedAdvertisersCount; // Increment Current Accepted Advertisers
          } else {
            this.campaign_details.advertisers[index].interested = 0;
            --this.acceptedAdvertisersCount; // Increment Current Accepted Advertisers
          }
        });
      }

      removeCampaign() { // Remove current campaign displayed in the page
        this._campaign.removeCampaign(this.campaign_details.campaign_id, (responseRemove: ResponseCallbackModel) => {
          window.location.reload();
        });
      }

      removeInterestedYoutuber(advertiser, campaign_details, index) {
        this.acceptedInterest.youtuber_id = advertiser.youtuber_id;
        this.acceptedInterest.campaign_id = +campaign_details.campaign_id;
        this.acceptedInterest.message = advertiser.message;
        this.acceptedInterest.date_interested = advertiser.date_interested;
        this._interest.removeInterestedYoutuber(this.acceptedInterest, (responseAccept: ResponseCallbackModel) => {
          for (let ads = 0; ads < campaign_details.advertisers.length; ads++) {
            if (advertiser.youtuber_id === campaign_details.advertisers[ads].youtuber_id) {
              campaign_details.advertisers.splice(ads, 1);
            }
          }
        });
      }


}

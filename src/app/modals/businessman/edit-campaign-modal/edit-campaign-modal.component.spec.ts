import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCampaignModal } from './edit-campaign-modal.component';

describe('EditCampaignModal', () => {
  let component: EditCampaignModal;
  let fixture: ComponentFixture<EditCampaignModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCampaignModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCampaignModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { UserBusinessman } from '../../../models/businessman.model';
import { BusinessmanService } from '../../../services/businessman';
import { AuthService } from 'angularx-social-login';
import { Router, RouterLink } from '@angular/router';
import { ResponseCallbackModel } from '../../../models';
import { CampaignModel, AdvertisingDetailModel, CampaignPhotoModel } from '../../../models/campaign.model';

import { UserService } from '../../../services/user.service';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
import { AuthServiceL } from '../../../services/auth.service';
import { CampaignService } from '../../../services/campaign';
import { NotificationService } from '../../../services';

declare var $: any;
declare var moment: any;

@Component({
    selector: 'app-edit-campaign-modal',
    templateUrl: './edit-campaign-modal.component.html',
    styleUrls: ['./edit-campaign-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class EditCampaignModal implements OnInit {

    @Input() campaign_id: any;
    campaign_category: any;
    campaign_photos: any;
    currentPage: any;
    campaign_deadline: any;
    editCampaign: CampaignModel;
    production_deadline: any;
    inputtedTTD: any;
    page = 1; // Pagination in Add Campaign Modal
    productPhoto: any;
    page_selected: any;
    advertising_details: any;
    user_businessman: UserBusinessman;
    get_complete_name: string; // User Businessman First name and Last name

    constructor(
        config: NgbDropdownConfig,
        private authService: AuthService,
        private _notification: NotificationService,
        private _router: Router,
        private _youtuber: YoutuberService,
        private _campaign: CampaignService,
        private _businessman: BusinessmanService,
        private _auth: AuthServiceL,
        private _user: UserService,
        public activeModal: NgbActiveModal) {
        this.user_businessman = new UserBusinessman();
        this.editCampaign = new CampaignModel();
        this.campaign_photos = [];
        this.advertising_details = [];
    }

    ngOnInit() {
        this._campaign.getCampaignDetails(this.campaign_id, (responseEdit: ResponseCallbackModel) => {
            for (let item = 0; item < responseEdit.data.length; item++) { // One item returned but placed inside loop because it returns as an array.
                this.editCampaign.businessman_id = +responseEdit.data[item].businessman_id;
                this.editCampaign.project_name = responseEdit.data[item].project_name;
                this.campaign_category = responseEdit.data[item].category.category_name;
                this.editCampaign.starting_budget = +responseEdit.data[item].starting_budget;
                this.editCampaign.ending_budget = +responseEdit.data[item].ending_budget;
                this.editCampaign.project_description = responseEdit.data[item].project_description;
                var camp_deadline = moment(responseEdit.data[item].campaign_deadline).toDate();
                this.campaign_deadline = { 
                    year: camp_deadline.getFullYear(),
                    month:camp_deadline.getMonth() + 1,
                    day: camp_deadline.getDate()
                };
                var prod_deadline = moment(responseEdit.data[item].production_deadline).toDate();
                this.production_deadline = { 
                    year: prod_deadline.getFullYear(),
                    month:prod_deadline.getMonth() + 1,
                    day: prod_deadline.getDate()
                };
                this.editCampaign.video_duration = +responseEdit.data[item].video_duration;
                this._campaign.getPhotoCampaignToFeed(this.campaign_id, (responsePhoto: ResponseCallbackModel) => {
                    this.campaign_photos = responsePhoto.data;
                });
                this.editCampaign.advertiser_needed = +responseEdit.data[item].advertiser_needed;
                this.advertising_details = responseEdit.data[item].advertising_details;
                const dateSamp = moment(responseEdit.data[item].campaign_deadline).toDate();
                console.log(this.editCampaign);
            }
        });
    }

    closeModal() {
        this.activeModal.close();
    }

    date_convert(deadline: any): string { // Convert date object into string
        const year_converted = (deadline.year).toString();
        let month_converted = deadline.month;
        if (month_converted < 10) {
            month_converted = '0' + (deadline.month).toString();
        } else {
            month_converted = (deadline.month).toString();
        }
        let day_converted = (deadline.day).toString();
        if (day_converted < 10) {
            day_converted = '0' + (deadline.day).toString();
        } else {
            day_converted = (deadline.day).toString();
        }
        return year_converted + '-' + month_converted + '-' + day_converted;
    }

    date_validation(campaign_deadline, production_deadline): boolean {
        const date_now = moment().format('LL');
        const day_now = Number(moment(date_now).date());
        const month_now = Number(moment(date_now).month()) + 1; // January starts at 0
        const year_now = Number(moment(date_now).year());

        // Condition of Date Now
        if (campaign_deadline.year >= year_now) {
            if (campaign_deadline.month >= month_now) {
                if (campaign_deadline.month == month_now) {
                    if (campaign_deadline.day > day_now) {
                        if (production_deadline.year >= campaign_deadline.year) {
                            if (production_deadline.month >= campaign_deadline.month) {
                                if (production_deadline.month == campaign_deadline.month) {
                                    if (production_deadline.day > campaign_deadline.day) {
                                        return true;
                                    } else {
                                        this._notification.warning('Production Deadline Day should be greater than Campaign Deadline Day.');
                                        return false;
                                    }
                                } else {
                                    return true;
                                }
                            } else {
                                this._notification.warning('Production Deadline Month should be greater than Campaign Deadline Month.');
                                return false;
                            }
                        } else {
                            this._notification.warning('Production Deadline Year should be greater than Campaign Deadline Year.');
                            return false;
                        }
                    } else {
                        this._notification.warning('Campaign Deadline Day should be greater than this day.');
                        return false;
                    }
                } else {
                    if (production_deadline.year >= campaign_deadline.year) {
                        if (production_deadline.month >= campaign_deadline.month) {
                            if (production_deadline.month == campaign_deadline.month) {
                                if (production_deadline.day > campaign_deadline.day) {
                                    return true;
                                } else {
                                    this._notification.warning('Production Deadline Day should be greater than Campaign Deadline Day.');
                                    return false;
                                }
                            } else {
                                return true;
                            }
                        } else {
                            this._notification.warning('Production Deadline Month should be greater than Campaign Deadline Month.');
                            return false;
                        }
                    } else {
                        this._notification.warning('Production Deadline Year should be greater than Campaign Deadline Year.');
                        return false;
                    }
                }
            } else {
                this._notification.warning('Month should be greater than or equal to this month.');
                return false;
            }
        } else {
            this._notification.warning('Year should be greater than or equal to this year.');
            return false;
        }
    }

    addThingsToDo() {
        if (this.inputtedTTD !== undefined) {
            var ttdAdded = {
                detail: this.inputtedTTD
            };
            this.advertising_details.push(ttdAdded);
            this.inputtedTTD = '';
        }
    }

    changePage(page) {
        switch (page) {
            case '1':
                $('.page-two').hide();
                $('.page-one').show();
                break;
            case '2':
                $('.page-two').show();
                $('.page-one').hide();
                break;
        }
    }

    removeThingsToDo(thingToDo: any) {
        for (let i = 0; i < this.advertising_details.length; i++) {
            if (this.advertising_details[i].detail === thingToDo) {
                this.advertising_details.splice(i, 1);
            }
        }
    }

    campaignCategory(): number {
        let category_id = 0;
        switch (this.campaign_category) {
            case 'Comedy': category_id = 1; break;
            case 'Drama': category_id = 2; break;
            case 'Education': category_id = 3; break;
            case 'Entertainment': category_id = 4; break;
            case 'Family': category_id = 5; break;
            case 'Gaming': category_id = 6; break;
            case 'Howto and Styles': category_id = 7; break;
            case 'Music': category_id = 8; break;
            case 'Sports': category_id = 9; break;
            case 'Travel and Events': category_id = 10; break;
            case 'People and Blogs': category_id = 11; break;
        }
        return category_id;
    }

    updateCampaign() {
        var validation_campaign_and_production;
        this.editCampaign.campaign_status_id = 1; // On Going Campaign
        this.editCampaign.category_id = this.campaignCategory();
        if( (this.campaign_deadline !== undefined && this.campaign_deadline !== null) &&
            (this.production_deadline !== undefined && this.production_deadline !== null)) {
            this.editCampaign.campaign_deadline = this.date_convert(this.campaign_deadline); // CAMPAIGN DEADLINE CONVERSION
            this.editCampaign.production_deadline = this.date_convert(this.production_deadline); // PRODUCTION DEADLINE CONVERSION
            validation_campaign_and_production = this.date_validation(this.campaign_deadline, this.production_deadline);
        
            if ( (this.editCampaign.starting_budget <= this.editCampaign.ending_budget) &&
                (this.editCampaign.starting_budget !== null && this.editCampaign.ending_budget !== null)) { // Budget Validation
                if (validation_campaign_and_production) { // Date Validation
                    if (this.advertising_details.length !== 0 &&
                        (this.editCampaign.project_name !== undefined && this.editCampaign.project_name !== '') &&
                        (this.editCampaign.project_description !== undefined && this.editCampaign.project_description !== '') &&
                        (this.editCampaign.campaign_deadline !== undefined && this.campaign_deadline !== '') &&
                        (this.editCampaign.production_deadline !== undefined && this.production_deadline !== '') &&
                        (this.editCampaign.advertiser_needed !== null && this.editCampaign.advertiser_needed !== 0)) { // All details validation
                            
                        console.log(this.editCampaign);

                        this._campaign.updateCampaign(this.campaign_id, this.editCampaign, (responseEdit: ResponseCallbackModel) => {
                            var db_photo_existed = []; // Existing Photos
                            for(let added_pic = 0; added_pic < this.campaign_photos.length; added_pic++) {
                                this._campaign.getPhotoCampaignToFeed(this.campaign_id, (getPhotoResponse: ResponseCallbackModel) => {
                                    /* Added New Photo Dynamically */
                                    var photo_exist = false;
                                    for(let current_pic = 0; current_pic < getPhotoResponse.data.length; current_pic++) {
                                        if(this.campaign_photos[added_pic].photo === getPhotoResponse.data[current_pic].photo) {
                                            console.log(added_pic + ' existed already.');
                                            db_photo_existed.push(
                                                {campaign_photo_id: this.campaign_photos[added_pic].campaign_photo_id}
                                            );
                                            current_pic = getPhotoResponse.data.length; // To stop the loop if existed.
                                            photo_exist = true;
                                        }
                                    }

                                    if(photo_exist == false) {
                                        // Insert photo to db
                                        var campaignPhotoModel: CampaignPhotoModel;
                                        campaignPhotoModel = new CampaignPhotoModel();
                                        campaignPhotoModel.campaign_id = this.campaign_id;
                                        campaignPhotoModel.photo = this.campaign_photos[added_pic].photo;
                                        console.log(campaignPhotoModel);
                                        this._campaign.addCampaignPhoto(campaignPhotoModel, (responseAddPhoto: ResponseCallbackModel) => {
                                            console.log('Newly Added: ' + added_pic);
                                        });
                                    }

                                    /* Remove Photo in database dynamically when user removes photo in array */
                                    if(added_pic == this.campaign_photos.length-1) {
                                        setTimeout(() => {
                                            console.log(getPhotoResponse.data);
                                            console.log(db_photo_existed);
                                            for(let existed = 0; existed < getPhotoResponse.data.length; existed++) {
                                                var db_existence = false;
                                                for(let saved_photo = 0; saved_photo < db_photo_existed.length; saved_photo++) {
                                                    if(db_photo_existed[saved_photo].campaign_photo_id === getPhotoResponse.data[existed].campaign_photo_id) {
                                                        saved_photo = db_photo_existed.length;
                                                        db_existence = true;
                                                    }
                                                }
                                                if(db_existence == false) {
                                                    // Delete detail to db
                                                    this._campaign.removeCampaignPhoto(getPhotoResponse.data[existed].campaign_photo_id, (responseRemovePhoto: ResponseCallbackModel) => {
                                                        console.log(getPhotoResponse.data[existed].campaign_photo_id + " is removed.");
                                                    });
                                                }
                                            }
                                        }, 2000);
                                    }
                                });
                            }

                            var db_detail_existed = [];
                            for(let added_detail = 0; added_detail < this.advertising_details.length; added_detail++) {
                                this._campaign.getCampaignDetails(this.campaign_id, (getAdsDetail: ResponseCallbackModel) => {
                                    /* Added New Advertising Detail Dynamically */
                                    var ad_detail_exist = false;
                                    for(let current_detail = 0; current_detail < getAdsDetail.data[0].advertising_details.length; current_detail++) {
                                        if(this.advertising_details[added_detail].detail === getAdsDetail.data[0].advertising_details[current_detail].detail) {
                                            console.log(added_detail + ' detail existed already.');
                                            db_detail_existed.push({
                                                advertising_details_id: this.advertising_details[added_detail].advertising_details_id
                                            });
                                            current_detail = getAdsDetail.data[0].advertising_details.length;
                                            ad_detail_exist = true;
                                        }
                                    }
                                    if(ad_detail_exist == false) {
                                        // Insert Detail to db
                                        var advertisingDetailModel: AdvertisingDetailModel;
                                        advertisingDetailModel = new AdvertisingDetailModel();
                                        advertisingDetailModel.campaign_id = this.campaign_id;
                                        advertisingDetailModel.detail = this.advertising_details[added_detail].detail;
                                        this._campaign.addAdvertisingDetail(advertisingDetailModel, (responseAddDetail: ResponseCallbackModel) => {
                                            console.log('Newly Added Detail: ' + added_detail);
                                        });
                                    }

                                    /* Remove Advertising Detail in database dynamically when user removes detail in array */
                                    if(added_detail == this.advertising_details.length-1) {
                                        setTimeout(() => {
                                            console.log(getAdsDetail.data[0].advertising_details);
                                            console.log(db_detail_existed);
                                            for(let detail_existed = 0; detail_existed < getAdsDetail.data[0].advertising_details.length; detail_existed++) {
                                                var db_detail_existence = false;
                                                for(let saved_detail = 0; saved_detail < db_detail_existed.length; saved_detail++) {
                                                    if(db_detail_existed[saved_detail].advertising_details_id === getAdsDetail.data[0].advertising_details[detail_existed].advertising_details_id) {
                                                        saved_detail = db_detail_existed.length;
                                                        db_detail_existence = true;
                                                    }
                                                }
                                                if(db_detail_existence == false) {
                                                    // Delete detail to db
                                                    this._campaign.removeAdvertisingDetail(getAdsDetail.data[0].advertising_details[detail_existed].advertising_details_id, (responseRemoveDetail) => {
                                                        console.log(getAdsDetail.data[0].advertising_details[detail_existed].advertising_details_id + " is removed.");
                                                    });
                                                }
                                            }
                                        }, 2000);
                                    }
                                });
                            }
                            setTimeout(() => {
                                this.activeModal.dismiss();
                            }, 4000);
                        });
                    } else {
                        this._notification.warning('Please comply all the details completely with correct validation.');
                    }
                } else {
                    // alert('Production Deadline should be advance to Campaign Deadline.');
                }
            } else {
                this._notification.warning('Invalid input of Starting and Ending Budget.');
            }
        } else {
            this._notification.warning('Invalid input of Campaign and/or Production Deadline.');
        }
    }

    removePhoto(photo: any) {
        for (let campaign_photo = 0; campaign_photo < this.campaign_photos.length; campaign_photo++) {
            if (this.campaign_photos[campaign_photo].photo === photo) {
                this.campaign_photos.splice(campaign_photo, 1);
            }
        }
    }
    /* Upload Photo */

    uploadListener($event) {
        this.readUploadedFile($event.target);
    }

    readUploadedFile(getPicture): void {
        const file: File = getPicture.files[0];
        const fileReader: FileReader = new FileReader();

        fileReader.onloadend = (e) => {
            var photo_added = {
                photo: (fileReader.result).replace('data:', '')
            };
            this.campaign_photos.push(photo_added);
        };
        fileReader.readAsDataURL(file);
    }

    /* ------ */
}

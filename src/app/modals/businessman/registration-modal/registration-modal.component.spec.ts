import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationModal } from './registration-modal.component';

describe('RegistrationModal', () => {
  let component: RegistrationModal;
  let fixture: ComponentFixture<RegistrationModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

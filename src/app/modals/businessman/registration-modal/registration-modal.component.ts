import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserBusinessman } from '../../../models/businessman.model';
import { BusinessmanService } from '../../../services/businessman';
import { ResponseCallbackModel } from '../../../models';
import { NotificationService } from '../../../services';

declare var $: any;

@Component({
    selector: 'app-registration-modal',
    templateUrl: './registration-modal.component.html',
    styleUrls: ['./registration-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class RegistrationModal implements OnInit {

    userBusinessman: UserBusinessman;
    retype_password: string;
    profilePicture_existence: any = false;

    constructor(
        public activeModal: NgbActiveModal,
        public _businessman: BusinessmanService,
        public _notification: NotificationService
    ) {
        this.userBusinessman = new UserBusinessman();
    }

    ngOnInit() {
    }

    /* Register */

    private checkPatterns(value: any, pattern: RegExp): boolean {
        if (pattern.test(value)) {
            return true;
        } else {
            return false;
        }
    }

    changePhoto() {
        this.profilePicture_existence = false;
        this.userBusinessman.profile_picture = undefined;
    }

    cancelRegistration() {
        this.activeModal.close();
    }

    submitRegistration() {

        const emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        const aboutsPattern = /[^a-zA-Z0-9._%+-@$]/;

        if (this.userBusinessman.username !== undefined &&
            this.userBusinessman.password !== undefined &&
            this.userBusinessman.business_name !== undefined &&
            this.userBusinessman.first_name !== undefined &&
            this.userBusinessman.last_name !== undefined &&
            this.userBusinessman.email_address !== undefined &&
            this.userBusinessman.company_address !== undefined &&
            this.userBusinessman.contact_number != null &&
            this.userBusinessman.abouts !== undefined &&
            this.userBusinessman.profile_picture !== undefined) {

            this.userBusinessman.profile_picture = this.userBusinessman.profile_picture.replace('data:', '');
            this.userBusinessman.contact_number = (this.userBusinessman.contact_number).toString();

            if (this.userBusinessman.password === this.retype_password) {
                if (this.checkPatterns(this.userBusinessman.email_address, emailPattern)) {
                    this.addUserBusinessman(this.userBusinessman); // Add businessman from API to user_businessman database
                    this.userBusinessman.username = '';
                    this.userBusinessman.password = '';
                    this.userBusinessman.business_name = '';
                    this.userBusinessman.first_name = '';
                    this.userBusinessman.last_name = '';
                    this.userBusinessman.email_address = '';
                    this.userBusinessman.company_address = '';
                    this.userBusinessman.contact_number = '';
                    this.userBusinessman.abouts = '';
                    this.userBusinessman.profile_picture = '';
                    this.userBusinessman.contact_number = '';
                    this.activeModal.dismiss();
                } else {
                    this.userBusinessman.profile_picture = this.userBusinessman.profile_picture;
                    this._notification.warning('Email Address is invalid.');
                    $('.email-address').css('border-color', 'red');
                    $('.email-address').css('border', '1px solid');
                }
            } else {
                this.userBusinessman.profile_picture = this.userBusinessman.profile_picture;
                this._notification.warning('Password and Re-type Password did not match.');
            }
        } else {
            this.userBusinessman.profile_picture = this.userBusinessman.profile_picture;
            this._notification.warning('Please input completely the required fields.');
        }
    }

    /* ------------ */

    /* Photo Upload */

    // uploadFile($event) {
    //   this.addPhotoToArray($event.target);
    // }

    // addPhotoToArray(getPicture): void {
    //   var file: File = getPicture.files[0];
    //   var fileReader: FileReader = new FileReader();

    //   fileReader.onloadend = (e) => {
    //     this.userBusinessman.profile_picture = fileReader.result;
    //     let image= this.userBusinessman.profile_picture;
    //     this.businessman_documents.push(image);
    //     console.log(this.businessman_documents);
    //   }
    //   fileReader.readAsDataURL(file);
    // }

    // removeSpecificDocument(selectedItem) {
    //   for (let i = 0; i < this.businessman_documents.length; i++) {
    //     if (this.businessman_documents[i] === selectedItem) {
    //       this.businessman_documents.splice(i, 1);
    //     }
    //   }
    // }

    uploadListener($event) {
        this.readUploadedFile($event.target);
    }

    readUploadedFile(getPicture): void {
        const file: File = getPicture.files[0];
        const fileReader: FileReader = new FileReader();

        fileReader.onloadend = (e) => {
            this.userBusinessman.profile_picture = fileReader.result;
            this.profilePicture_existence = true;
        };
        fileReader.readAsDataURL(file);
    }

    removeProfilePicture() {
        this.userBusinessman.profile_picture = '';
        this.profilePicture_existence = false;
    }

    /* ------------ */

    /* API Functions */
    addUserBusinessman(userBusinessman) {
        this._businessman.addUserBusinessman(this.userBusinessman, (responseAdd: ResponseCallbackModel) => { });
    }
    /* ------------ */

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReopenCampaignModal } from './reopen-campaign-modal.component';

describe('ReopenCampaignModal', () => {
  let component: ReopenCampaignModal;
  let fixture: ComponentFixture<ReopenCampaignModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReopenCampaignModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReopenCampaignModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedModal } from './submitted-modal.component';

describe('SubmitCampaignModal', () => {
  let component: SubmittedModal;
  let fixture: ComponentFixture<SubmittedModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmittedModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

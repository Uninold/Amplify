import * as storage from 'store';

import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { ResponseCallbackModel, ResponseModel } from '../../../models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VideoModel } from '../../../models/campaign.model';
import { InterestMessageModal } from '../../../modals/youtuber';
import { INTEREST_MESSAGE_OPTIONS } from '../../../configs/modal-options.config';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
import { YoutubeDataAPI } from '../../../services/youtube_data_api/youtube_data_api.service';
import { EmbedVideoService } from 'ngx-embed-video';
declare var $: any;
import { NotificationService } from '../../../services';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'submitted-modal',
    templateUrl: './submitted-modal.component.html',
    styleUrls: ['./submitted-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class SubmittedModal implements OnInit {
    @Input() campaign: any;
    videoDetails: any;
    channelId: string;
    video: VideoModel;
    urlvideo: string;
    google: any;
    iframe_html: any;
    youtuberInfo: any;
    campaign_details: any;
    campaign_photos: any;
    nav: any = 'campaign';
    videoExisted: any = true;
    videoTitle: string;
    videoDescription: string;
    viewCount: number;
    commentCount: number;
    likeCount: number;
    dislikeCount: number;
    noVideo: boolean;
    constructor(
        private _notification: NotificationService,
        private modal: NgbModal,
        public activeModal: NgbActiveModal,
        public _campaign: CampaignService,
        private _youtuber: YoutuberService,
        private embedService: EmbedVideoService,
        private youtubeApi: YoutubeDataAPI
    ) {
        this.noVideo = true;
        this.video = new VideoModel();
        this.urlvideo = '';
    }

    ngOnInit() {
        console.log(this.campaign);
        // this._campaign.getVideoWithYoutuberInfo(this.youtuber_id, this.campaign_id, (response: ResponseCallbackModel) => {
        //     console.log('Campaign Video', response.data);
        //     this.campaign = response.data.youtuber_details;
             this.youtuberInfo = this.campaign.youtuber_details;
             console.log(this.youtuberInfo);
             this.videoTitle = this.campaign.video_details.video_title;
             this.videoDescription = this.campaign.video_details.video_description;
             this.likeCount = this.campaign.video_details.likes_count;
             this.dislikeCount = this.campaign.video_details.dislikes_count;
             this.viewCount = this.campaign.video_details.views_count;
             this.commentCount = this.campaign.video_details.comments_count;
        //     this.noVideo = false;
            this.iframe_html = this.embedService.embed_youtube(this.campaign.video_details.video_id, { attr: { width: 500, height: 300 } });
        // });
        // this._campaign.getCampaignDetails(this.campaign_id, (responseGet: ResponseCallbackModel) => {
        //     this.campaign_details = responseGet.data;
        //     console.log(this.campaign_details);
        //     this._campaign.getPhotoCampaignToFeed(this.campaign_id, (responseAddPhoto: ResponseCallbackModel) => {
        //         this.campaign_photos = responseAddPhoto.data;
        //         console.log(this.campaign_photos);
        //     });
        // });
    }

    closeModal() {
        this.activeModal.close();
    }

    switchNav(selected) {
        // this.nav = selected;
        // if (selected === 'campaign') {
        //     $('.campaign-info').css({'font-weight': 'bold', 'color': '#c0392b'});
        //     $('.reports').css({'font-weight': 'normal', 'color': '#000'});
        // } else if (selected === 'reports') {
        //     $('.reports').css({'font-weight': 'bold', 'color': '#c0392b'});
        //     $('.campaign-info').css({'font-weight': 'normal', 'color': '#000'});
        //     this._campaign.getVideo(this.campaign.accepted_interest_id, (responseGetVideo: ResponseCallbackModel) => {
        //         console.log('Campaign Video', responseGetVideo.data);
        //         this.videoTitle = responseGetVideo.data.video_title;
        //         this.videoDescription = responseGetVideo.data.video_description;
        //         this.likeCount = responseGetVideo.data.likes_count;
        //         this.dislikeCount = responseGetVideo.data.dislikes_count;
        //         this.viewCount = responseGetVideo.data.views_count;
        //         this.commentCount = responseGetVideo.data.comments_count;
        //         this.noVideo = false;
        //         this.iframe_html = this.embedService.embed(this.urlvideo, { attr: { width: 500, height: 300 } });
        //     });
        // }
    }

    // getVideo() {
    //     this._youtuber.getYoutuberInfo(this.google.id, (responseYoutuber: ResponseCallbackModel) => {
    //         this.channelId = responseYoutuber.data.youtubeCh;
    //                 const url = this.urlvideo.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    //                 console.log(url[2]);
    //                 this.youtubeApi.getVideo(url[2], (response: ResponseCallbackModel) => {
    //                     console.log(response.data);
    //                     this.videoDetails = response.data;
    //                         if (response.data.items[0].snippet.channelId === responseYoutuber.data.youtubeCh) {
    //                             this.video.accepted_interest_id = this.campaign.accepted_interest_id;
    //                             this.video.video_id = url[2];
    //                             this.video.video_title = this.videoDetails.items[0].snippet.title;
    //                             this.video.video_description = this.videoDetails.items[0].snippet.description;
    //                             this.video.views_count = this.videoDetails.items[0].statistics.viewCount;
    //                             this.video.likes_count = this.videoDetails.items[0].statistics.likeCount;
    //                             this.video.dislikes_count = this.videoDetails.items[0].statistics.dislikeCount;
    //                             this.video.comments_count = this.videoDetails.items[0].statistics.commentCount;
    //                             this._campaign.addVideo(this.video);
    //                             console.log('yours truly');
    //                         } else {
    //                             this._notification.warning('The video is not in your channel. Please check your URL');
    //                             console.log('not yout video');
    //                         }
    //                 });
    //         });
    //     }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProfileModal } from './view-profile-modal.component';

describe('ViewProfileModal', () => {
  let component: ViewProfileModal;
  let fixture: ComponentFixture<ViewProfileModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewProfileModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProfileModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

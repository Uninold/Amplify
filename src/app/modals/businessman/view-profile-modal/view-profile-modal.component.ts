import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResponseCallbackModel, UserBusinessman } from '../../../models';
import { BusinessmanService } from '../../../services/businessman';

declare var $: any;

@Component({
    selector: 'view-profile-modal',
    templateUrl: './view-profile-modal.component.html',
    styleUrls: ['./view-profile-modal.component.css']
})
export class ViewProfileModal implements OnInit {

    businessman: UserBusinessman;
    complete_name: any;

    constructor(
        public activeModal: NgbActiveModal,
        public _businessman: BusinessmanService) {
            this.businessman = new UserBusinessman();
    }

    ngOnInit() {
        this._businessman.getCurrentBusinessman((responseCurrentBusinessman: ResponseCallbackModel) => {
            console.log('getCurrentBusinessman', responseCurrentBusinessman.data);
            this.businessman.businessman_id = responseCurrentBusinessman.data.businessman_id;
            this.businessman.business_name = responseCurrentBusinessman.data.business_name;
            this.businessman.first_name = responseCurrentBusinessman.data.first_name;
            this.businessman.last_name = responseCurrentBusinessman.data.last_name;
            this.businessman.company_address = responseCurrentBusinessman.data.company_address;
            this.businessman.abouts = responseCurrentBusinessman.data.abouts;
            this.businessman.contact_number = responseCurrentBusinessman.data.contact_number;
            this.businessman.email_address = responseCurrentBusinessman.data.email_address;
            this.businessman.profile_picture = responseCurrentBusinessman.data.profile_picture;
            this.complete_name = this.businessman.first_name + ' ' + this.businessman.last_name;
        })
    }

    closeModal(){
        this.activeModal.close();
    }
}

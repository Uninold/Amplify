import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared.module';
import { EmbedVideoService } from 'ngx-embed-video';

import {
  RegistrationModal,
  ViewProfileModal,
  AddCampaignModal,
  SubmittedModal,
  EditCampaignModal,
  ReopenCampaignModal,
  BusinessmanRatingModal,
  CampaignModal
} from './businessman';
import {
  CampaignDetailsModal,
  CampaignDetailsReferModal,
  YoutuberViewProfileModal,
  InterestMessageModal,
  SubmitCampaignModal,
  OpenVideoModal,
  MediakitModal,
  ReferalModal
} from './youtuber';
import { ConfigureVideoWeightModal, ConfigureBusinessmanRatingWeightModal } from './admin';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    RegistrationModal,
    ViewProfileModal,
    AddCampaignModal,
    SubmittedModal,
    EditCampaignModal,
    CampaignDetailsModal,
    CampaignDetailsReferModal,
    YoutuberViewProfileModal,
    InterestMessageModal,
    SubmitCampaignModal,
    OpenVideoModal,
    MediakitModal,
    ReferalModal,
    ReopenCampaignModal,
    CampaignModal,
    BusinessmanRatingModal,
    ConfigureVideoWeightModal,
    ConfigureBusinessmanRatingWeightModal
  ],
  entryComponents: [
    RegistrationModal,
    ViewProfileModal,
    AddCampaignModal,
    CampaignModal,
    CampaignDetailsModal,
    CampaignDetailsReferModal,
    SubmittedModal,
    EditCampaignModal,
    YoutuberViewProfileModal,
    InterestMessageModal,
    SubmitCampaignModal,
    OpenVideoModal,
    MediakitModal,
    ReferalModal,
    ReopenCampaignModal,
    BusinessmanRatingModal,
    ConfigureVideoWeightModal,
    ConfigureBusinessmanRatingWeightModal
  ],
  providers: [EmbedVideoService]
})
export class ModalsModule { }

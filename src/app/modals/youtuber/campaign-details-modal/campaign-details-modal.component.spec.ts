import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDetailsModal } from './campaign-details-modal.component';

describe('CampaignDetailsModal', () => {
  let component: CampaignDetailsModal;
  let fixture: ComponentFixture<CampaignDetailsModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignDetailsModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDetailsModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

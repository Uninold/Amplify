import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { ResponseCallbackModel } from '../../../models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { REFERAL_OPTIONS } from '../../../configs/modal-options.config';

import { ReferalModal } from '../../../modals/youtuber';

declare var $: any;

@Component({
    selector: 'campaign-details-modal',
    templateUrl: './campaign-details-modal.component.html',
    styleUrls: ['./campaign-details-modal.component.css']
})
export class CampaignDetailsModal implements OnInit {

    @Input() campaign_id: any;
    campaign_details: any;
    campaign_photos: any;

    constructor(
        private modal: NgbModal,
        public activeModal: NgbActiveModal,
        public _campaign: CampaignService
    ) {
    }

    ngOnInit() {
        this._campaign.getCampaignDetails(this.campaign_id, (responseGet : ResponseCallbackModel) => {
            this.campaign_details = responseGet.data;
            console.log(this.campaign_details);
            this._campaign.getPhotoCampaignToFeed(this.campaign_id, (responseAddPhoto : ResponseCallbackModel) => {
                this.campaign_photos = responseAddPhoto.data;
            });
        });
    }

    closeModal() {
        this.activeModal.close();
    }
    openReferalModal() {
        this.activeModal.close();
        const referalmodal =  this.modal.open(ReferalModal, REFERAL_OPTIONS);
        referalmodal.componentInstance.campaign_id = this.campaign_id;
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDetailsReferModal } from './campaign-details-refer-modal.component';

describe('CampaignDetailsModal', () => {
  let component: CampaignDetailsReferModal;
  let fixture: ComponentFixture<CampaignDetailsReferModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignDetailsReferModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDetailsReferModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

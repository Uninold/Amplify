import * as storage from 'store';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { ResponseCallbackModel } from '../../../models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { REFERAL_OPTIONS } from '../../../configs/modal-options.config';
import { InterestedModel, CampaignDetailModel } from '../../../models/campaign.model';
import { CampaignDetailsModal, InterestMessageModal } from '../../../modals/youtuber';
import { CAMPAIGN_DETAILS_OPTIONS, INTEREST_MESSAGE_OPTIONS } from '../../../configs/modal-options.config';
import { ReferalModal } from '../../../modals/youtuber';
import { InterestService } from '../../../services/interest/interest.service';
import { NotificationModel } from '../../../models/notification.model';
import { NotificationActivityService } from '../../../services/notification-activity';
declare var $: any;

@Component({
    selector: 'campaign-details-refer-modal',
    templateUrl: './campaign-details-refer-modal.component.html',
    styleUrls: ['./campaign-details-refer-modal.component.css']
})
export class CampaignDetailsReferModal implements OnInit {

    @Input() campaign_id: any;
    all_campaigns: Array<CampaignDetailModel>;
    campaign_details: any;
    campaign_photos: any;
    campaign: any;
    google: any;
    interestedDetails: InterestedModel;

    constructor(
        private modal: NgbModal,
        public activeModal: NgbActiveModal,
        public _campaign: CampaignService,
        private _interest: InterestService
    ) {
    }

    ngOnInit() {
        this.google = storage.get('current_youtuber');
        this._campaign.getAllCampaign(this.google.id, (responseAllFeed: ResponseCallbackModel) => {
            console.log('asd1', responseAllFeed);
            this.all_campaigns = responseAllFeed.data;
            for (let a of this.all_campaigns) {
                if (a.campaign_id == this.campaign_id) {
                    this.campaign = a;
                    console.log('asd', this.campaign);
                }
            }
        });
        this._campaign.getCampaignDetails(this.campaign_id, (responseGet : ResponseCallbackModel) => {
            this.campaign_details = responseGet.data;
            console.log(this.campaign_details);
            this._campaign.getPhotoCampaignToFeed(this.campaign_id, (responseAddPhoto : ResponseCallbackModel) => {
                this.campaign_photos = responseAddPhoto.data;
            });
        });
    }
    interested() {
        if (this.campaign.interested === 0) { // Opens modal, send message, then interested.
            const messageModal = this.modal.open(InterestMessageModal, INTEREST_MESSAGE_OPTIONS);
            messageModal.componentInstance.campaign = this.campaign;
            messageModal.componentInstance.interested = this.campaign.interested;
            messageModal.componentInstance.emitService.subscribe((emmitedValue) => {
            this.campaign.interested = emmitedValue;
            });
        } else { // If youtuber is Uninterested.
            this.google = storage.get('current_youtuber');
            this.interestedDetails.campaign_id = this.campaign.campaign_id;
            this.interestedDetails.youtuber_id = this.google.id;
            this.interestedDetails.message = '';
            this._interest.interested(this.interestedDetails);
            this.campaign.interested = 0;
        }
    }
    closeModal() {
        this.activeModal.close();
    }
    openReferalModal() {
        this.activeModal.close();
        const referalmodal =  this.modal.open(ReferalModal, REFERAL_OPTIONS);
        referalmodal.componentInstance.campaign_id = this.campaign_id;
    }

}

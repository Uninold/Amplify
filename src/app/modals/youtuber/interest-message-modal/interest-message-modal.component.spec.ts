import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestMessageModal } from './interest-message-modal.component';

describe('InterestMessageModal', () => {
  let component: InterestMessageModal;
  let fixture: ComponentFixture<InterestMessageModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestMessageModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestMessageModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

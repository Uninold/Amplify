import * as storage from 'store';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { ResponseCallbackModel } from '../../../models';
import { InterestedModel } from '../../../models/campaign.model';
import { InterestService } from '../../../services/interest/interest.service';
import { NotificationActivityService } from '../../../services/notification-activity/notification-activity.service';
import { NotificationModel } from '../../../models/notification.model';
declare var $: any;

@Component({
    selector: 'interest-message-modal',
    templateUrl: './interest-message-modal.component.html',
    styleUrls: ['./interest-message-modal.component.css']
})
export class InterestMessageModal implements OnInit {

    @Input() campaign: any;
    @Input() interested: any;
    @Output() emitService = new EventEmitter();
    message: string;
    google: any;
    interestedDetails: any;
    campaign_details: any;
    notification = new NotificationModel();
    constructor(
        public activeModal: NgbActiveModal,
        public _interest: InterestService,
        private _notification: NotificationActivityService,
        private _campaign: CampaignService
    ) {
        this.interestedDetails = new InterestedModel();
    }

    ngOnInit() {
        this.google = storage.get('current_youtuber');
        console.log('Interested State: ', this.campaign);
    }

    closeModal() {
        this.activeModal.close();
    }

    onKeydown(event) {
        if (event.key === 'Enter') {
            this.interestedAction();
        }
    }

    interestedAction() {
        if (this.message != null && this.message != undefined && this.message != '') {
            this._campaign.getCampaignDetails(this.campaign.campaign_id, (responseGet : ResponseCallbackModel) => {
            this.campaign_details = responseGet.data[0];
            console.log('asd', this.campaign_details);
            this.google = storage.get('current_youtuber');
            this.notification.notif_msg = 'is interested';
            this.notification.notif_from = this.google.id;
            this.notification.notif_to = this.campaign_details.businessman_id;
            this.notification.user_img = this.google.photoUrl;
            this.notification.name = this.google.name;
            this.notification.notif_subject = this.campaign_details.campaign_id;
            this._notification.notification(this.notification);
            this.interestedDetails.campaign_id = +this.campaign.campaign_id;
            this.interestedDetails.youtuber_id = this.google.id;
            this.interestedDetails.message = this.message;
            console.log('details', this.interestedDetails);
            this._interest.interested(this.interestedDetails);
            if (this.interested === 0) {
                this.emitService.next(1);
            }
            // Notification if submitted.
            this.activeModal.close();
            });
        } else {
            // Notification if no message.
        }
    }

}

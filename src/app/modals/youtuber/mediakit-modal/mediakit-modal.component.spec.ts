import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediakitModal } from './mediakit-modal.component';

describe('MediakitModal', () => {
  let component: MediakitModal;
  let fixture: ComponentFixture<MediakitModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediakitModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediakitModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

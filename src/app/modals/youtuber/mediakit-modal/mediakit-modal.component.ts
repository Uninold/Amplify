import * as storage from 'store';
import { Component, OnInit, Input } from '@angular/core';
import { NgbDropdownConfig, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { Router, RouterLink } from '@angular/router';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { YoutuberMediakit, UserYoutuber, ResponseCallbackModel, YoutuberMediakitInterest, YoutuberVideoData } from '../../../models';
import { NotificationService, AuthServiceL } from '../../../services';
import { YoutuberService } from '../../../services/youtuber';
import { YoutubeDataAPI } from '../../../services/youtube_data_api';

@Component({
  selector: 'mediakit-modal',
  templateUrl: './mediakit-modal.component.html',
  styleUrls: ['./mediakit-modal.component.css'],
  providers: [NgbDropdownConfig]
})
export class MediakitModal implements OnInit {

  categories: any;
  user: SocialUser;
  youtuberInfo: YoutuberMediakit;
  userYoutuber: UserYoutuber;
  google: any;
  firstName: string;
  input_interest: any;
  interests: any;
  load_time: any;
  fill: any;
  submitted: any;

  private loggedIn: boolean;
  constructor(config: NgbDropdownConfig,
    private _notification: NotificationService,
    private authService: AuthService,
    private router: Router,
    private _youtuber: YoutuberService,
    private _youtube_data_api: YoutubeDataAPI,
    private location: Location,
    private _auth: AuthServiceL,
    private activeModal: NgbActiveModal) {
    config.placement = 'top-left';
    config.autoClose = false;
  }

  ngOnInit() {
    this.fill = [];
    this.userYoutuber = new UserYoutuber();
    this.youtuberInfo = new YoutuberMediakit();
    this.interests = [];
    this.categories = [10, 17, 19, 20, 22, 23, 24, 26, 27, 36, 37];
    this.load_time = 0;
    this.submitted = false;
  }

  onSubmitMediaKit() {
    if (this.youtuberInfo.firstName !== undefined && this.youtuberInfo.lastName !== undefined &&
      this.youtuberInfo.contact_no != null && this.youtuberInfo.gender !== undefined &&
      this.youtuberInfo.address !== undefined && this.interests.length != 0 &&
      this.youtuberInfo.abouts !== undefined && this.youtuberInfo.youtubeCh !== undefined &&
      this.youtuberInfo.fb_ac !== undefined && this.youtuberInfo.twitter_ac !== undefined &&
      this.youtuberInfo.ig_ac !== undefined) {
      this._youtube_data_api.youtubeChannelExistence(this.youtuberInfo.youtubeCh, (responseExistence: ResponseCallbackModel) => {
        if(!responseExistence.data) {
          this._youtube_data_api.getYoutuberChannel(this.youtuberInfo.youtubeCh, (responseChannel: ResponseCallbackModel) => {
            if (!responseChannel.error && responseChannel.data.items.length != 0) {
              this.submitted = true;
              this.getAllVideoNoPage(this.youtuberInfo.youtubeCh);
            } else {
              this._notification.error('You should upload at least one video in your channel or channel may not exist.');
            }
          });
        }
      });
    } else {
      this._notification.warning("Fill all the blanks in the form.");
    }
  }

  getAllVideoNoPage(channel_id) {
    this._youtube_data_api.getAllVideoYoutuberNoPageToken(channel_id, (responseData: ResponseCallbackModel) => {
      let data_returned = responseData.data;
      if(data_returned.items.length < 50 && data_returned.items.length > 0) {
        for (let turn = 0; turn < data_returned.items.length; turn++) {
          let youtuberVideoData = new YoutuberVideoData();
          let videoId = data_returned.items[turn].id.videoId;
          if (videoId !== undefined) {
            this._youtube_data_api.getVideo(videoId, (responseVideo: ResponseCallbackModel) => {
              let video_data_returned = responseVideo.data;
              youtuberVideoData.category_id = +video_data_returned.items[0].snippet.categoryId;
              for (let cat = 0; cat < this.categories.length; cat++) {
                if (this.categories[cat] != youtuberVideoData.category_id) {
                  // No implementation
                } else {
                  // Insert Video here.
                  this.load_time = this.load_time + 350; // Extra reload
                  switch (youtuberVideoData.category_id) {
                    case 10: youtuberVideoData.category_id = this.campaignCategory('Music'); break; // Music
                    case 17: youtuberVideoData.category_id = this.campaignCategory('Sports'); break; // Sports
                    case 19: youtuberVideoData.category_id = this.campaignCategory('Travel and Events'); break; // Comedy
                    case 20: youtuberVideoData.category_id = this.campaignCategory('Gaming'); break; // Gaming
                    case 22: youtuberVideoData.category_id = this.campaignCategory('People and Blogs'); break; // Comedy
                    case 23: youtuberVideoData.category_id = this.campaignCategory('Comedy'); break; // Comedy
                    case 24: youtuberVideoData.category_id = this.campaignCategory('Entertainment'); break; // Entertainment
                    case 26: youtuberVideoData.category_id = this.campaignCategory('Howto and Styles'); break; // Howto and Styles
                    case 27: youtuberVideoData.category_id = this.campaignCategory('Education'); break; // Education
                    case 36: youtuberVideoData.category_id = this.campaignCategory('Drama'); break; // Drama
                    case 37: youtuberVideoData.category_id = this.campaignCategory('Family'); break; // Family
                  }
                  youtuberVideoData.video_id = responseVideo.data.items[0].id;
                  youtuberVideoData.channel_id = responseVideo.data.items[0].snippet.channelId;
                  youtuberVideoData.channel_title = responseVideo.data.items[0].snippet.channelTitle;
                  youtuberVideoData.video_description = responseVideo.data.items[0].snippet.description;
                  youtuberVideoData.default_thumbnail = responseVideo.data.items[0].snippet.thumbnails.default.url;
                  youtuberVideoData.video_title = responseVideo.data.items[0].snippet.title;
                  youtuberVideoData.comments_count = +responseVideo.data.items[0].statistics.commentCount;
                  youtuberVideoData.dislikes_count = +responseVideo.data.items[0].statistics.dislikeCount;
                  youtuberVideoData.likes_count = +responseVideo.data.items[0].statistics.likeCount;
                  youtuberVideoData.views_count = +responseVideo.data.items[0].statistics.viewCount;
                  let youtuber_id = storage.get('current_youtuber');
                  youtuberVideoData.youtuber_id = youtuber_id.id; // Foreign key video id
                  this._youtube_data_api.addYoutuberVideoData(youtuberVideoData, (responseAddVideo: ResponseCallbackModel) => {
                    console.log(youtuberVideoData);
                  });
                }
              }
            });
          }
          if (turn == data_returned.items.length - 1) {
            this.google = storage.get('current_youtuber');
            this.youtuberInfo.photoUrl = this.google.photoUrl;
            this.youtuberInfo.youtuber_id = this.google.id;
            this._youtuber.getYoutuberInfo(this.google.youtuber_id, (response: ResponseCallbackModel) => {
              if (!response.error && !response.data) {
                this._youtuber.addYoutuberMediakit(this.youtuberInfo, (responseAdd: ResponseCallbackModel) => {
                  if (responseAdd.data && !responseAdd.error) {
                    for (let interest = 0; interest < this.interests.length; interest++) {
                      const youtuberMediakitInterest = new YoutuberMediakitInterest();
                      youtuberMediakitInterest.interest = this.interests[interest];
                      youtuberMediakitInterest.id = responseAdd.data.id;
                      this._youtuber.addYoutuberMediakitInterest(youtuberMediakitInterest, (responseAddInterest: ResponseCallbackModel) => {
                        storage.remove('youtuber_existence');
                      });
                    }
                  }
                });
              }
            });
            setTimeout(() => {
              this.activeModal.dismiss();
            }, this.load_time+3000);
          }
        }
      } else {
        if(data_returned.items.length > 0) {
          for (let turn = 0; turn < data_returned.items.length; turn++) {
            let youtuberVideoData = new YoutuberVideoData();
            let videoId = data_returned.items[turn].id.videoId;
            if (videoId !== undefined) {
              this._youtube_data_api.getVideo(videoId, (responseVideo: ResponseCallbackModel) => {
                let video_data_returned = responseVideo.data;
                youtuberVideoData.category_id = +video_data_returned.items[0].snippet.categoryId;
                for (let cat = 0; cat < this.categories.length; cat++) {
                  if (this.categories[cat] != youtuberVideoData.category_id) {
                    // No implementation
                  } else {
                    // Insert Video here.
                    this.load_time = this.load_time + 350; // Extra reload
                    switch (youtuberVideoData.category_id) {
                      case 10: youtuberVideoData.category_id = this.campaignCategory('Music'); break; // Music
                      case 17: youtuberVideoData.category_id = this.campaignCategory('Sports'); break; // Sports
                      case 19: youtuberVideoData.category_id = this.campaignCategory('Travel and Events'); break; // Comedy
                      case 20: youtuberVideoData.category_id = this.campaignCategory('Gaming'); break; // Gaming
                      case 22: youtuberVideoData.category_id = this.campaignCategory('People and Blogs'); break; // Comedy
                      case 23: youtuberVideoData.category_id = this.campaignCategory('Comedy'); break; // Comedy
                      case 24: youtuberVideoData.category_id = this.campaignCategory('Entertainment'); break; // Entertainment
                      case 26: youtuberVideoData.category_id = this.campaignCategory('Howto and Styles'); break; // Howto and Styles
                      case 27: youtuberVideoData.category_id = this.campaignCategory('Education'); break; // Education
                      case 36: youtuberVideoData.category_id = this.campaignCategory('Drama'); break; // Drama
                      case 37: youtuberVideoData.category_id = this.campaignCategory('Family'); break; // Family
                    }
                    youtuberVideoData.video_id = responseVideo.data.items[0].id;
                    youtuberVideoData.channel_id = responseVideo.data.items[0].snippet.channelId;
                    youtuberVideoData.channel_title = responseVideo.data.items[0].snippet.channelTitle;
                    youtuberVideoData.video_description = responseVideo.data.items[0].snippet.description;
                    youtuberVideoData.default_thumbnail = responseVideo.data.items[0].snippet.thumbnails.default.url;
                    youtuberVideoData.video_title = responseVideo.data.items[0].snippet.title;
                    youtuberVideoData.comments_count = +responseVideo.data.items[0].statistics.commentCount;
                    youtuberVideoData.dislikes_count = +responseVideo.data.items[0].statistics.dislikeCount;
                    youtuberVideoData.likes_count = +responseVideo.data.items[0].statistics.likeCount;
                    youtuberVideoData.views_count = +responseVideo.data.items[0].statistics.viewCount;
                    let youtuber_id = storage.get('current_youtuber');
                    youtuberVideoData.youtuber_id = youtuber_id.id; // Foreign key video id
                    this._youtube_data_api.addYoutuberVideoData(youtuberVideoData, (responseAddVideo: ResponseCallbackModel) => {
                      console.log(youtuberVideoData);
                    });
                  }
                }
              });
            }
            if (turn == data_returned.items.length - 1) {
              this.getAllVideoWithPage(channel_id, data_returned.nextPageToken);
            }
          }
        } else {
          this.google = storage.get('current_youtuber');
          console.log(this.google);
          this.youtuberInfo.photoUrl = this.google.photoUrl;
          this.youtuberInfo.youtuber_id = this.google.id;
          this._youtuber.getYoutuberInfo(this.google.youtuber_id, (response: ResponseCallbackModel) => {
            if (!response.error && !response.data) {
              this._youtuber.addYoutuberMediakit(this.youtuberInfo, (responseAdd: ResponseCallbackModel) => {
                if (responseAdd.data && !responseAdd.error) {
                  for (let interest = 0; interest < this.interests.length; interest++) {
                    const youtuberMediakitInterest = new YoutuberMediakitInterest();
                    youtuberMediakitInterest.interest = this.interests[interest];
                    youtuberMediakitInterest.id = responseAdd.data.id;
                    this._youtuber.addYoutuberMediakitInterest(youtuberMediakitInterest, (responseAddInterest: ResponseCallbackModel) => {
                      storage.remove('youtuber_existence');
                    });
                  }
                }
              });
            }
          });
          setTimeout(() => {
            this.activeModal.dismiss();
          }, 4000);
        }
      }
    });
  }

  getAllVideoWithPage(channel_id, page_token) {
    this._youtube_data_api.getAllVideoYoutuberWithPageToken(channel_id, page_token, (responseDataNext: ResponseCallbackModel) => {
      const next_data_returned = responseDataNext.data;
      if (next_data_returned.items.length !== 0) {
        for (let turnNext = 0; turnNext < next_data_returned.items.length; turnNext++) {
          const youtuberVideoData = new YoutuberVideoData();
          const videoId = next_data_returned.items[turnNext].id.videoId;
          if (videoId !== undefined) {
            this._youtube_data_api.getVideo(videoId, (responseVideoNext: ResponseCallbackModel) => {
              const video_next_data_returned = responseVideoNext.data;
              youtuberVideoData.category_id = +video_next_data_returned.items[0].snippet.categoryId;
              for (let cat = 0; cat < this.categories.length; cat++) {
                if (this.categories[cat] !== youtuberVideoData.category_id) {
                  // No implementation
                } else {
                  // Insert Video here.
                  this.load_time = this.load_time + 350; // Extra reload
                  switch (youtuberVideoData.category_id) {
                    case 10: youtuberVideoData.category_id = this.campaignCategory('Music'); break; // Music
                    case 17: youtuberVideoData.category_id = this.campaignCategory('Sports'); break; // Sports
                    case 19: youtuberVideoData.category_id = this.campaignCategory('Travel and Events'); break; // Comedy
                    case 20: youtuberVideoData.category_id = this.campaignCategory('Gaming'); break; // Gaming
                    case 22: youtuberVideoData.category_id = this.campaignCategory('People and Blogs'); break; // Comedy
                    case 23: youtuberVideoData.category_id = this.campaignCategory('Comedy'); break; // Comedy
                    case 24: youtuberVideoData.category_id = this.campaignCategory('Entertainment'); break; // Entertainment
                    case 26: youtuberVideoData.category_id = this.campaignCategory('Howto and Styles'); break; // Howto and Styles
                    case 27: youtuberVideoData.category_id = this.campaignCategory('Education'); break; // Education
                    case 36: youtuberVideoData.category_id = this.campaignCategory('Drama'); break; // Drama
                    case 37: youtuberVideoData.category_id = this.campaignCategory('Family'); break; // Family
                  }
                  youtuberVideoData.video_id = responseVideoNext.data.items[0].id;
                  youtuberVideoData.channel_id = responseVideoNext.data.items[0].snippet.channelId;
                  youtuberVideoData.channel_title = responseVideoNext.data.items[0].snippet.channelTitle;
                  youtuberVideoData.video_description = responseVideoNext.data.items[0].snippet.description;
                  youtuberVideoData.default_thumbnail = responseVideoNext.data.items[0].snippet.thumbnails.default.url;
                  youtuberVideoData.video_title = responseVideoNext.data.items[0].snippet.title;
                  youtuberVideoData.comments_count = +responseVideoNext.data.items[0].statistics.commentCount;
                  youtuberVideoData.dislikes_count = +responseVideoNext.data.items[0].statistics.dislikeCount;
                  youtuberVideoData.likes_count = +responseVideoNext.data.items[0].statistics.likeCount;
                  youtuberVideoData.views_count = +responseVideoNext.data.items[0].statistics.viewCount;
                  let youtuber_id = storage.get('current_youtuber');
                  youtuberVideoData.youtuber_id = youtuber_id.id; // Foreign key video id
                  this._youtube_data_api.addYoutuberVideoData(youtuberVideoData, (responseAddVideo: ResponseCallbackModel) => {
                    console.log(youtuberVideoData);
                  });
                }
              }
            });
          }
        }
      }
      if (next_data_returned.nextPageToken !== undefined) {
        this.getAllVideoWithPage(channel_id, next_data_returned.nextPageToken);
      } else if (next_data_returned.nextPageToken === undefined) {
        this.google = storage.get('current_youtuber');
        console.log(this.google);
        this.youtuberInfo.photoUrl = this.google.photoUrl;
        this.youtuberInfo.youtuber_id = this.google.id;
        this._youtuber.getYoutuberInfo(this.google.youtuber_id, (response: ResponseCallbackModel) => {
          if (!response.error && !response.data) {
            this._youtuber.addYoutuberMediakit(this.youtuberInfo, (responseAdd: ResponseCallbackModel) => {
              if (responseAdd.data && !responseAdd.error) {
                for (let interest = 0; interest < this.interests.length; interest++) {
                  const youtuberMediakitInterest = new YoutuberMediakitInterest();
                  youtuberMediakitInterest.interest = this.interests[interest];
                  youtuberMediakitInterest.id = responseAdd.data.id;
                  this._youtuber.addYoutuberMediakitInterest(youtuberMediakitInterest, (responseAddInterest: ResponseCallbackModel) => {
                    storage.remove('youtuber_existence');
                  });
                }
              }
            });
          }
        });
        setTimeout(() => {
          this.activeModal.dismiss();
        }, this.load_time);
      }
    });
  }

  campaignCategory(campaign_category): number {
    let category_id = 0;
    switch (campaign_category) {
      case 'Comedy': category_id = 1; break;
      case 'Drama': category_id = 2; break;
      case 'Education': category_id = 3; break;
      case 'Entertainment': category_id = 4; break;
      case 'Family': category_id = 5; break;
      case 'Gaming': category_id = 6; break;
      case 'Howto and Styles': category_id = 7; break;
      case 'Music': category_id = 8; break;
      case 'Sports': category_id = 9; break;
      case 'Travel and Events': category_id = 10; break;
      case 'People and Blogs': category_id = 11; break;
    }
    return category_id;
  }

  onKeydown(event) {
    if(event.key === "Enter") {
      this.interests.push(event.target.value);
      this.input_interest = '';
    }
  }

  onBack(): void {
    this._auth.logOut((response: ResponseCallbackModel) => {
      if(response.error) {
        this.activeModal.close();
      }
    });
  }
  signOut(): void {
    this._auth.logOut((response: ResponseCallbackModel) => {
      this.authService.signOut().then(res => {
        storage.clearAll();
        this.router.navigate(['/login'])
      });
    });

  }
  addInterest() {
    this.interests.push(this.input_interest);
    this.input_interest = '';
  }
  removeInterest(interest) {
    for (let int = 0; int < this.interests.length; int++) {
      if (this.interests[int] === interest) {
        this.interests.splice(int, 1);
      }
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenVideoModal } from './open-video-modal.component';

describe('SubmitCampaignModal', () => {
  let component: OpenVideoModal;
  let fixture: ComponentFixture<OpenVideoModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenVideoModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenVideoModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

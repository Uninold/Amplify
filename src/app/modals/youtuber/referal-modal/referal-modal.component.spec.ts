import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferalModal } from './referal-modal.component';

describe('ReferalModal', () => {
  let component: ReferalModal;
  let fixture: ComponentFixture<ReferalModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferalModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferalModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import * as storage from 'store';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { ResponseCallbackModel } from '../../../models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { YoutuberMediakit } from '../../../models/youtuber.model';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
declare var $: any;
import { NotificationActivityService } from '../../../services/notification-activity/notification-activity.service';
import { NotificationModel } from '../../../models/notification.model';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'referal-modal',
    templateUrl: './referal-modal.component.html',
    styleUrls: ['./referal-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class ReferalModal implements OnInit {

    @Input() campaign_id: any;
    campaign_details: any;
    campaign_photos: any;
    current_youtuber: any;
    youtubers: Array<YoutuberMediakit> = [];
    search_name: string;
    notification = new NotificationModel();
    google: any;

    constructor(
        private modal: NgbModal,
        private activeModal: NgbActiveModal,
        private _campaign: CampaignService,
        private _youtuber: YoutuberService,
        private _notification: NotificationActivityService,
    ) {
        this.search_name = '';
    }

    ngOnInit() {
        this.current_youtuber = storage.get('current_youtuber');
        this._youtuber.getReferAllYoutuber(this.current_youtuber.id, (responseGet: ResponseCallbackModel) => {
            this.youtubers = responseGet.data;
            console.log('youtubers', this.youtubers);
        });
    }

    closeModal() {
        this.activeModal.close();
    }
    search(search_name) {
        if (search_name == '') {
            this._youtuber.getReferAllYoutuber(this.current_youtuber.id, (responseGet: ResponseCallbackModel) => {
                this.youtubers = responseGet.data;
            });
        } else {
            this._youtuber.searchYoutuberMediakit(search_name, (responseGet: ResponseCallbackModel) => {
                this.youtubers = responseGet.data;
            });
        }
    }

    refer(youtuber_id) {
            this.google = storage.get('current_youtuber');
            this.notification.notif_msg = 'referred you';
            this.notification.notif_from = this.google.id;
            this.notification.notif_to = youtuber_id;
            this.notification.user_img = this.google.photoUrl;
            this.notification.name = this.google.name;
            this.notification.notif_subject = this.campaign_id;
            this._notification.notification(this.notification);
            this.activeModal.close();
    }
}


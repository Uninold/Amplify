import * as storage from 'store';

import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { ResponseCallbackModel, ResponseModel } from '../../../models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VideoModel } from '../../../models/campaign.model';
import { InterestMessageModal, OpenVideoModal } from '../../../modals/youtuber';
import { INTEREST_MESSAGE_OPTIONS, OPEN_VIDEO_OPTIONS } from '../../../configs/modal-options.config';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
import { YoutubeDataAPI } from '../../../services/youtube_data_api/youtube_data_api.service';
import { EmbedVideoService } from 'ngx-embed-video';
import { NotificationService } from '../../../services';
import { InterestService } from '../../../services/interest';

declare var $: any;
declare var moment: any;

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'submit-campaign-modal',
    templateUrl: './submit-campaign-modal.component.html',
    styleUrls: ['./submit-campaign-modal.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class SubmitCampaignModal implements OnInit {
    @Input() campaign: any;
    @Input() campaign_id: any;
    channelId: string;
    video: VideoModel;
    urlvideo: string;
    google: any;
    campaign_details: any;
    campaign_photos: any;
    nav: any = 'campaign';
    videoTitle: string;
    videoDescription: string;
    videoPhoto: any;
    viewCount: number;
    commentCount: number;
    likeCount: number;
    dislikeCount: number;
    noVideo: boolean;

    constructor(
        private _notification: NotificationService,
        private modal: NgbModal,
        public activeModal: NgbActiveModal,
        public _campaign: CampaignService,
        public _interest: InterestService,
        private _youtuber: YoutuberService,
        private embedService: EmbedVideoService,
        private youtubeApi: YoutubeDataAPI
    ) {
        this.noVideo = true;
        this.urlvideo = '';
        this.video = new VideoModel();
    }

    ngOnInit() {
        console.log(this.campaign);
        this.google = storage.get('current_youtuber');
        this._campaign.getVideo(this.campaign.accepted_interest_id, (response: ResponseCallbackModel) => {
            if (!response.error) {
                this.videoTitle = response.data.video_title;
                this.videoDescription = response.data.video_description;
                this.likeCount = response.data.likes_count;
                this.dislikeCount = response.data.dislikes_count;
                this.viewCount = response.data.views_count;
                this.commentCount = response.data.comments_count;
                this.videoPhoto = response.data.default_thumbnail;
                this.noVideo = false;
            } else {
                this.noVideo = true;
                this._notification.error('Video Already Posted. Please don\'t reuse a video. ');
            }
        });
    }

    closeModal() {
        this.activeModal.close();
    }

    switchNavVidExistence(selected) {
        if (selected === 'reports') {
            this._campaign.getVideo(this.campaign.accepted_interest_id, (responseGetVideo: ResponseCallbackModel) => {
                console.log('Campaign Video', responseGetVideo.data);
                this.videoTitle = responseGetVideo.data.video_title;
                this.videoDescription = responseGetVideo.data.video_description;
                this.likeCount = responseGetVideo.data.likes_count;
                this.dislikeCount = responseGetVideo.data.dislikes_count;
                this.viewCount = responseGetVideo.data.views_count;
                this.commentCount = responseGetVideo.data.comments_count;
                this.noVideo = false;
            });
        }
    }

    getVideo() {
        this._youtuber.getYoutuberInfo(this.google.id, (responseYoutuber: ResponseCallbackModel) => {
            this.channelId = responseYoutuber.data.youtubeCh;
            const url = this.urlvideo.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
            console.log(url[2]);
            this.youtubeApi.getVideo(url[2], (response: ResponseCallbackModel) => {
                console.log(response.data);
                if (response.data.items[0].snippet.channelId === responseYoutuber.data.youtubeCh) {
                    this.video.accepted_interest_id = +this.campaign.accepted_interest_id;
                    let youtuber = storage.get('current_youtuber');
                    this.video.youtuber_id = youtuber.id;
                    this.video.video_id = url[2];

                    switch (+response.data.items[0].snippet.categoryId) {
                        case 10: this.video.category_id = this.campaignCategory('Music'); break; // Music
                        case 17: this.video.category_id = this.campaignCategory('Sports'); break; // Sports
                        case 19: this.video.category_id = this.campaignCategory('Travel and Events'); break; // Comedy
                        case 20: this.video.category_id = this.campaignCategory('Gaming'); break; // Gaming
                        case 22: this.video.category_id = this.campaignCategory('People and Blogs'); break; // Comedy
                        case 23: this.video.category_id = this.campaignCategory('Comedy'); break; // Comedy
                        case 24: this.video.category_id = this.campaignCategory('Entertainment'); break; // Entertainment
                        case 26: this.video.category_id = this.campaignCategory('Howto and Styles'); break; // Howto and Styles
                        case 27: this.video.category_id = this.campaignCategory('Education'); break; // Education
                        case 36: this.video.category_id = this.campaignCategory('Drama'); break; // Drama
                        case 37: this.video.category_id = this.campaignCategory('Family'); break; // Family
                    }

                    this.video.video_title = response.data.items[0].snippet.title;
                    this.video.video_description = response.data.items[0].snippet.description;
                    this.video.views_count = +response.data.items[0].statistics.viewCount;
                    this.video.likes_count = +response.data.items[0].statistics.likeCount;
                    this.video.default_thumbnail = response.data.items[0].snippet.thumbnails.high.url;
                    this.video.dislikes_count = +response.data.items[0].statistics.dislikeCount;
                    this.video.comments_count = +response.data.items[0].statistics.commentCount;
                    this.video.channel_id = response.data.items[0].snippet.channelId;
                    this.video.channel_title = response.data.items[0].snippet.channelTitle;

                    if(this.video.category_id == +this.campaign.campaign_details.category_id) { // If video uploaded category is equals to the campaign category
                        // Video Existed API
                        this._youtuber.getVideoExisted(this.video.video_id, (responseGetVideo: ResponseCallbackModel) => {
                            console.log(responseGetVideo);
                            if(responseGetVideo.message !== 'Video existed already.') {
                                this._campaign.addVideo(this.video, (responseAddVideo: ResponseCallbackModel) => {
                                    if(!responseAddVideo.error) {
                                        let date_now = moment();
                                        let prod_deadline = moment(this.campaign.campaign_details.production_deadline);
                                        if(date_now > prod_deadline) { // If submission is late
                                            // Youtuber ID, Campaign ID, Submission Status (1 for submitted, 3 for Late), With Video
                                            this._interest.updateAcceptedInterest(this.video.youtuber_id, +this.campaign.campaign_id, 3, 1, (responseAddVideo: ResponseCallbackModel) => {
                                                setTimeout(() => {
                                                    this.activeModal.dismiss();
                                                }, 1000);
                                            });
                                        } else { // If submission is on time
                                            // Youtuber ID, Campaign ID, Submission Status (1 for submitted, 3 for Late), With Video
                                            this._interest.updateAcceptedInterest(this.video.youtuber_id, +this.campaign.campaign_id, 1, 1, (responseAddVideo: ResponseCallbackModel) => {
                                                setTimeout(() => {
                                                    this.activeModal.dismiss();
                                                }, 1000);
                                            });
                                        }
                                        this._notification.success('Video successfully added.');
                                    }
                                });
                            } else {
                                this._notification.warning(responseGetVideo.message);
                            }
                        });
                    } else {
                        this._notification.error('Category of video upload is not the same as the campaign.');
                    }
                } else {
                    this._notification.warning('The video is not in your channel. Please check your URL');
                    console.log('not yout video');
                }
            });
        });
    }

    campaignCategory(campaign_category): number {
        let category_id = 0;
        switch (campaign_category) {
            case 'Comedy': category_id = 1; break;
            case 'Drama': category_id = 2; break;
            case 'Education': category_id = 3; break;
            case 'Entertainment': category_id = 4; break;
            case 'Family': category_id = 5; break;
            case 'Gaming': category_id = 6; break;
            case 'Howto and Styles': category_id = 7; break;
            case 'Music': category_id = 8; break;
            case 'Sports': category_id = 9; break;
            case 'Travel and Events': category_id = 10; break;
            case 'People and Blogs': category_id = 11; break;
        }
        return category_id;
    }

    openVideo(video: any) {
        this._campaign.getVideo(this.campaign.accepted_interest_id, (response: ResponseCallbackModel) => {
            if (!response.error) {
                let modal = this.modal.open(OpenVideoModal, OPEN_VIDEO_OPTIONS);
                modal.componentInstance.video_id = response.data.video_id;
            }
        });
    }

}

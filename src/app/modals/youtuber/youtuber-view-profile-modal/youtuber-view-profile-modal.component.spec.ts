import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutuberViewProfileModal } from './youtuber-view-profile-modal.component';

describe('YoutuberViewProfileModal', () => {
  let component: YoutuberViewProfileModal;
  let fixture: ComponentFixture<YoutuberViewProfileModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoutuberViewProfileModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutuberViewProfileModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

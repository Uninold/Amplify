import * as storage from 'store';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ResponseCallbackModel } from '../../../models';
import { YoutuberService } from '../../../services/youtuber';
import { YoutubeDataAPI } from '../../../services/youtube_data_api';
import { OPEN_VIDEO_PROFILE_OPTIONS } from '../../../configs/modal-options.config';
import { OpenVideoModal } from '..';

declare var $: any;

@Component({
    selector: 'app-youtuber-view-profile-modal',
    templateUrl: './youtuber-view-profile-modal.component.html',
    styleUrls: ['./youtuber-view-profile-modal.component.css']
})
export class YoutuberViewProfileModal implements OnInit {

    @Input() youtuber_id: any;
    tab_1: any = true;
    tab_2: any = false;
    tab_3: any = false;
    reputation_score: any;
    youtuber_details: any;
    videos: any;
    overall_influence_rating: any = 0;

    constructor(
        public activeModal: NgbActiveModal,
        public _youtuber: YoutuberService,
        public _youtube_data_api: YoutubeDataAPI,
        private modal: NgbModal
    ) {
        this.reputation_score = [];
    }

    ngOnInit() {
        $('.tab1').css("color", "#c0392b");
        $('.tab2').css("color", "#000");
        $('.tab3').css("color", "#000");
        $('.tab4').css("color", "#000");

        this._youtuber.getYoutuberProfile(this.youtuber_id, (responseProfile: ResponseCallbackModel) => {
            this.youtuber_details = responseProfile.data;

            for(let index = 0; index < this.youtuber_details.reputation_score.length; index++) { // Placing the category in an array if there is points.
                if(this.youtuber_details.reputation_score[index].points > 0) {
                    this.reputation_score.push(this.youtuber_details.reputation_score[index]);
                }
            }

            let tempVar;

            for(let i=0; i < this.reputation_score.length; i++) {
                for(let j=i; j < this.reputation_score.length; j++) {
                    if(this.reputation_score[i].points < this.reputation_score[j].points) {
                        tempVar = this.reputation_score[i];
                        this.reputation_score[i] = this.reputation_score[j];
                        this.reputation_score[j] = tempVar;
                    }
                }
            }

            /* Compute Overall Influence Rating */

            for(let j = 0; j < this.reputation_score.length; j++) {
                this.overall_influence_rating = this.overall_influence_rating + this.reputation_score[j].points;
            }
        });
    }

    openPage(currentPage) {
        switch(currentPage) {
            case 'Tab 1': {
                this.tab_1 = true;
                this.tab_2 = false;
                this.tab_3 = false;
                $('.tab1').css("color", "#c0392b");
                $('.tab2').css("color", "#000");
                $('.tab3').css("color", "#000");
                $('.profile').css("color", "#c0392b");
                $('.points').css("color", "#000");
                $('.video').css("color", "#000");
            }; break;
            case 'Tab 2': {
                this.tab_1 = false;
                this.tab_2 = true;
                this.tab_3 = false;
                $('.tab1').css("color", "#000");
                $('.tab2').css("color", "#c0392b");
                $('.tab3').css("color", "#000");
                $('.profile').css("color", "#000");
                $('.points').css("color", "#c0392b");
                $('.video').css("color", "#000");
            }; break;
            case 'Tab 3': {
                this.tab_1 = false;
                this.tab_2 = false;
                this.tab_3 = true;
                $('.tab1').css("color", "#000");
                $('.tab2').css("color", "#000");
                $('.tab3').css("color", "#c0392b");
                $('.profile').css("color", "#000");
                $('.points').css("color", "#000");
                $('.video').css("color", "#c0392b");
            }; break;
        }
    }

    openVideo(video: any) {
        let modal = this.modal.open(OpenVideoModal, OPEN_VIDEO_PROFILE_OPTIONS);
        modal.componentInstance.video_id = video.video_id;
    }

    closeModal(){
        this.activeModal.close();
    }
}

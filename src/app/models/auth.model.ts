export class LoginModel {
  identity: string;
  password: string;
}

export class AuthModel {
  user: string;
  token: string;
}

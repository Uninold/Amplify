export class UserBusinessman {
    businessman_id: number;
    username: string;
    password: string;
    business_name: string;
    first_name: string;
    last_name: string;
    email_address: string;
    company_address: string;
    contact_number: string;
    abouts: string;
    profile_picture: string;
    date_added?: string;
}

export class ActivityLogBusinessman {
    businessman_id: number;
    activity: string;
}

export class BusinessmanRatingModel {
    engaging: number = 0;
    credibility: number = 0;
    impression: number = 0;
    action_oriented: number = 0;
    significance: number = 0;
    integrated: number = 0;
    brand_service: number = 0;
    brand_innovation: number = 0;
    brand_quality: number = 0;
    accepted_interest_id: any; // Foreign Key
}
export class CampaignModel {
    businessman_id: number;
    project_name: string;
    starting_budget: number;
    ending_budget: number;
    project_description: string;
    campaign_deadline: string;
    production_deadline: string;
    video_duration: number;
    advertiser_needed: number;
    category_id: number;
    date_added?: string;
    campaign_status_id: number;
}

export class BusinessmanCampaignModel {
    businessman_id: number;
    campaign_id: number;
    starting_budget: number;
    ending_budget: number;
    advertisers: Array<AdvertiserCampaignModel>;
    project_name: string;
    project_description: string;
    photos: CampaignPhotoModel;
    campaign_deadline: string;
    production_deadline: string;
    video_duration: number;
    advertiser_needed: number;
    category: CategoryModel;
    date_added?: string;
    campaign_status_id: number;
}
export class CampaignDetailModel {
    advertiser_needed: number;
    campaign_id: number;
    starting_budget: number;
    ending_budget: number;
    project_name: string;
    photos: PhotoModel;
    campaign_deadline: string;
    production_deadline: string;
    category_name: string;
    interested: number;
}
export class CategoryModel {
    category_name: string;
}
export class AdvertiserCampaignModel {
    authToken: string;
    firstName: string;
    lastName: string;
    campaign_id: number;
    date_added: string;
    email: string;
    idToken: string;
    name: string;
    photoUrl: string;
    youtuber_id: number;
    youtuber_interest_id: number;
    video_statistics: Array<VideoStatistics>;
}
export class StatisticsDate {
    count: number;
    date_added: string;
}
export class VideoStatistics {
    video_id: string;
    views: number;
    likes: number;
    dislikes: number;
    date_added: string;
}
export class PhotoModel {
    photo: string;
}
export class CampaignPhotoModel {
    campaign_id?: number;
    photo: string;
}

export class AdvertisingDetailModel {
    campaign_id?: number;
    detail: string;
}

export class InterestedModel {
    campaign_id: number;
    youtuber_id: string;
    message: string;
}
export class MyCampaignModel {
    advertiser_needed: number;
    campaign_id: number;
    starting_budget: number;
    ending_budget: number;
    project_name: string;
    campaign_deadline: string;
    production_deadline: string;
    category_name: string;
    businessman_id: number;
    date_added: string;
    project_description: string;
    status: string;
    video_duration: number;
    photos: PhotoModel;
    businessman_details: BusinessmanDetailsModel;
    advertisers: Array<AdvertiserCampaignModel>;
    campaign_details: Campaign;
}
export class Campaign {
    advertiser_needed: number;
    businessman_id: number;
    campaign_deadline: any;
    campaign_id: number;
    category_name: string;
    date_added: string;
    ending_budget: number;
    production_deadline: string;
    project_description: any;
    project_name: string;
    starting_budget: number;
    status: string;
    video_duration: number;
}
export class BusinessmanDetailsModel {
    abouts: string;
    business_name: string;
    businessman_id: number;
    company_address: string;
    contact_number: string;
    date_added: string;
    email_address: string;
    first_name: string;
    last_name: string;
    profile_picture: string;
}

export class VideoModel {
    video_id: string;
    video_title: string;
    video_description: string;
    channel_id: string;
    channel_title: string;
    views_count: number;
    likes_count: number;
    dislikes_count: number;
    category_id: number;
    comments_count: number;
    default_thumbnail: string;
    date_accomplished?: string;
    youtuber_id: string;
    accepted_interest_id: number;
}
export class GetVideo { // Model for getting video
    youtuber_id: string;
    campaign_id: number;
}

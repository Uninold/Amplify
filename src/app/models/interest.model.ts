export class AcceptedInterestModel {
    campaign_id: number;
    youtuber_id: string;
    message: string;
    date_interested: string;
    submission_status_id: number;
    israted?: boolean;
}
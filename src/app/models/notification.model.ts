export class NotificationModel {
    notif_id?: number;
    user_img: string;
    name: string;
    notif_subject: string;
    notif_msg: string;
    notif_to: string;
    notif_from: string;
    notif_subject_name?: string;
    seen?: any;
}

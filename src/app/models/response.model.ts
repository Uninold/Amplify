export class ResponseModel {
  error: number;
  message: string;
  data: any;
}

export class ResponseCallbackModel {
  error: boolean;
  message: string;
  data: any;
}

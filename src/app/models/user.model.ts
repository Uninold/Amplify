export class UserModel {
  display_name: string;
  profile_picture: string;
  user_role_desc: string;
}

export class YoutubeVideo {
    videoId: string;
    category_id: number;
    likeCount: number;
    dislikeCount: number;
    commentCount: number;
    viewCount: number;
}

export class YoutubeVideoStatistics {
    video_id: string;
    likeCount: number;
    dislikeCount: number;
    viewCount: number;
    date_added?: string;
}

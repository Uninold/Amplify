export class UserYoutuber {
  id: string;
  authToken: string;
  name: string;
  email: string;
  photoUrl: string;
  idToken: string;
  date_added:string;
  date_ended:string;
}

export class YoutuberMediakit {
  youtuber_id: number;
  firstName: string;
  lastName: string;
  address: string;
  contact_no: string;
  youtubeCh: string;
  ig_ac: string;
  fb_ac: string;
  twitter_ac: string;
  gender: string;
  photoUrl: string;
  interests: string;
  abouts: string;
  date_added: string;
  date_ended: string;
}
export class YoutuberMediakitInterest {
  interest: string;
  id: number;
}

export class YoutubeAPIKey {
  api_key = 'AIzaSyDmh6Lml_0GJy5mec_i-oX32uL9OK-Y4sE';
}

export class YoutuberVideoData {
  video_id: any;
  video_title: any;
  video_description: any;
  channel_id: any;
  channel_title: any;
  category_id: any;
  default_thumbnail: any;
  comments_count: any;
  dislikes_count: any;
  likes_count: any;
  views_count: any;
  youtuber_id: any; // Youtuber ID foreign key
}
export class YoutuberSearch {
  search_name: string;
}

export class TopInfluencer {
  top_influencer: any;
}



import * as storage from 'store';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { YoutubeDataAPI } from '../../services/youtube_data_api/youtube_data_api.service';
import { ResponseCallbackModel } from '../../models/response.model';
import { UserYoutuber, YoutuberMediakit, YoutubeAPIKey, YoutubeVideo } from '../../models';
import { NotificationService, AuthServiceL } from '../../services';
import { CampaignService } from '../../services/campaign';
import { YoutuberService } from '../../services/youtuber';
import { BusinessmanService } from '../../services/businessman';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigureVideoWeightModal, ConfigureBusinessmanRatingWeightModal } from '../../modals/admin';
import { CONFIGURE_VIDEO_WEIGHT_OPTIONS, CONFIGURE_BUSINESSMAN_RATING_WEIGHT_OPTIONS } from '../../configs/modal-options.config';

declare var $: any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css', '../../app.component.css']
})

export class AdminComponent implements OnInit {

  businessmen: any;
  categories: any;
  criteria: any;
  criteria_id: any;
  current_percentage: any;
  criteria_percentage: any;
  load_time: any;
  youtubers: any;
  youtubeVideo: YoutubeVideo;

  constructor(
    private _notification: NotificationService,
    private _auth: AuthServiceL,
    private authService: AuthService,
    private _router: Router,
    private _businessman: BusinessmanService,
    private _youtube_data_api: YoutubeDataAPI,
    private modal: NgbModal,
    private _youtuber: YoutuberService) {
      this.youtubeVideo = new YoutubeVideo();
      this.categories = [10, 17, 19, 20, 22, 23, 24, 26, 27, 36, 37];
      this.load_time = 0;
  }

  ngOnInit() {
    this._youtuber.getAllYoutuber((responseAllYoutuber: ResponseCallbackModel) => {
      this.youtubers = responseAllYoutuber.data;
      console.log(this.youtubers);
    });
    this._businessman.getAllBusinessman((responseAllBusinessman: ResponseCallbackModel) => {
      this.businessmen = responseAllBusinessman.data;
      console.log(this.businessmen);
    });
  }

  logout(): void {
    this._auth.logOut((response: ResponseCallbackModel) => {
      this._router.navigate(['/login']);
    });
    setTimeout(() => {
      this.authService.signOut().then(res => {
        storage.clearAll();
      });
    }, 2000);
  }

  syncAllData() {
    this._youtube_data_api.removeYoutubeData((responseRemove: ResponseCallbackModel) => {
      if(!responseRemove.error) {
        this.autoGetYoutubeData();
      }
    });
  }

  autoGetYoutubeData() {
    const categoryIDs = ['23', '17', '19', '26', '27', '36', '37', '24', '20', '10', '22'];
    for (let cId = 0; cId < categoryIDs.length; cId++) {
      this.syncWithoutPageToken(categoryIDs[cId]);
    }
  }

  syncWithoutPageToken(categoryId) {
    this._youtube_data_api.getAllDataPerCategoryNoPage(categoryId, (responseSearchData: ResponseCallbackModel) => {
      for (let video = 0; video < responseSearchData.data.items.length; video++) {
        const videoId = responseSearchData.data.items[video].id.videoId;
        if (videoId !== undefined) {
          this._youtube_data_api.getVideo(videoId, (responseVideoData: ResponseCallbackModel) => {
            this.youtubeVideo = new YoutubeVideo();
            this.youtubeVideo.videoId = responseVideoData.data.items[0].id;
            switch (+categoryId) {
              case 10: this.youtubeVideo.category_id = this.campaignCategory('Music'); break; // Music
              case 17: this.youtubeVideo.category_id = this.campaignCategory('Sports'); break; // Sports
              case 19: this.youtubeVideo.category_id = this.campaignCategory('Travel and Events'); break; // Comedy
              case 20: this.youtubeVideo.category_id = this.campaignCategory('Gaming'); break; // Gaming
              case 22: this.youtubeVideo.category_id = this.campaignCategory('People and Blogs'); break; // Comedy
              case 23: this.youtubeVideo.category_id = this.campaignCategory('Comedy'); break; // Comedy
              case 24: this.youtubeVideo.category_id = this.campaignCategory('Entertainment'); break; // Entertainment
              case 26: this.youtubeVideo.category_id = this.campaignCategory('Howto and Styles'); break; // Howto and Styles
              case 27: this.youtubeVideo.category_id = this.campaignCategory('Education'); break; // Education
              case 36: this.youtubeVideo.category_id = this.campaignCategory('Drama'); break; // Drama
              case 37: this.youtubeVideo.category_id = this.campaignCategory('Family'); break; // Family
            }
            // tslint:disable-next-line:radix
            this.youtubeVideo.viewCount = parseInt(responseVideoData.data.items[0].statistics.viewCount);
            // tslint:disable-next-line:radix
            this.youtubeVideo.likeCount = parseInt(responseVideoData.data.items[0].statistics.likeCount);
            // tslint:disable-next-line:radix
            this.youtubeVideo.dislikeCount = parseInt(responseVideoData.data.items[0].statistics.dislikeCount);
            // tslint:disable-next-line:radix
            this.youtubeVideo.commentCount = parseInt(responseVideoData.data.items[0].statistics.commentCount);
            if (responseVideoData.data.items[0].statistics.commentCount === undefined) { // Video that won't allow comments
              this.youtubeVideo.commentCount = 0;
            }
            if (responseVideoData.data.items[0].statistics.likeCount === undefined) { // Video that won't allow comments
              this.youtubeVideo.likeCount = 0;
            }
            if (responseVideoData.data.items[0].statistics.dislikeCount === undefined) { // Video that won't allow comments
              this.youtubeVideo.dislikeCount = 0;
            }
            if (responseVideoData.data.items[0].statistics.viewCount === undefined) { // Video that won't allow comments
              this.youtubeVideo.viewCount = 0;
            }
            this._youtube_data_api.syncAllDataPerCategory(this.youtubeVideo, (responseAddVideoData: ResponseCallbackModel) => {
              console.log(responseAddVideoData.message);
            });

          });
        }
        if (video === responseSearchData.data.items.length - 1) {
          this.syncWithPageToken(categoryId, responseSearchData.data.nextPageToken);
        }
      }
    });
  }

  syncWithPageToken(categoryId: string, nextPageToken: string) {
    this._youtube_data_api.getAllDataPerCategoryWithPage(categoryId, nextPageToken, (responseSearchData: ResponseCallbackModel) => {
      if (responseSearchData.data.items.length !== 0) {
        for (let video = 0; video < responseSearchData.data.items.length; video++) {
          const videoId = responseSearchData.data.items[video].id.videoId;
          if (videoId !== undefined) {
            this._youtube_data_api.getVideo(videoId, (responseVideoData: ResponseCallbackModel) => {
              this.youtubeVideo = new YoutubeVideo();
              if (responseVideoData.data.items[0].id != null) {
                this.youtubeVideo.videoId = responseVideoData.data.items[0].id;
                switch (+categoryId) {
                  case 10: this.youtubeVideo.category_id = this.campaignCategory('Music'); break; // Music
                  case 17: this.youtubeVideo.category_id = this.campaignCategory('Sports'); break; // Sports
                  case 19: this.youtubeVideo.category_id = this.campaignCategory('Travel and Events'); break; // Comedy
                  case 20: this.youtubeVideo.category_id = this.campaignCategory('Gaming'); break; // Gaming
                  case 22: this.youtubeVideo.category_id = this.campaignCategory('People and Blogs'); break; // Comedy
                  case 23: this.youtubeVideo.category_id = this.campaignCategory('Comedy'); break; // Comedy
                  case 24: this.youtubeVideo.category_id = this.campaignCategory('Entertainment'); break; // Entertainment
                  case 26: this.youtubeVideo.category_id = this.campaignCategory('Howto and Styles'); break; // Howto and Styles
                  case 27: this.youtubeVideo.category_id = this.campaignCategory('Education'); break; // Education
                  case 36: this.youtubeVideo.category_id = this.campaignCategory('Drama'); break; // Drama
                  case 37: this.youtubeVideo.category_id = this.campaignCategory('Family'); break; // Family
                }
                // tslint:disable-next-line:radix
                this.youtubeVideo.viewCount = parseInt(responseVideoData.data.items[0].statistics.viewCount);
                // tslint:disable-next-line:radix
                this.youtubeVideo.likeCount = parseInt(responseVideoData.data.items[0].statistics.likeCount);
                // tslint:disable-next-line:radix
                this.youtubeVideo.dislikeCount = parseInt(responseVideoData.data.items[0].statistics.dislikeCount);
                // tslint:disable-next-line:radix
                this.youtubeVideo.commentCount = parseInt(responseVideoData.data.items[0].statistics.commentCount);
                if (responseVideoData.data.items[0].statistics.commentCount === undefined) { // Video that won't allow comments
                  this.youtubeVideo.commentCount = 0;
                }
                if (responseVideoData.data.items[0].statistics.likeCount === undefined) { // Video that won't allow comments
                  this.youtubeVideo.likeCount = 0;
                }
                if (responseVideoData.data.items[0].statistics.dislikeCount === undefined) { // Video that won't allow comments
                  this.youtubeVideo.dislikeCount = 0;
                }
                if (responseVideoData.data.items[0].statistics.viewCount === undefined) { // Video that won't allow comments
                  this.youtubeVideo.viewCount = 0;
                }
                this._youtube_data_api.syncAllDataPerCategory(this.youtubeVideo, (responseAddVideoData: ResponseCallbackModel) => {
                  console.log(responseAddVideoData.message);
                });
              }
            });
          }
        }
      }
      if (responseSearchData.data.nextPageToken !== undefined) {
        this.syncWithPageToken(categoryId, responseSearchData.data.nextPageToken);
      }
    });
  }

  campaignCategory(campaign_category): number {
    let category_id = 0;
    switch (campaign_category) {
      case 'Comedy': category_id = 1; break;
      case 'Drama': category_id = 2; break;
      case 'Education': category_id = 3; break;
      case 'Entertainment': category_id = 4; break;
      case 'Family': category_id = 5; break;
      case 'Gaming': category_id = 6; break;
      case 'Howto and Styles': category_id = 7; break;
      case 'Music': category_id = 8; break;
      case 'Sports': category_id = 9; break;
      case 'Travel and Events': category_id = 10; break;
      case 'People and Blogs': category_id = 11; break;
    }
    return category_id;
  }

  updateVideo(youtuber_id: any, youtube_channel_id: any) {
    this._youtube_data_api.getYoutuberChannel(youtube_channel_id, (responseChannel: ResponseCallbackModel) => {
      if (!responseChannel.error && responseChannel.data.items.length != 0) {
        console.log('It entered!');
        this.getAllVideoNoPage(youtube_channel_id);
        this._youtuber.getYoutuberProfile(youtuber_id, (responseProfile: ResponseCallbackModel) => {
          if(responseProfile.message === 'YouTuber Profile') {
            this._notification.success('Youtuber has been updated.');
          }
        });
      } else {
        this._notification.error('You should upload at least one video in your channel or channel may not exist.');
      }
    });
  }

  getAllVideoNoPage(youtube_channel_id) {
    this._youtube_data_api.getAllVideoYoutuberNoPageToken(youtube_channel_id, (responseData: ResponseCallbackModel) => {
      let data_returned = responseData.data;
      if(data_returned.items.length < 50) {
        for (let turn = 0; turn < data_returned.items.length; turn++) {
          let videoId = data_returned.items[turn].id.videoId;
          this._youtube_data_api.getVideoExisted(videoId, (responseExisted: ResponseCallbackModel) => {
            if(responseExisted.message !== 'Video existed already.') {
              if (videoId !== undefined) {
                this._youtube_data_api.getVideo(videoId, (responseVideo: ResponseCallbackModel) => {
                  let video_data_returned = responseVideo.data;
                  let youtuberVideoData = {
                    comments_count: +responseVideo.data.items[0].statistics.commentCount,
                    dislikes_count: +responseVideo.data.items[0].statistics.dislikeCount,
                    likes_count: +responseVideo.data.items[0].statistics.likeCount,
                    views_count: +responseVideo.data.items[0].statistics.viewCount
                  };
                  this._youtube_data_api.updateVideo(videoId, youtuberVideoData, (responseUpdate: ResponseCallbackModel) => {
                    console.log('Ning update?');
                  });
                });
              }
            }
          });
          if (turn == data_returned.items.length - 1) {
            setTimeout(() => {
              // window.location.reload();
            }, this.load_time+3000);
          }
        }
      } else {
        for (let turn = 0; turn < data_returned.items.length; turn++) {
          let videoId = data_returned.items[turn].id.videoId;
          this._youtube_data_api.getVideoExisted(videoId, (responseExisted: ResponseCallbackModel) => {
            if(responseExisted.message !== 'Video existed already.') {
              if (videoId !== undefined) {
                this._youtube_data_api.getVideo(videoId, (responseVideo: ResponseCallbackModel) => {
                  let video_data_returned = responseVideo.data;
                  let youtuberVideoData = {
                    comments_count: +responseVideo.data.items[0].statistics.commentCount,
                    dislikes_count: +responseVideo.data.items[0].statistics.dislikeCount,
                    likes_count: +responseVideo.data.items[0].statistics.likeCount,
                    views_count: +responseVideo.data.items[0].statistics.viewCount
                  };
                  this._youtube_data_api.updateVideo(videoId, youtuberVideoData, (responseUpdate: ResponseCallbackModel) => { });
                });
              }
            }
          });
          if (turn == data_returned.items.length - 1) {
            this.getAllVideoWithPage(youtube_channel_id, data_returned.nextPageToken);
          }
        }
      }
    });
  }

  getAllVideoWithPage(youtube_channel_id, page_token) {
    this._youtube_data_api.getAllVideoYoutuberWithPageToken(youtube_channel_id, page_token, (responseDataNext: ResponseCallbackModel) => {
      const next_data_returned = responseDataNext.data;
      if (next_data_returned.items.length !== 0) {
        for (let turnNext = 0; turnNext < next_data_returned.items.length; turnNext++) {
          let videoId = next_data_returned.items[turnNext].id.videoId;
          this._youtube_data_api.getVideoExisted(videoId, (responseExisted: ResponseCallbackModel) => {
            if(responseExisted.message !== 'Video existed already.') {
              if (videoId !== undefined) {
                this._youtube_data_api.getVideo(videoId, (responseVideo: ResponseCallbackModel) => {
                  let video_data_returned = responseVideo.data;
                  let youtuberVideoData = {
                    comments_count: +responseVideo.data.items[0].statistics.commentCount,
                    dislikes_count: +responseVideo.data.items[0].statistics.dislikeCount,
                    likes_count: +responseVideo.data.items[0].statistics.likeCount,
                    views_count: +responseVideo.data.items[0].statistics.viewCount
                  };
                  this._youtube_data_api.updateVideo(videoId, youtuberVideoData, (responseUpdate: ResponseCallbackModel) => { });
                });
              }
            }
          });
        }
      }
      if (next_data_returned.nextPageToken !== undefined) {
        this.getAllVideoWithPage(youtube_channel_id, next_data_returned.nextPageToken);
      }
      else if (next_data_returned.nextPageToken == undefined) {
        setTimeout(() => {
          // window.location.reload();
        }, this.load_time);
      }
    });
  }
  

  removeYoutuber(youtuber_id, index) {
    this._youtuber.removeYoutuber(youtuber_id, (responseRemove: ResponseCallbackModel) => {
      if(!responseRemove.error) {
        this.youtubers.splice(index, 1);
      }
    });
  }

  removeBusinessman(businessman_id) {
    this._businessman.removeBusinessman(businessman_id, (responseRemove: ResponseCallbackModel) => {
      if(!responseRemove.error) {
        setTimeout(() => {
          // window.location.reload();
        }, 1000);
      }
    });
  }

  editPercentage() {
    this.criteria_percentage = +this.criteria_percentage / 100;
    this._businessman.updateWeight(this.criteria_id, +this.criteria_percentage, (responseExisted: ResponseCallbackModel) => {
      if(responseExisted.message !== 'Percentage Weight sucessfully updated') {
        // window.location.reload();
      }
    });
  }
  
  configureVideoWeight() {
    this.modal.open(ConfigureVideoWeightModal, CONFIGURE_VIDEO_WEIGHT_OPTIONS);
  }

  configureBusinessmanRatingWeight() {
    this.modal.open(ConfigureBusinessmanRatingWeightModal, CONFIGURE_BUSINESSMAN_RATING_WEIGHT_OPTIONS);
  }

}


import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../../shared.module';
import { EmbedVideoService } from 'ngx-embed-video';
import { AdminComponent } from './admin.component';

@NgModule({
  imports: [
    AdminRoutingModule,
    SharedModule,
  ],
  declarations: [AdminComponent],
  providers: [EmbedVideoService]
})
export class AdminModule { }

import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { YoutuberService } from '../../../../services/youtuber/youtuber.service';
import { CampaignService } from '../../../../services/campaign';
import { BusinessmanService } from '../../../../services/businessman';
import { ResponseCallbackModel, } from '../../../../models/response.model';
import { BusinessmanCampaignModel } from '../../../../models/campaign.model';
import { UserYoutuber, TopInfluencer, YoutuberMediakit } from '../../../../models/youtuber.model';
import { YoutubeDataAPI } from '../../../../services/youtube_data_api/youtube_data_api.service';
import { ActivityLogBusinessman } from '../../../../models';

declare var $: any;
declare var moment: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css', '../../../../app.component.css'],
  providers: [NgbDropdownConfig]
})
export class DashboardComponent implements OnInit {

  all_new_influencer: any;
  currentNoCampaigns: number;
  campaigns: Array<BusinessmanCampaignModel>;
  new_influencer: UserYoutuber;
  first_new_influencer: any;
  youtuberInfo: YoutuberMediakit;
  influencer: any;
  new_youtuber: Array<UserYoutuber>;
  ended_date_youtuber: any;
  date_now: any = moment().format('LL');
  showNoNewInfluencer: any;
  subscriberCount: number;

  all_top_influencer: any;
  top_influencer: Array<TopInfluencer>;
  top_youtuber_length: any;
  top_youtuber: any;
  first_top_youtuber: any;
  subscriber_count: any;



  constructor(config: NgbDropdownConfig,
    private authService: AuthService,
    private router: Router,
    private _youtuber: YoutuberService,
    private _businessman: BusinessmanService,
    private _youtube_data_api: YoutubeDataAPI,
    private _campaign: CampaignService) {

    this.new_influencer = new UserYoutuber();
    this.youtuberInfo = new YoutuberMediakit();
    this.all_new_influencer = [];
    this.new_youtuber = [];

    this.currentNoCampaigns = 0;
    this.showNoNewInfluencer = false;

    this.subscriberCount = 0;
    this.top_influencer = [];
    this.top_youtuber = [];
    this.all_top_influencer = [];
    this.first_top_youtuber = [];
    this.subscriber_count = [];
  }

  ngOnInit() {
    this._campaign.businessmanCampaignData.subscribe(list => {
      this.campaigns = list;
      console.log(this.campaigns);
      this.currentNoCampaigns = list.length;
    });
    this._businessman.getCurrentBusinessman((responseBusinessman: ResponseCallbackModel) => {
      if (!responseBusinessman.error && responseBusinessman.data) {
        // this._campaign.getCampaign(responseBusinessman.data.businessman_id);
        this._campaign.getYoutubersFromCampaign(responseBusinessman.data.businessman_id);
      }
    });

    //newinfluencer
    this._youtuber.getallYoutuberInfo((responseAllFeed: ResponseCallbackModel) => {
      this.new_youtuber = responseAllFeed.data;
      console.log(this.new_youtuber);
      if (this.new_youtuber.length > 0) {
        this.showNoNewInfluencer = false;
        for (let hot = 0; hot < this.new_youtuber.length; hot++) {
          if (hot == 0) {
            this.first_new_influencer = this.new_youtuber[hot]; // First hot campaign will be displayed active
            console.log(this.first_new_influencer);
          } else { // Insert the rest of the hot campaigns
            this.all_new_influencer.push(this.new_youtuber[hot]);
          }
        }
        console.log(this.all_new_influencer);
      } else {
        this.showNoNewInfluencer = true;
      }
    });

    //topinfluencer

    this._youtuber.getTopInfluencer((responseAllFeed: ResponseCallbackModel) => {
      this.top_youtuber = responseAllFeed.data;
      this.top_youtuber_length = this.top_youtuber.length;
      console.log();
      for (let item = 0; item < this.top_youtuber.length; item++) { // items, an array returned
        this._youtuber.getYoutuberInfoById(responseAllFeed.data[item].youtuber.youtuber_id, (responseMediaKit: ResponseCallbackModel) => {
          if (!responseMediaKit.error && responseMediaKit.data) {
            this.youtuberInfo = responseMediaKit.data;
            this._youtube_data_api.getYoutuberChannel(this.youtuberInfo.youtubeCh, (responseChannel: ResponseCallbackModel) => {
              this.subscriber_count = responseChannel.data.items;
              // for (let i = 0; i < this.subscriber_count.length; i++) { // items, an array returned
                this.subscriberCount =+ responseChannel.data.items[0].statistics.subscriberCount;
                console.log(responseAllFeed.data[item].youtuber.name+ "  " + this.subscriberCount);

              // }
            });
          }
        });
        if (item == 0) {
          this.first_top_youtuber = this.top_youtuber[item];
        } else {
          this.all_top_influencer.push(this.top_youtuber[item])
        }
      }

    });
    console.log('ahsr', this.all_top_influencer);


  }
  end_date_validate(ended_date_youtuber): boolean {
    return moment(ended_date_youtuber).isAfter(moment(this.date_now));
  }
}


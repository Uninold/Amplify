import { NgModule } from '@angular/core';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../../../../shared.module';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  imports: [
    DashboardRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [DashboardComponent],
  declarations: [DashboardComponent],
  providers: []
})
export class DashboardModule { }

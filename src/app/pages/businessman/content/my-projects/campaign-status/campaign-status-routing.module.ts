import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampaignStatusComponent } from './campaign-status.component';

const routes: Routes = [
  { path: '', component: CampaignStatusComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignStatusRoutingModule { }

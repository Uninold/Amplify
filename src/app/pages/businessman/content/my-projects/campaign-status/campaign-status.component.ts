import * as storage from 'store';
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { CampaignService } from '../../../../../services/campaign';
import { ResponseCallbackModel, AcceptedInterestModel } from '../../../../../models';
import { InterestService } from '../../../../../services/interest/interest.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InterestedModel, CampaignDetailModel, MyCampaignModel } from '../../../../../models/campaign.model';
import { SubmitCampaignModal, YoutuberViewProfileModal } from '../../../../../modals/youtuber';
import { EditCampaignModal } from '../../../../../modals/businessman';
import { INTEREST_MESSAGE_OPTIONS, SUBMIT_CAMPAIGN_OPTIONS, EDIT_CAMPAIGN_OPTIONS, YOUTUBER_VIEW_PROFILE_OPTIONS } from '../../../../../configs/modal-options.config';
import { SubmittedModal } from '../../../../../modals/businessman/submitted-modal/submitted-modal.component';
import { NotificationService } from '../../../../../services';
import { NotificationModel } from '../../../../../models/notification.model';
import { NotificationActivityService } from '../../../../../services/notification-activity';

declare var $: any;

@Component({
  selector: 'app-campaign-status',
  templateUrl: './campaign-status.component.html',
  styleUrls: ['./campaign-status.component.css', '../../../../../app.component.css'],
  providers: [NgbDropdownConfig]
})
export class CampaignStatusComponent implements OnInit {

  @Output() backToMain = new EventEmitter<boolean>();
  @Input() campaignId: any; // Global for this page
  activeTab: any;
  acceptedInterest: AcceptedInterestModel;
  acceptedAdvertisersCount: any;
  acceptedToWork: boolean;
  campaign_details: any;
  campaign_photos: any;
  google: any;
  notification = new NotificationModel();
  recommendedYoutuber: any;
  recommendedYoutuber_count: any;

  constructor(config: NgbDropdownConfig,
    private modal: NgbModal,
    private authService: AuthService,
    private router: Router,
    private _route: ActivatedRoute,
    private _campaign: CampaignService,
    private _interest: InterestService,
    private _notification: NotificationService,
    private _notificationActivity: NotificationActivityService) {
    _route.paramMap.subscribe((params: ParamMap) => {
      this.activeTab = params.get('roomStatus');
    });
    this.acceptedToWork = false;
    this.acceptedInterest = new AcceptedInterestModel();
  }

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
    this.instance();
  }

  instance() {
    this._campaign.getSpecificOnGoingCampaignOfBusinessmanWithAds(this.campaignId, (responseCampaign: ResponseCallbackModel) => {
      this.campaign_details = responseCampaign.data[0];
      this.acceptedAdvertisersCount = this.campaign_details.accepted_advertiser_count;
      console.log(this.campaign_details);
      this._campaign.getRecommendedYoutuber(this.campaign_details.category_id, (responseRecommend: ResponseCallbackModel) => {
        if (responseRecommend.message === 'Recommended Youtuber') {
          console.log(responseRecommend.data);
          this.recommendedYoutuber = responseRecommend.data;
          this.recommendedYoutuber_count = this.recommendedYoutuber.length;
          this._campaign.getPhotoCampaignToFeed(this.campaignId, (responsePhotos: ResponseCallbackModel) => {
            this.campaign_photos = responsePhotos.data;
            console.log(this.campaign_photos);
          });
        }
      });
    });
  }

  /* Modals */
  sendRecommendation(youtuber_id) {
    console.log(youtuber_id);
    this.google = storage.get('current_businessman');
    this.notification.notif_msg = 'recommended you';
    this.notification.notif_from = this.google.businessman_id;
    this.notification.notif_to = youtuber_id;
    this.notification.user_img = this.google.profile_picture;
    this.notification.name = this.google.business_name;
    this.notification.notif_subject = this.campaignId;
    this._notificationActivity.notification(this.notification);
  }
  editCampaign() {
    const editCampaignModal = this.modal.open(EditCampaignModal, EDIT_CAMPAIGN_OPTIONS);
    editCampaignModal.result.then((data) => {

    }, (reason) => {
      this.instance();
    });
    editCampaignModal.componentInstance.campaign_id = this.campaignId;
  }

  viewYoutuberProfile(youtuber_id: any) {
    let modal = this.modal.open(YoutuberViewProfileModal, YOUTUBER_VIEW_PROFILE_OPTIONS);
    modal.componentInstance.youtuber_id = youtuber_id;
  }

  /* ------ */

  addToProduction() {
    this._campaign.addToOnGoingProduction(this.campaignId, (responseUpdateStatus: ResponseCallbackModel) => {
      if (responseUpdateStatus.data.error == 0) {
        this._notification.success("Campaign is added to On Going Production.");
        window.location.reload();
      }
    });
  }

  acceptAdvertiser(advertiser, campaign, index) {
    if (this.acceptedAdvertisersCount < this.campaign_details.advertiser_needed) {
      console.log('acadvertiser', advertiser);
      console.log('accampaign', campaign);

      this.google = storage.get('current_businessman');
      this.notification.notif_msg = 'accepted your interest';
      this.notification.notif_from = this.google.businessman_id;
      this.notification.notif_to = advertiser.youtuber_id;
      this.notification.user_img = this.google.profile_picture;
      this.notification.name = this.google.business_name;
      this.notification.notif_subject = campaign.campaign_id;
      this._notificationActivity.notification(this.notification);

      this.acceptedInterest.youtuber_id = advertiser.youtuber_id;
      this.acceptedInterest.campaign_id = +campaign.campaign_id;
      this.acceptedInterest.message = advertiser.message;
      this.acceptedInterest.date_interested = advertiser.date_interested;
      this.acceptedInterest.submission_status_id = 2; // On Going Status
      this._interest.acceptedInterested(this.acceptedInterest, (responseAccept: ResponseCallbackModel) => {
        if (this.campaign_details.advertisers[index].interested === 0) {
          this.campaign_details.advertisers[index].interested = 1;
          ++this.acceptedAdvertisersCount; // Increment Current Accepted Advertisers
        } else {
          this.campaign_details.advertisers[index].interested = 0;
          --this.acceptedAdvertisersCount; // Increment Current Accepted Advertisers
        }
      });
    } else {
      this._notification.warning("You have reached maximum number of advertisers you need.");
    }
  }

  onBack() { // Back to Accordion
    this.backToMain.emit(false);
  }

  openSubmittedModal(campaign, advertiser) {
    this._campaign.getVideoWithYoutuberInfo(advertiser.youtuber_id, campaign.campaign_id, (response: ResponseCallbackModel) => {
      const submitCampaignModal = this.modal.open(SubmittedModal, SUBMIT_CAMPAIGN_OPTIONS);
      submitCampaignModal.componentInstance.campaign = response.data;
    });
  }

  removeAcceptAdvertiser(advertiser, campaign, index) {
    this.google = storage.get('current_businessman');
    this.notification.notif_msg = 'removed your interest';
    this.notification.notif_from = this.google.businessman_id;
    this.notification.notif_to = advertiser.youtuber_id;
    this.notification.user_img = this.google.profile_picture;
    this.notification.name = this.google.business_name;
    this.notification.notif_subject = campaign.campaign_id;
    this._notificationActivity.notification(this.notification);
    this.acceptedInterest.youtuber_id = advertiser.youtuber_id;
    this.acceptedInterest.campaign_id = +campaign.campaign_id;
    this.acceptedInterest.message = advertiser.message;
    this.acceptedInterest.date_interested = advertiser.date_interested;
    this.acceptedInterest.submission_status_id = 2;
    this._interest.acceptedInterested(this.acceptedInterest, (responseAccept: ResponseCallbackModel) => {
      if (this.campaign_details.advertisers[index].interested === 0) {
        this.campaign_details.advertisers[index].interested = 1;
        ++this.acceptedAdvertisersCount; // Increment Current Accepted Advertisers
      } else {
        this.campaign_details.advertisers[index].interested = 0;
        --this.acceptedAdvertisersCount; // Increment Current Accepted Advertisers
      }
    });
  }

  removeCampaign() { // Remove current campaign displayed in the page
    this._campaign.removeCampaign(this.campaign_details.campaign_id, (responseRemove: ResponseCallbackModel) => {
      window.location.reload();
    });
  }

  removeInterestedYoutuber(advertiser, campaign_details, index) {
    this.acceptedInterest.youtuber_id = advertiser.youtuber_id;
    this.acceptedInterest.campaign_id = +campaign_details.campaign_id;
    this.acceptedInterest.message = advertiser.message;
    this.acceptedInterest.date_interested = advertiser.date_interested;
    this._interest.removeInterestedYoutuber(this.acceptedInterest, (responseAccept: ResponseCallbackModel) => {
      for (let ads = 0; ads < campaign_details.advertisers.length; ads++) {
        if (advertiser.youtuber_id === campaign_details.advertisers[ads].youtuber_id) {
          campaign_details.advertisers.splice(ads, 1);
        }
      }
    });
  }

}

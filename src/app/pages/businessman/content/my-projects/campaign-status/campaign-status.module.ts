import { NgModule } from '@angular/core';
import { CampaignStatusRoutingModule } from './campaign-status-routing.module';
import { SharedModule } from '../../../../../shared.module';
import { CommonModule } from '@angular/common';
import { CampaignStatusComponent } from './campaign-status.component';

@NgModule({
  imports: [
    CampaignStatusRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [CampaignStatusComponent],
  declarations: [CampaignStatusComponent],
  providers: []
})
export class CampaignStatusModule { }

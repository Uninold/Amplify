import * as storage from 'store';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { YoutuberService } from '../../../../services/youtuber/youtuber.service';
import { ResponseCallbackModel } from '../../../../models/response.model';
import { CampaignService } from '../../../../services/campaign';

declare var $: any;
declare var moment: any;

@Component({
  selector: 'app-my-projects',
  templateUrl: './my-projects.component.html',
  styleUrls: ['./my-projects.component.css', '../../../../app.component.css'],
  providers: [NgbDropdownConfig]
})
export class MyProjectsComponent implements OnInit {

  campaignState: boolean = false;
  campaign_id: any;
  productionState: boolean = false;
  campaignStatusComponentView: boolean = false;
  productionStatusComponentView: boolean = false;
  onGoingCampaigns: any;
  onGoingProductions: any;
  filter: any = 'On Going Campaign';

  constructor(config: NgbDropdownConfig,
    private authService: AuthService,
    private router: Router,
    private _youtuber: YoutuberService,
    private _campaign: CampaignService) { }

  ngOnInit() {
    if(this.filter === 'On Going Campaign') {
      this.onGoingCampaign_func();
    }
  }

  /* From Event Emitter */

  onBack($event) {
    this.campaignStatusComponentView = $event; // false
    this.productionStatusComponentView = $event; // false
    this.campaignState = true;
    this.productionState = false;
  }

  /* Status View */

  openCampaignStatus(campaign_id) {
    // setTimeout(() => {
      this.campaign_id = campaign_id; // Campaign Id passed to campaign status component
      this.campaignStatusComponentView = true;
    // }, 2000);
  }

  openProductionStatus(campaign_id) {
    // setTimeout(() => {
      this.campaign_id = campaign_id;
      this.productionStatusComponentView = true;
    // }, 2000);
  }

  filterOut(chosen_filter) {
    this.filter = chosen_filter;
    if(chosen_filter === 'On Going Campaign') {
      this.onGoingCampaign_func();
    } else if(chosen_filter === 'On Going Production') {
      this.onGoingProduction_func();
    }
  }

  onGoingCampaign_func() { // On Going Campaign Display
    this.campaignState = true;
    this.productionState = false;
    const currentBusinessman = storage.get('current_businessman');
    this._campaign.getAllOnGoingCampaignOfBusinessman(currentBusinessman.businessman_id, (responseCampaign : ResponseCallbackModel) => {
      this.onGoingCampaigns = responseCampaign.data;
      for(let campaign_obj = 0; campaign_obj < this.onGoingCampaigns.length; campaign_obj++) {
        const campaign_deadline = moment(this.onGoingCampaigns[campaign_obj].campaign_deadline).format('LL');
        const date_now = moment().format('LL');
      }
    });
  }

  onGoingProduction_func() {
    this.campaignState = false;
    this.productionState = true;
    const currentBusinessman = storage.get('current_businessman');
    this._campaign.getAllOnGoingProductionOfBusinessman(currentBusinessman.businessman_id, (responseProduction : ResponseCallbackModel) => {
      this.onGoingProductions = responseProduction.data;
      console.log(this.onGoingProductions);
    });
  }
}

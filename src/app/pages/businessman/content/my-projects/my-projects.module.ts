import { NgModule } from '@angular/core';
import { MyProjectsRoutingModule } from './my-projects-routing.module';
import { SharedModule } from '../../../../shared.module';
import { CommonModule } from '@angular/common';
import { MyProjectsComponent } from './my-projects.component';
import { CampaignStatusModule } from './campaign-status/campaign-status.module';
import { ProductionStatusModule } from './production-status/production-status.module';

@NgModule({
  imports: [
    MyProjectsRoutingModule,
    SharedModule,
    CommonModule,
    CampaignStatusModule,
    ProductionStatusModule
  ],
  exports: [MyProjectsComponent],
  declarations: [MyProjectsComponent],
  providers: [
  ]
})
export class MyProjectsModule { }

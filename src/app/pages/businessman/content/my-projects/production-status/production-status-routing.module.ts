import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductionStatusComponent } from './production-status.component';

const routes: Routes = [
  { path: '', component: ProductionStatusComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionStatusRoutingModule { }

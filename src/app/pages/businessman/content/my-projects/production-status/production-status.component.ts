import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { CampaignService } from '../../../../../services/campaign';
import { ResponseCallbackModel } from '../../../../../models';
import { ReopenCampaignModal, BusinessmanRatingModal } from '../../../../../modals/businessman';
import { REOPEN_CAMPAIGN_OPTIONS, BUSINESSMAN_RATING_OPTIONS } from '../../../../../configs/modal-options.config';

declare var $: any;

@Component({
  selector: 'app-production-status',
  templateUrl: './production-status.component.html',
  styleUrls: ['./production-status.component.css', '../../../../../app.component.css'],
  providers: [NgbDropdownConfig]
})
export class ProductionStatusComponent implements OnInit {

  @Output() backToMain = new EventEmitter<boolean>();
  @Input() campaignId: any; // Global for this page
  production_campaign: any;

  constructor(config: NgbDropdownConfig,
    private modal: NgbModal,
    private _campaign: CampaignService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.loadAdvertisers();
    $('[data-toggle="tooltip"]').tooltip();
  }

  loadAdvertisers() {
    this._campaign.getSpecificOnGoingProductionOfBusinessmanWithAds(this.campaignId, (responseSpecific: ResponseCallbackModel) => {
      this.production_campaign = responseSpecific.data;
      for(let i=0; i<responseSpecific.data.length; i++) {
        this.production_campaign = responseSpecific.data[i];
      }
      console.log(this.production_campaign);
    });
  }

  /* Modals */

  reopenCampaign() {
    const editCampaignModal = this.modal.open(ReopenCampaignModal, REOPEN_CAMPAIGN_OPTIONS);
    editCampaignModal.componentInstance.campaign_id = this.campaignId;
  }
  
  removeCampaign() { // Remove current campaign displayed in the page
    this._campaign.removeCampaign(this.production_campaign.campaign_id, (responseRemove: ResponseCallbackModel) => {
      window.location.reload();
    });
  }

  openRating(interested_youtuber: any) {
    const businessmanRatingModal = this.modal.open(BusinessmanRatingModal, BUSINESSMAN_RATING_OPTIONS);
    businessmanRatingModal.componentInstance.interested_youtuber = interested_youtuber;
    businessmanRatingModal.result.then((data) => {

    }, (reason) => {
      this.loadAdvertisers();
    });
    businessmanRatingModal.componentInstance.done_rating.subscribe((data) => {
      console.log(data);
    });
  }

  /* ------ */

  onBack() { // Back to Accordion
    this.backToMain.emit(false);
  }
  
}

import { NgModule } from '@angular/core';
import { ProductionStatusRoutingModule } from './production-status-routing.module';
import { SharedModule } from '../../../../../shared.module';
import { CommonModule } from '@angular/common';
import { ProductionStatusComponent } from './production-status.component';

@NgModule({
  imports: [
    ProductionStatusRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [ProductionStatusComponent],
  declarations: [ProductionStatusComponent],
  providers: [
  ]
})
export class ProductionStatusModule { }

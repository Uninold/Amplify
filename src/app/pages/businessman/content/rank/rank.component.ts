import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { YoutuberService } from '../../../../services/youtuber/youtuber.service';
import { ResponseCallbackModel } from '../../../../models/response.model';

declare var $: any;

@Component({
  selector: 'app-rank',
  templateUrl: './rank.component.html',
  styleUrls: ['./rank.component.css', '../../../../app.component.css'],
  providers: [NgbDropdownConfig]
})
export class RankComponent implements OnInit {

  constructor(config: NgbDropdownConfig,
    private authService: AuthService,
    private router: Router,
    private _youtuber: YoutuberService) {}

  ngOnInit() {

  }
  
}

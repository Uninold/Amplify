import { NgModule } from '@angular/core';
import { RankRoutingModule } from './rank-routing.module';
import { SharedModule } from '../../../../shared.module';
import { CommonModule } from '@angular/common';
import { RankComponent } from './rank.component';

@NgModule({
  imports: [
    RankRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [RankComponent],
  declarations: [RankComponent],
  providers: []
})
export class RankModule { }

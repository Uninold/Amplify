import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, RouterLink } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { YoutuberService } from '../../../../services/youtuber/youtuber.service';
import { CampaignService } from '../../../../services/campaign/campaign.service';
import { YoutubeDataAPI } from '../../../../services/youtube_data_api/youtube_data_api.service';
import { ResponseCallbackModel } from '../../../../models/response.model';
import { BusinessmanCampaignModel } from '../../../../models/campaign.model';
import { YoutubeVideoStatistics } from '../../../../models/youtube_data.model';
import { OpenVideoModal } from '../../../../modals/youtuber';
import { NotificationService } from '../../../../services/notification.service';
import { StatisticsDate, AdvertiserCampaignModel } from '../../../../models/campaign.model';
import { EmbedVideoService } from 'ngx-embed-video';
import { OPEN_VIDEO_OPTIONS } from '../../../../configs/modal-options.config';
import * as storage from 'store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { equal } from 'assert';
declare var moment: any;

declare var $: any;

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css', '../../../../app.component.css'],
  providers: [NgbDropdownConfig]
})
export class ReportsComponent implements OnInit {
  first_date: Date;
  months = ['01/2/2222',
            '02/2/2222',
            '03/2/2222',
            '04/22/2222',
            '05/22/2222',
            '06/22/2222',
            '07/2/2222',
            '08/22/2222',
            '09/22/2222',
            '10/22/2222',
            '11/22/2222',
            '12/22/2222'
          ];
  currentCampaign = new BusinessmanCampaignModel();
  totalViewsCampaign: number;
  requestData: Array<object> = [];
  releasedData: Array<number> = [];
  campaigns: Array<BusinessmanCampaignModel>;
  currentNoCampaigns: number;
  iframe_html: any;
  video: YoutubeVideoStatistics;
  advertisers: Array<AdvertiserCampaignModel> = [];
  constructor(config: NgbDropdownConfig,
    private authService: AuthService,
    private router: Router,
    private modal: NgbModal,
    private _youtuber: YoutuberService,
    private _campaign: CampaignService,
    private _notification: NotificationService,
    private embedService: EmbedVideoService,
    private youtubeApi: YoutubeDataAPI) {
      this.currentNoCampaigns = 0;
      this.video = new YoutubeVideoStatistics();
    }

  ngOnInit() {
    $('.launch-modal').on('click', function(e) {
      e.preventDefault();
      $( '#' + $(this).data('modal-id') ).modal();
  });
  $('.close').click(function() {
    $('iframe').attr('src', $('iframe').attr('src'));
    });
    const currentBusinessman = storage.get('current_businessman');
    const date_now = moment().format('LL');
    const day_now = moment(date_now).date();
    const month_now = Number(moment(date_now).month());
    const temp_requestData = [80, 90, 70, 50, 30, null, null, null, null];
    const temp_releasedData = [10, null, 50, 5, 28, null, null, null, null];
    const vitalsDate = [11, 15, 18, 23, 24];
    this.totalViewsCampaign = 0;
    this._campaign.businessmanCampaignData.subscribe(list => {
      this.campaigns = list;
      console.log('log', list);
      if (list.length !== 0) {
        this.getVideoStatistics(this.campaigns[0], '01');
        this.currentCampaign = this.campaigns[0];
      }
      console.log('list of reports', this.requestData);
      this.currentNoCampaigns = list.length;
    });
  this._campaign.getYoutubersFromCampaign(currentBusinessman.businessman_id);
  this._campaign.getAllVideoId( (response: ResponseCallbackModel) => {
    console.log(response.data);
    for (const video of response.data) {
            this.youtubeApi.getVideo(video.video_id, (responseVideo: ResponseCallbackModel) => {
              if (responseVideo.data) {
                this.video.video_id = video.video_id;
                this.video.viewCount = responseVideo.data.items[0].statistics.viewCount;
                this.video.likeCount = responseVideo.data.items[0].statistics.likeCount;
                this.video.dislikeCount = responseVideo.data.items[0].statistics.dislikeCount;
                this._youtuber.addYoutubeVideoStatistics(this.video, (responseVideoAdding: ResponseCallbackModel) => {
                });
              }
            });
    }
  });
  }
  getVideoStatistics(campaign, month) {
    this.currentCampaign = campaign;
    this.totalViewsCampaign = 0;
    this.requestData = [];
    console.log('hlo', campaign);
      this.advertisers = campaign.advertisers;
      for (const j of campaign.advertisers) {
        const temp_statistics: Array<StatisticsDate> = [];
        const statistics: Array<number> = [];
        console.log('len', j.video_statistics.length);
        this.totalViewsCampaign += +j.video_statistics[j.video_statistics.length - 1].views;
        console.log('advert', this.totalViewsCampaign);

            for (const count of j.video_statistics) {
              temp_statistics.push({count: +count.views, date_added: count.date_added});
            }
            console.log('advertiser', temp_statistics);
          this.first_date = new Date(temp_statistics[0].date_added);
          let counter = 0;
      for (let x = 1; x <= moment(month.substring(0, 2)).daysInMonth(); x++) {
        let added = false;
          for (let temp of temp_statistics) {
            console.log('counter', counter);
            console.log('lengthta', temp_statistics.length);
            let newDate = new Date(temp.date_added);
            if (x === +newDate.getDate() && +month.substring(0, 2) === +newDate.getMonth() + 1) {
              statistics.push(temp.count);
              added = true;
              }
          }
          console.log('2s', x);
          if (added === false) {
            statistics.push(null);
          }
      if (temp_statistics.length <= counter) {
        break;
      }
    }
    const letters = '0123456789ABCDEF'.split('');
    let rancolor = '#';
      for (let colorcount = 0; colorcount < 6; colorcount++) {
          rancolor += letters[Math.round(Math.random() * 15)];
      }
      this.requestData.push({youtuber: j.firstName + ' ' + j.lastName, count: statistics,  date_first_added: this.first_date, color: rancolor});
      }
  }
  getVideo(advertisers) {
    this.iframe_html = this.embedService.embed_youtube(advertisers.video_statistics[0].video_id, { attr: { width: 500, height: 300 } });
    console.log('iframe', this.iframe_html);
  }
  openVideoModal(advertisers) {
    console.log('lol', advertisers);
    if (advertisers.video_statistics.length > 0 ) {
      const submitCampaignModal = this.modal.open(OpenVideoModal, OPEN_VIDEO_OPTIONS);
      submitCampaignModal.componentInstance.video_id = advertisers.video_statistics[0].video_id;
    } else {
      this._notification.warning('No Video Submitted');
    }
}
changeDate(month: string) {
    console.log('month', moment(month.substring(0, 2)).daysInMonth() );
    this.getVideoStatistics(this.currentCampaign, month);
  }
}

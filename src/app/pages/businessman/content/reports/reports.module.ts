import { NgModule } from '@angular/core';
import { ReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from '../../../../shared.module';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { ChartsModule } from '../../../../components/custom/charts/charts.module';


@NgModule({
  imports: [
    ReportsRoutingModule,
    SharedModule,
    ChartsModule,
    CommonModule
  ],
  exports: [ReportsComponent],
  declarations: [ReportsComponent],
  providers: [
  ]
})
export class ReportsModule { }

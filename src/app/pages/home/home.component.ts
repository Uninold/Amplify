import * as storage from 'store';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { YoutubeDataAPI } from '../../services/youtube_data_api/youtube_data_api.service';
import { ResponseCallbackModel } from '../../models/response.model';
import { UserYoutuber, YoutuberMediakit, YoutubeAPIKey } from '../../models';
import { NotificationService, AuthServiceL } from '../../services';
import { CampaignService } from '../../services/campaign';
import { YoutuberService } from '../../services/youtuber';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../../app.component.css']
})

export class HomeComponent implements OnInit {

  campaigns: any;
  currentPage: any;
  google: any;
  mediaKit: any;
  user: any;
  userYoutuber: any;
  page_selected: any;
  youtuberInfo: any;
  youtubeStats: any;

  constructor(
    private _notification: NotificationService,
    private _auth: AuthServiceL,
    private authService: AuthService,
    private _router: Router,
    private _campaign: CampaignService,
    private _youtube_data_api: YoutubeDataAPI,
    private _youtuber: YoutuberService) {
    this.userYoutuber = new UserYoutuber();
    this.youtuberInfo = new YoutuberMediakit();
    this.user = new SocialUser();
    this.mediaKit = false;
    this.campaigns = [];
  }

  ngOnInit() {
    this.page_selected = 'dashboard';
    this.currentPage = 'Dashboard';
    this.google = storage.get('current_youtuber');
    this._youtuber.getYoutuberInfoById(this.google.id, (responseMediaKit: ResponseCallbackModel) => {
      if (!responseMediaKit.error && responseMediaKit.data) {
        this.youtuberInfo = responseMediaKit.data;
      }
    });
  }

  goTo(selected) {
    switch (selected) {
      case 'dashboard': {
        this.page_selected = selected;
        this.currentPage = 'Dashboard';
        break;
      }
      case 'hot_campaigns': {
        this.page_selected = selected;
        this.currentPage = 'Hot Campaigns';
        break;
      }
      case 'my_campaigns': {
        // Implementation here
        this.page_selected = selected;
        this.currentPage = 'My Campaigns';
        break;
      }
      case 'reports': {
        // Implementation here
        this.page_selected = selected;
        this.currentPage = 'Reports';
        break;
      }
      case 'rank': {
        // Implementation here
        this.page_selected = selected;
        this.currentPage = 'World Rank';
        break;
      }
      case 'settings': {
        // Implementation here
        this.page_selected = selected;
        break;
      }
      case 'logout': {
        this._auth.logOut((response: ResponseCallbackModel) => {
          this._router.navigate(['/login']);
        });
        break;
      }
    }
  }

  signOut(): void {
    this._auth.logOut((response: ResponseCallbackModel) => {
      this._router.navigate(['/login']);
    });
    setTimeout(() => {
      this.authService.signOut().then(res => {
        storage.clearAll();
      });
    }, 2000);
  }

}


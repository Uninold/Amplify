import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../../shared.module';
import { EmbedVideoService } from 'ngx-embed-video';
import { HomeComponent } from './home.component';
import { YoutuberDashboardModule } from './youtuber-dashboard/youtuber-dashboard.module';
import { HotCampaignsModule } from './hot-campaigns/hot-campaigns.module';
import { MyCampaignsModule } from './my-campaigns/my-campaigns.module';

@NgModule({
  imports: [
    HomeRoutingModule,
    SharedModule,
    YoutuberDashboardModule,
    HotCampaignsModule,
    MyCampaignsModule
  ],
  declarations: [HomeComponent],
  providers: [EmbedVideoService]
})
export class HomeModule { }

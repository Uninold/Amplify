import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HotCampaignsComponent } from './hot-campaigns.component';
import { HomeComponent } from '../../home/home.component';
const routes: Routes = [
  { path: '', component: HotCampaignsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotCampaignsRoutingModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotCampaignsComponent } from './hot-campaigns.component';

describe('HotCampaignsComponent', () => {
  let component: HotCampaignsComponent;
  let fixture: ComponentFixture<HotCampaignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotCampaignsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotCampaignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

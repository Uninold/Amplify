import * as storage from 'store';
import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { InterestedModel, CampaignDetailModel } from '../../../models/campaign.model';
import { CampaignDetailsModal, InterestMessageModal } from '../../../modals/youtuber';
import { CAMPAIGN_DETAILS_OPTIONS, INTEREST_MESSAGE_OPTIONS } from '../../../configs/modal-options.config';
import { ResponseCallbackModel } from '../../../models';
import { InterestService } from '../../../services/interest/interest.service';
import { NotificationModel } from '../../../models/notification.model';
import { NotificationActivityService } from '../../../services/notification-activity';
declare var $: any;

@Component({
    selector: 'app-hot-campaigns',
    templateUrl: './hot-campaigns.component.html',
    styleUrls: ['./hot-campaigns.component.css', '../../../app.component.css'],
    providers: []
})
export class HotCampaignsComponent implements OnInit {
    all_campaigns: Array<CampaignDetailModel>;
    all_category_campaign: Array<CampaignDetailModel>;
    filter: any = 'ALL';
    google: any;
    interestedDetails: InterestedModel;
    campaign_category: any;
    category_id: any;
    starting_budget_input: any;
    ending_budget_input: any;
    notification = new NotificationModel();
    constructor(
        private modal: NgbModal,
        private _campaign: CampaignService,
        private _interest: InterestService,
        private _notification: NotificationActivityService
    ) {
        this.interestedDetails = new InterestedModel();
        this.all_campaigns = [];
        this.all_category_campaign = [];
    }

    ngOnInit() {

        $('.dropdown-toggle').dropdown();
        this.google = storage.get('current_youtuber');
        this._campaign.getAllCampaignToFeed(this.google.id, (responseAllFeed: ResponseCallbackModel) => {
            this.all_campaigns = responseAllFeed.data;
        });
        this._campaign.getAllCategoryInCampaign((responseAllFeed: ResponseCallbackModel) => {
            this.all_category_campaign = responseAllFeed.data;
        });
    }

    filterByCategory(event) {
        const category_name = event.target.value;
        switch (category_name) {
            case 'All': this.category_id = 0;
                this.google = storage.get('current_youtuber');
                this._campaign.getAllCampaignToFeed(this.google.id, (responseAllFeed: ResponseCallbackModel) => {
                    this.all_campaigns = responseAllFeed.data;
                });
                break;
            case 'Comedy': this.selectedCategory(1);
                this.category_id = 1; break;
            case 'Drama': this.selectedCategory(2);
                this.category_id = 2; break;
            case 'Education': this.selectedCategory(3);
                this.category_id = 3; break;
            case 'Entertainment': this.selectedCategory(4);
                this.category_id = 4; break;
            case 'Family': this.selectedCategory(5);
                this.category_id = 5; break;
            case 'Gaming': this.selectedCategory(6);
                this.category_id = 6; break;
            case 'Howto and Styles': this.selectedCategory(7);
                this.category_id = 7; break;
            case 'Music': this.selectedCategory(8);
                this.category_id = 8; break;
            case 'Sports': this.selectedCategory(9);
                this.category_id = 9; break;
            case 'Travel and Events': this.selectedCategory(10);
                this.category_id = 10; break;
            case 'People and Blogs': this.selectedCategory(11);
                this.category_id = 11; break;
        }
        return this.category_id;
    }

    selectedCategory(category) {
        this.google = storage.get('current_youtuber');
        this._campaign.getAllCampaignToFeedByCategory(this.google.id, category, (responseAllFeed: ResponseCallbackModel) => {
            this.all_campaigns = responseAllFeed.data;
        });
    }
    budgetRange(starting_budget, ending_budget) {
        this.google = storage.get('current_youtuber');
        this._campaign.getAllCampaignToFeedByBudget(this.google.id, starting_budget, ending_budget, (responseAllFeed: ResponseCallbackModel) => {
            this.all_campaigns = responseAllFeed.data;
        });
    }
    filterCategoryAndBudget(category_id, starting_budget, ending_budget) {
        this.google = storage.get('current_youtuber');
        this._campaign.getAllCampaignToFeedByCategoryBudget(this.google.id, category_id, starting_budget, ending_budget, (responseAllFeed: ResponseCallbackModel) => {
            this.all_campaigns = responseAllFeed.data;
        });
    }
    filter_camapign(category_name) {
        switch (category_name) {
            case 'All': {
                this.google = storage.get('current_youtuber');
                this._campaign.getAllCampaignToFeed(this.google.id, (responseAllFeed: ResponseCallbackModel) => {
                    this.all_campaigns = responseAllFeed.data;
                    console.log(this.all_campaigns);
                });
            }
                break;
            case 'Comedy': {
                this.category_id = 1;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                }
            }
                break;
            case 'Drama': {
                this.category_id = 2;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                }
                break;
            }
            case 'Education': {
                this.category_id = 3;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
            case 'Entertainment': {
                this.category_id = 4;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
            case 'Family': {
                this.category_id = 5;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
            case 'Gaming': {
                this.category_id = 6;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
            case 'Howto and Styles': {
                this.category_id = 7;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
            case 'Music': {
                this.category_id = 8;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
            case 'Sports': {
                this.category_id = 9;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
            case 'Travel and Events': {
                this.category_id = 10;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
            case 'People and Blogs': {
                this.category_id = 11;
                if (this.starting_budget_input != null && this.ending_budget_input != null) {
                    this.filterCategoryAndBudget(this.category_id, this.starting_budget_input, this.ending_budget_input);
                } else {
                    this.selectedCategory(this.category_id);
                } break;
            }
        }
        return this.category_id;
    }
    filter_budget() {
        if (this.starting_budget_input != null && this.ending_budget_input != null) {
            this.budgetRange(this.starting_budget_input, this.ending_budget_input);
        }
    }
    reset() {
        this.campaign_category = null;
        this.starting_budget_input = null;
        this.ending_budget_input = null;
        this.google = storage.get('current_youtuber');
        this._campaign.getAllCampaignToFeed(this.google.id, (responseAllFeed: ResponseCallbackModel) => {
            this.all_campaigns = responseAllFeed.data;
        });
    }

    openViewDetailsModal(campaign_id) {
        const tempModal = this.modal.open(CampaignDetailsModal, CAMPAIGN_DETAILS_OPTIONS);
        tempModal.componentInstance.campaign_id = campaign_id;
    }

    interested(campaign: CampaignDetailModel, index: number): void {
        console.log('cams', campaign);
        if (this.all_campaigns[index].interested === 0) { // Opens modal, send message, then interested.
            const messageModal = this.modal.open(InterestMessageModal, INTEREST_MESSAGE_OPTIONS);
            messageModal.componentInstance.campaign = campaign;
            messageModal.componentInstance.interested = campaign.interested;
            messageModal.componentInstance.emitService.subscribe((emmitedValue) => {
                this.all_campaigns[index].interested = emmitedValue;
            });
        } else { // If youtuber is Uninterested.
            this.google = storage.get('current_youtuber');
            this.interestedDetails.campaign_id = campaign.campaign_id;
            this.interestedDetails.youtuber_id = this.google.id;
            this.interestedDetails.message = '';
            this._interest.interested(this.interestedDetails);
            this.all_campaigns[index].interested = 0;
        }
    }
    
    notificationUninterested(campaign) {
        this._campaign.getCampaignDetails(campaign.campaign_id, (responseAllFeed: ResponseCallbackModel) => {
            if (responseAllFeed.data) {
                this.google = storage.get('current_youtuber');
                this.notification.notif_msg = 'is not interested anymore';
                this.notification.notif_from = this.google.id;
                this.notification.notif_to =  responseAllFeed.data[0].businessman_id;
                this.notification.user_img = this.google.photoUrl;
                this.notification.name = this.google.name;
                this.notification.notif_subject = campaign.campaign_id;
                this._notification.notification(this.notification);
            }
        });
    }
}

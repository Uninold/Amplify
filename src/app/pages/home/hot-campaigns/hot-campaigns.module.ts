import { NgModule } from '@angular/core';
import { HotCampaignsRoutingModule } from './hot-campaigns-routing.module';
import { SharedModule } from '../../../shared.module';
import { CommonModule } from '@angular/common';
import { HotCampaignsComponent } from './hot-campaigns.component';

@NgModule({
  imports: [
    HotCampaignsRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [HotCampaignsComponent],
  declarations: [HotCampaignsComponent],
  providers: [
  ]
})
export class HotCampaignsModule { }

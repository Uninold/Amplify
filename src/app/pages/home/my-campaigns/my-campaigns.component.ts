import * as storage from 'store';
import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from '../../../services/campaign';
import { InterestedModel, CampaignDetailModel, MyCampaignModel } from '../../../models/campaign.model';
import { SubmittedModal } from '../../../modals/businessman';
import { CAMPAIGN_DETAILS_OPTIONS, INTEREST_MESSAGE_OPTIONS, SUBMIT_CAMPAIGN_OPTIONS } from '../../../configs/modal-options.config';
import { ResponseCallbackModel} from '../../../models';
import { SubmitCampaignModal } from '../../../modals/youtuber';

declare var $: any;

@Component({
    selector: 'app-my-campaigns',
    templateUrl: './my-campaigns.component.html',
    styleUrls: ['./my-campaigns.component.css', '../../../app.component.css'],
    providers: []
})
export class MyCampaignsComponent implements OnInit {
    all_campaigns: Array<CampaignDetailModel>;
    my_campaigns: Array<MyCampaignModel>;
    filter: any = 'ALL';
    google: any;
    interestedDetails: InterestedModel;
    my_campaigns_existence: any;

    constructor(
        private modal: NgbModal,
        private _campaign: CampaignService
    ) {
        this.interestedDetails = new InterestedModel();
        this.all_campaigns = [];
        this.my_campaigns = [];
    }

    ngOnInit() {
        $('.dropdown-toggle').dropdown();
        this.loadCampaigns();
    }

    loadCampaigns() {
        this.google = storage.get('current_youtuber');
        // this._campaign.getAllCampaignToFeed(this.google.id , (responseAllFeed: ResponseCallbackModel) => {
        //     this.all_campaigns = responseAllFeed.data;
        //     console.log(responseAllFeed.data) ;
        // });
        this._campaign.getMyCampaignsForYoutuber(this.google.id , (responseMyCampaigns: ResponseCallbackModel) => {
            this.my_campaigns = responseMyCampaigns.data;
            console.log(this.my_campaigns);
            if(this.my_campaigns.length > 0) {
                this.my_campaigns_existence = true;
            } else {
                this.my_campaigns_existence = false;
            }
        });
    }

    filterOut(chosen_filter) {
        this.filter = chosen_filter;
    }

    openSubmitCampaignModal(campaign) {
        const submitCampaignModal = this.modal.open(SubmitCampaignModal, SUBMIT_CAMPAIGN_OPTIONS);
        submitCampaignModal.result.then((data) => {

        }, (reason) => {
          this.loadCampaigns();
        });
        submitCampaignModal.componentInstance.campaign = campaign;
    }
}

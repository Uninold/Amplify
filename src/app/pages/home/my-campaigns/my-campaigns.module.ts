import { NgModule } from '@angular/core';
import { MyCampaignsRoutingModule } from './my-campaigns-routing.module';
import { SharedModule } from '../../../shared.module';
import { CommonModule } from '@angular/common';
import { MyCampaignsComponent } from './my-campaigns.component';

@NgModule({
  imports: [
    MyCampaignsRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [MyCampaignsComponent],
  declarations: [MyCampaignsComponent],
  providers: []
})
export class MyCampaignsModule { }

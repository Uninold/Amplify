import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { YoutuberReportsComponent } from './reports.component';

const routes: Routes = [
  { path: '', component: YoutuberReportsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class YoutuberReportsRoutingModule { }

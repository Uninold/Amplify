import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutuberReportsComponent } from './reports.component';

describe('YoutuberReportsComponent', () => {
  let component: YoutuberReportsComponent;
  let fixture: ComponentFixture<YoutuberReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoutuberReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutuberReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

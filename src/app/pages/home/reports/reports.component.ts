import * as storage from 'store';
import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { ResponseCallbackModel} from '../../../models';
import { YoutuberService } from '../../../services/youtuber';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OpenVideoModal, CampaignDetailsModal } from '../../../modals/youtuber';
import { OPEN_VIDEO_PROFILE_OPTIONS, CAMPAIGN_DETAILS_OPTIONS } from '../../../configs/modal-options.config';

declare var $: any;

@Component({
    selector: 'app-youtuber-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.css', '../../../app.component.css'],
    providers: []
})
export class YoutuberReportsComponent implements OnInit {

    total_rating: any = 5;
    drop: any;
    report_details: any;
    report_list: any;
    report_existence: any;

    constructor(
        private _youtuber: YoutuberService,
        private modal: NgbModal
    ) {
        this.report_list = [];
    }

    ngOnInit() {
        this.drop = false;
        let youtuber = storage.get('current_youtuber');
        this._youtuber.getYoutuberReportList(youtuber.id, (response: ResponseCallbackModel) => {
            if(response.message == 'Youtuber reports retrived') {
                this.report_list = response.data;
                console.log(this.report_list);
                if(this.report_list.length > 0) { // If there are reports, displays the reports.
                    this.report_existence = true;
                    this._youtuber.getYoutuberReportDetails(this.report_list[0]['accepted_interest_id'], (responseAI: ResponseCallbackModel) => {
                        if(responseAI.message == 'Youtuber report details retrived') {
                            this.report_details = responseAI.data;
                            this.total_rating = (this.report_details.businessman_rating.action_oriented + this.report_details.businessman_rating.brand_innovation +
                                                this.report_details.businessman_rating.brand_quality + this.report_details.businessman_rating.brand_service +
                                                this.report_details.businessman_rating.credibility + this.report_details.businessman_rating.engaging +
                                                this.report_details.businessman_rating.impression + this.report_details.businessman_rating.integrated +
                                                this.report_details.businessman_rating.significance) / 9;
                        }
                    });
                } else { // If none, displays notes.
                    this.report_existence = false;
                }
            }
        });
    }

    dropdown() {
        this.drop = !this.drop;
        $('#collapse').slideToggle('fast');
    }

    viewReport(accepted_interest_id: any) {
        this._youtuber.getYoutuberReportDetails(accepted_interest_id, (response: ResponseCallbackModel) => {
            if(response.message == 'Youtuber report details retrived') {
                this.report_details = response.data;
            }
        });
    }

    viewDetails(campaign_id: any) {
        let modal = this.modal.open(CampaignDetailsModal, CAMPAIGN_DETAILS_OPTIONS);
        modal.componentInstance.campaign_id = campaign_id;
    }

    openVideo(video_id: any) {
        let modal = this.modal.open(OpenVideoModal, OPEN_VIDEO_PROFILE_OPTIONS);
        modal.componentInstance.video_id = video_id;
    }

}

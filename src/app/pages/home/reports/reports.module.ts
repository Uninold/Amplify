import { NgModule } from '@angular/core';
import { YoutuberReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from '../../../shared.module';
import { CommonModule } from '@angular/common';
import { YoutuberReportsComponent } from './reports.component';

@NgModule({
  imports: [
    YoutuberReportsRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [YoutuberReportsComponent],
  declarations: [YoutuberReportsComponent],
  providers: []
})
export class YoutuberReportsModule { }

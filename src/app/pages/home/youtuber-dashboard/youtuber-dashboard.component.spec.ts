import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutuberDashboardComponent } from './youtuber-dashboard.component';

describe('YoutuberDashboardComponent', () => {
  let component: YoutuberDashboardComponent;
  let fixture: ComponentFixture<YoutuberDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoutuberDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutuberDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

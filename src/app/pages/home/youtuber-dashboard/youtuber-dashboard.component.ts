import { Component, OnInit, NgModule } from '@angular/core';
import * as storage from 'store';
import { Router } from '@angular/router';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { MyCampaignModel, CampaignDetailModel, InterestedModel } from '../../../models/campaign.model';
import { YoutuberService } from '../../../services/youtuber/youtuber.service';
import { YoutubeDataAPI } from '../../../services/youtube_data_api/youtube_data_api.service';
import { ResponseCallbackModel } from '../../../models/response.model';
import { UserYoutuber, YoutuberMediakit, YoutubeAPIKey } from '../../../models';
import { NotificationService, AuthServiceL } from '../../../services';
import { CampaignService } from '../../../services/campaign';
import { HotCampaignService } from '../../../services/hot-campaign';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CampaignDetailsModal, InterestMessageModal, MediakitModal } from '../../../modals/youtuber';
import { CAMPAIGN_DETAILS_OPTIONS, INTEREST_MESSAGE_OPTIONS, MEDIAKIT_MODAL_OPTIONS } from '../../../configs/modal-options.config';
import { InterestService } from '../../../services/interest';
declare var $: any;

@Component({
    selector: 'app-youtuber-dashboard',
    templateUrl: './youtuber-dashboard.component.html',
    styleUrls: ['./youtuber-dashboard.component.css', '../../../app.component.css'],
    providers: []
})
export class YoutuberDashboardComponent implements OnInit {

    google: any;
    first_hot_campaign: any;
    all_hot_campaigns: any;
    data_hot_campaigns: any;
    interestedDetails: InterestedModel;
    user: SocialUser;
    mediaKit: boolean;
    userYoutuber: UserYoutuber;
    youtuberInfo: YoutuberMediakit;
    youtubeStats: any;
    campaigns: any;
    viewCount: number;
    subscriberCount: number;
    my_campaigns: Array<MyCampaignModel>;
    showNoHotCampaign: boolean = false;

    constructor(
        private _notification: NotificationService,
        private _auth: AuthServiceL,
        private authService: AuthService,
        private router: Router,
        private _youtuber: YoutuberService,
        private _campaign: CampaignService,
        private _hotcampaign: HotCampaignService,
        private _youtube_data_api: YoutubeDataAPI,
        private modal: NgbModal,
        private _interest: InterestService) {
        this.userYoutuber = new UserYoutuber();
        this.youtuberInfo = new YoutuberMediakit();
        this.user = new SocialUser();
        this.interestedDetails = new InterestedModel();
        this.mediaKit = false;
        this.campaigns = [];
        this.subscriberCount = 0;
        this.viewCount = 0;
        this.my_campaigns = [];
        this.data_hot_campaigns = [];
        this.all_hot_campaigns = [];
    }

    ngOnInit() {
        this.google = storage.get('current_youtuber');
        this._youtuber.getYoutuberInfoById(this.google.id, (responseMediaKit: ResponseCallbackModel) => {
            if (!responseMediaKit.error && responseMediaKit.data) {
                this.youtuberInfo = responseMediaKit.data;

                this._youtube_data_api.getYoutuberChannel(this.youtuberInfo.youtubeCh, (responseChannel: ResponseCallbackModel) => {
                    if (!responseChannel.error && responseChannel.data) {
                        for (let item = 0; item < responseChannel.data.items.length; item++) { // items, an array returned
                            this.subscriberCount = +responseChannel.data.items[item].statistics.subscriberCount;
                            this.viewCount = +responseChannel.data.items[item].statistics.viewCount;
                        }
                    }
                });
            }
        });
        this._campaign.getMyCampaignsForYoutuber(this.google.id, (responseMyCamapains: ResponseCallbackModel) => {
            this.my_campaigns = responseMyCamapains.data;
        });
        this._hotcampaign.displayHotCampaigns(this.google.id, (responseHotCampaign: ResponseCallbackModel) => {
            this.data_hot_campaigns = responseHotCampaign.data;
            console.log('this.data_hot_campaigns', this.data_hot_campaigns);
            if (this.data_hot_campaigns.length > 0) {
                this.showNoHotCampaign = false;
                for (let hot = 0; hot < this.data_hot_campaigns.length; hot++) {
                    if (hot == 0) {
                        this.first_hot_campaign = this.data_hot_campaigns[hot]; // First hot campaign will be displayed active
                        console.log(this.first_hot_campaign);
                    } else { // Insert the rest of the hot campaigns
                        this.all_hot_campaigns.push(this.data_hot_campaigns[hot]);
                    }
                }
                console.log(this.all_hot_campaigns);
            } else {
                this.showNoHotCampaign = true;
            }
        });
    }

    openViewDetailsModal(campaign_id) {
        const tempModal = this.modal.open(CampaignDetailsModal, CAMPAIGN_DETAILS_OPTIONS);
        tempModal.componentInstance.campaign_id = campaign_id;
    }

    interested(campaign: CampaignDetailModel, index: number): void {
        if (this.data_hot_campaigns[index].interested === 0) { // Opens modal, send message, then interested.
            const messageModal = this.modal.open(InterestMessageModal, INTEREST_MESSAGE_OPTIONS);
            messageModal.componentInstance.campaign = campaign;
            messageModal.componentInstance.interested = campaign.interested;
            messageModal.componentInstance.emitService.subscribe((emmitedValue) => {
                this.data_hot_campaigns[index].interested = emmitedValue;
            });
        } else { // If youtuber is Uninterested.
            this.google = storage.get('current_youtuber');
            this.interestedDetails.campaign_id = campaign.campaign_id;
            this.interestedDetails.youtuber_id = this.google.id;
            this.interestedDetails.message = '';
            this._interest.interested(this.interestedDetails);
            this.data_hot_campaigns[index].interested = 0;
        }
    }

}

import { NgModule } from '@angular/core';
import { YoutuberDashboardRoutingModule } from './youtuber-dashboard-routing.module';
import { SharedModule } from '../../../shared.module';
import { CommonModule } from '@angular/common';
import { YoutuberDashboardComponent } from './youtuber-dashboard.component';

@NgModule({
  imports: [
    YoutuberDashboardRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [YoutuberDashboardComponent],
  declarations: [YoutuberDashboardComponent],
  providers: [
  ]
})
export class YoutuberDashboardModule { }

import * as storage from 'store';
import { Component, OnInit, Output, NgModule } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { LoginOpt } from 'angularx-social-login';
import { YoutuberService } from '../../services/youtuber/youtuber.service';
import { AuthServiceL, NotificationService } from '../../services';
import { BusinessmanService } from '../../services/businessman/businessman.service';
import { ResponseCallbackModel, ResponseModel } from '../../models/response.model';
import { UserBusinessman, ActivityLogBusinessman } from '../../models/businessman.model';
import { LoginModel, AuthModel } from '../../models/auth.model';

import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegistrationModal } from './../../modals/businessman/registration-modal/registration-modal.component';
import { REGISTRATION_OPTIONS, MEDIAKIT_MODAL_OPTIONS } from '../../../app/configs/modal-options.config';
import { YoutubeAPIKey, YoutubeVideo, UserModel } from '../../models';
import { YoutubeDataAPI } from '../../services/youtube_data_api';
import { MediakitModal } from '../../modals/youtuber';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '../../app.component.css']
})

export class LoginComponent implements OnInit {

  authData: LoginModel;
  adminAuthData: LoginModel;
  authModel: AuthModel;
  businessman_documents: any = [];
  retype_password: string;
  pageToken: any;
  private loggedIn: boolean;
  prompt: 'select_account';
  userBusinessman: UserBusinessman;
  youtuber: any;
  youtubeAPIkey: YoutubeAPIKey;
  youtubeVideo: YoutubeVideo;
  activityLog: ActivityLogBusinessman;

  constructor(
    private _notification: NotificationService,
    private authService: AuthService,
    private _router: Router,
    private _youtuber: YoutuberService,
    private _youtube_data_api: YoutubeDataAPI,
    private _auth: AuthServiceL,
    private _businessman: BusinessmanService,
    private modal: NgbModal
  ) {
    this.userBusinessman = new UserBusinessman();
    this.authData = new LoginModel();
    this.adminAuthData = new LoginModel();
    this.authModel = new AuthModel();
    this.youtubeAPIkey = new YoutubeAPIKey();
    this.youtubeVideo = new YoutubeVideo();
    this.activityLog = new ActivityLogBusinessman();
  }

  ngOnInit() {
    $('.carousel').carousel({
      interval: false
    });

    storage.clearAll();
  }

  campaignCategory(campaign_category): number {
    let category_id = 0;
    switch (campaign_category) {
      case 'Comedy': category_id = 1; break;
      case 'Drama': category_id = 2; break;
      case 'Education': category_id = 3; break;
      case 'Entertainment': category_id = 4; break;
      case 'Family': category_id = 5; break;
      case 'Gaming': category_id = 6; break;
      case 'Howto and Styles': category_id = 7; break;
      case 'Music': category_id = 8; break;
      case 'Sports': category_id = 9; break;
      case 'Travel and Events': category_id = 10; break;
      case 'People and Blogs': category_id = 11; break;
    }
    return category_id;
  }

  /* Login and Sign In With Google */

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data) => {
      if (data) {
        this.youtuber = data;
        console.log(this.youtuber);
        this.authModel.token = this.youtuber.authToken;
        const googleAuthorize = this._auth.logInWithGoogle(this.authModel);
        if (googleAuthorize) {
          const authData = storage.get('authData');
          if (authData.user === 'youtuber') {
            this._youtuber.getUserYoutuber(this.youtuber.id, (response: ResponseCallbackModel) => {
              if (!response.error && response.data) {
                console.log('hear hear 1', response);
                storage.set('current_youtuber', this.youtuber);
                this._youtuber.getYoutuberInfo(response.data.youtuber_id, (responseYoutuberInfo: ResponseCallbackModel) => {
                  console.log('hear hear 2', responseYoutuberInfo);
                  if (!responseYoutuberInfo.error && responseYoutuberInfo.data == null) {
                    let modal = this.modal.open(MediakitModal, MEDIAKIT_MODAL_OPTIONS);
                    modal.result.then((data) => {

                    }, (reason) => {
                      this._router.navigate(['/youtuber-dashboard']);
                    });
                  } else {
                    this._router.navigate(['/youtuber-dashboard']);
                  }
                });
              } else {
                this._youtuber.addUserYoutuber(this.youtuber, (responseadduser: ResponseCallbackModel) => {
                  let modal = this.modal.open(MediakitModal, MEDIAKIT_MODAL_OPTIONS);
                  modal.result.then((data) => {

                  }, (reason) => {
                    this._router.navigate(['/youtuber-dashboard']);
                  });
                });
              }
            });
          }
        }
      }
    });
  }

  login() {
    this._auth.logIn(this.authData, (response: ResponseCallbackModel) => {
      if (!response.error) {
        this._router.navigate(['/dashboard']);
      }
    });
  }

  loginAdmin() {
    this._auth.logInAdmin(this.adminAuthData, (response: ResponseCallbackModel) => {
      if (!response.error) {
        this._router.navigate(['/admin']);
      }
    });
  }

  signOut(): void {
    this.authService.signOut();
  }

  openSignUp() {
    this.modal.open(RegistrationModal, REGISTRATION_OPTIONS);
  }

}


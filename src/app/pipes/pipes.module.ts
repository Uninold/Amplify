import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CapitalizePipe } from './index';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    CapitalizePipe
  ],
  declarations: [
    CapitalizePipe
  ]
})
export class PipesModule { }

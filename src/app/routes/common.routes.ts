import { Route } from '@angular/router';

import { AuthGuard } from '../guards/auth.guard';

import { CommonLayoutComponent } from '../layouts/common/common.layout';

import { LoginComponent } from '../pages/login/login.component';

// COMMON LAYOUT ROUTES
export const COMMON_ROUTES: Route = {
    path: '',
    component: CommonLayoutComponent,
    children: [
        {
            path: '',
            component: LoginComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'admin',
            loadChildren: './pages/admin/admin.module#AdminModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'dashboard',
            loadChildren: './pages/businessman/content/dashboard/dashboard.module#DashboardModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'my-projects',
            loadChildren: './pages/businessman/content/my-projects/my-projects.module#MyProjectsModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'rank',
            loadChildren: './pages/businessman/content/rank/rank.module#RankModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'reports',
            loadChildren: './pages/businessman/content/reports/reports.module#ReportsModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'home',
            loadChildren: './pages/home/home.module#HomeModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'youtuber-dashboard',
            loadChildren: './pages/home/youtuber-dashboard/youtuber-dashboard.module#YoutuberDashboardModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'youtuber-hot-campaigns',
            loadChildren: './pages/home/hot-campaigns/hot-campaigns.module#HotCampaignsModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'youtuber-my-campaigns',
            loadChildren: './pages/home/my-campaigns/my-campaigns.module#MyCampaignsModule',
            canActivate: [AuthGuard]
        },
        {
            path: 'youtuber-reports',
            loadChildren: './pages/home/reports/reports.module#YoutuberReportsModule',
            canActivate: [AuthGuard]
        }
    ]
};


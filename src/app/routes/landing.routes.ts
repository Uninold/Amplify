import { Route } from '@angular/router';

import { NoAuthGuard } from '../guards/no-auth.guard';

import { LandingLayoutComponent } from '../layouts/landing/landing.layout';

import { LoginComponent } from '../pages/login/login.component';

// LANDING LAYOUT ROUTES
export const LANDING_ROUTES: Route = {
  path: '',
  component: LandingLayoutComponent,
  children: [
    { path: '', component: LoginComponent, canActivate: [NoAuthGuard], pathMatch: 'full' }
  ]
};

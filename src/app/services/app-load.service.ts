import { Injectable } from '@angular/core';

import { UserService, AuthServiceL } from '../services';
import { ResponseCallbackModel } from '../models';

@Injectable()
export class AppLoadService {

  constructor(
    private _auth: AuthServiceL,
    private _user: UserService
  ) { }

  initApp() {
    return new Promise((resolve, reject) => {
      if (this._auth.isUserAuthorized()) {
        this._user.getCurrent((response: ResponseCallbackModel) => {
          if (!response.error) {
            resolve(true);
          } else {
            reject(true);
          }
        });
      } else {
        resolve(true);
      }

    });
  }
}

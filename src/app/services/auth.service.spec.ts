import { TestBed, inject } from '@angular/core/testing';

import { AuthServiceL } from './auth.service';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthServiceL]
    });
  });

  it('should be created', inject([AuthServiceL], (service: AuthServiceL) => {
    expect(service).toBeTruthy();
  }));
});

import * as storage from 'store';

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { ServerConfig } from '../configs/server.config';
import { ResponseHelper } from '../helpers';
import { LoginModel, AuthModel, ResponseModel } from '../models';


@Injectable()
export class  AuthServiceL {
  private _token: string;
  private _isAuthorized: boolean;
  private _authModel: AuthModel;
  user: SocialUser;
  constructor(private _http: HttpClient, private authService: AuthService) {
    this.isUserAuthorized();
    this._authModel = new AuthModel();
  }

  get token() {
    return this._token;
  }

  get isAuthorized() {
    return this._isAuthorized;
  }

  isUserAuthorized(): boolean {
    const authData = storage.get('authData');
    this._isAuthorized = !!authData;
    if (authData) {
      this._token = authData.token;
    } else {
      this._token = '';
    }
    return this._isAuthorized;
  }

  logIn(auth_data: LoginModel, callback: any = () => { }): void {
    this._http.post(`${ServerConfig.API}auth/login`, auth_data, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          if (response.error === 0) {
            this._isAuthorized = true;
            this._authModel.user = 'businessman';
            this._authModel.token = response.data.token;
            storage.set('authData', this._authModel);
          }
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('AuthService.logIn Complete')
      );
  }

  logInAdmin(auth_data: LoginModel, callback: any = () => { }): void {
    this._http.post(`${ServerConfig.API}auth/login_admin`, auth_data, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          if (response.error === 0) {
            this._isAuthorized = true;
            this._authModel.user = 'admin';
            this._authModel.token = response.data.token;
            storage.set('authData', this._authModel);
          }
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('AuthService.logIn Complete')
      );
  }

  logInWithGoogle(dataReturned: AuthModel): boolean {
    this._isAuthorized = true;
    this._authModel.user = 'youtuber';
    this._authModel.token = dataReturned.token;
    storage.set('authData', this._authModel);
    return this._isAuthorized;
  }

  logOut(callback: any = () => { }): void {
    setTimeout(() => {
      this._token = '';
      this._isAuthorized = false;
      storage.clearAll();
      callback({
        error: true,
        data: [],
        message: 'Sucessfully logged out.'
      });
    }, 200);
  }

}

import { TestBed, inject } from '@angular/core/testing';

import { BusinessmanService } from './businessman.service';

describe('BusinessmanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BusinessmanService]
    });
  });

  it('should be created', inject([BusinessmanService], (service: BusinessmanService) => {
    expect(service).toBeTruthy();
  }));
});

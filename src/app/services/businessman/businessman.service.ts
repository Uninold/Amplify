import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ResponseModel, UserBusinessman, ActivityLogBusinessman, BusinessmanRatingModel, AcceptedInterestModel } from '../../models';
import { ServerConfig } from '../../configs/server.config';
import { ResponseHelper } from '../../helpers';

@Injectable()
export class BusinessmanService {

    constructor(private _http: HttpClient) { }

    /* GET method */

    getAllBusinessman(callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}businessman/all_businessman`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('All Businessman')
            );
    }

    getCurrentBusinessman(callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}businessman/current_businessman`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Current Businessman')
            );
    }

    /* ---------- */

    /* POST method */

    addUserBusinessman(userBusinessman: UserBusinessman, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}businessman/add_businessman/`, userBusinessman, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Businessman Service. add userBusinessman')
            );
    }

    addActivity(activity_log: ActivityLogBusinessman, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}businessman/add_activity_log_businessman/`, activity_log, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Businessman Service. add activityLogBusinessman')
            );
    }

    addBusinessmanRating(businessman_rating: BusinessmanRatingModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}businessman/add_businessman_rating/`, businessman_rating, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Businessman Service. add userBusinessman')
            );
    }

    updateRatingStatus(accepted_interest_id: any, is_rated: any, callback: any = () => { }): void {
        this._http.put(`${ServerConfig.API}interest/updated_rating_status/${accepted_interest_id}`, { israted: is_rated }, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Businessman Service. update AcceptedInterest')
            );
    }

    /* ----------- */

    /* Remove Method */

    removeBusinessman(businessman_id: string, callback: any = () => { }): void {
        this._http.delete(`${ServerConfig.API}businessman/remove_businessman/${businessman_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Remove Businessman')
            );
    }

    /* ------------- */

    updateWeight(criteria_id: any, percentage: any, callback: any = () => { }): void {
        this._http.put(`${ServerConfig.API}auth/update_weight/${criteria_id}`, { criteria_percentage: +percentage }, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Businessman Service. update AcceptedInterest')
            );
    }

    updateWeightYoutube(criteria: any, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}auth/update_weight_youtube`, JSON.stringify(criteria), { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Businessman Service. update AcceptedInterest')
            );
    }

    updateWeightBusiness(criteria: any, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}auth/update_weight_business`, JSON.stringify(criteria), { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Businessman Service. update AcceptedInterest')
            );
    }

    getAllWeight(callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}auth/all_criteria/`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('All Businessman')
            );
    }
}

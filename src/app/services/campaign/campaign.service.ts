import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ResponseModel } from '../../models';
import { ServerConfig } from '../../configs/server.config';
import { ResponseHelper } from '../../helpers';
import {
    CampaignModel,
    AdvertisingDetailModel,
    CampaignPhotoModel,
    InterestedModel,
    BusinessmanCampaignModel,
    VideoModel
} from '../../models/campaign.model';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class CampaignService {

    private _businessmanCampaignData: BehaviorSubject<Array<BusinessmanCampaignModel>>;

    businessmanCampaignData: Observable<Array<BusinessmanCampaignModel>>;
    private dataStore: {
        businessmanCampaignData: Array<BusinessmanCampaignModel>
    };
    constructor(private _http: HttpClient) {
        this._setObservables();
    }
    private _setObservables(): void {
        this._businessmanCampaignData = <BehaviorSubject<Array<BusinessmanCampaignModel>>>new BehaviorSubject([]);
        this.businessmanCampaignData = this._businessmanCampaignData.asObservable();

        this.dataStore = {
            businessmanCampaignData: [new BusinessmanCampaignModel()]
        };
    }

    /* GET method */

    getCampaign(id: number, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_campaigns_businessman/${id}`, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    if (response.error === 0) {
                        this.dataStore.businessmanCampaignData = response.data;
                        this._businessmanCampaignData.next(Object.assign({}, this.dataStore).businessmanCampaignData);
                    }
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }

    getAllOnGoingCampaignOfBusinessman(businessman_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_on_going_campaigns_businessman/${businessman_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }

    getAllOnGoingProductionOfBusinessman(businessman_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_on_going_production_businessman/${businessman_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Production')
            );
    }

    getSpecificOnGoingCampaignOfBusinessmanWithAds(campaign_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/specific_on_going_campaign_businessman_with_advertisers/${campaign_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }

    getSpecificOnGoingProductionOfBusinessmanWithAds(campaign_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/specific_on_going_production_businessman_with_advertisers/${campaign_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }

    // Displays all the On Going Campaign with INTERESTED Campaign of a YouTuber.
    getAllCampaignToFeed(youtuber_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_on_going_campaigns/${youtuber_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
        }
     // Retrieve all video id submitted.
     getAllVideoId(callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/video_id`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    getPhotoCampaignToFeed(campaign_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/campaign_photo_youtuber_feed/${campaign_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }

    getCampaignDetails(campaign_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/campaign_details/${campaign_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }

    getYoutubersFromCampaign(businessman_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/interested_youtuber_campaign/${businessman_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    if (response.error === 0) {
                        this.dataStore.businessmanCampaignData = response.data;
                        this._businessmanCampaignData.next(Object.assign({}, this.dataStore).businessmanCampaignData);
                    }
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }

    getAllCampaign(youtuber_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_campaigns/${youtuber_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    // === HAROLD
    // filter hot campaign by category
    getAllCampaignToFeedByCategory(youtuber_id: string, category_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_on_going_campaigns_by_category/${youtuber_id}/${category_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    // filter hot campaign by budget
    getAllCampaignToFeedByBudget(youtuber_id: string, starting_budget: string, ending_budget: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_on_going_campaigns_by_budget/${youtuber_id}/${starting_budget}/${ending_budget}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    getAllCampaignToFeedByCategoryBudget(youtuber_id: string, category_id: string, starting_budget: string, ending_budget: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_on_going_campaigns_by_category_and_budget/${youtuber_id}/${category_id}/${starting_budget}/${ending_budget}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    // get all category in campaigns
    getAllCategoryInCampaign( callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/all_camapign_category`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }




    // === HAROLD

    /* ---------- */

    /* POST method */

    addCampaign(campaign: CampaignModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}campaign/add_campaign/`, campaign, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Campaign Service. add campaign')
            );
    }

    addCampaignPhoto(campaign_photo: CampaignPhotoModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}campaign/add_product_photo_campaign/`, campaign_photo, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Campaign Service. add campaign')
            );
    }

    addAdvertisingDetail(advertising_detail: AdvertisingDetailModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}campaign/add_advertising_detail/`, advertising_detail, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Campaign Service. add campaign')
            );
    }

    getMyCampaignsForYoutuber(youtuberId: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/youtuber_my_campaigns/${youtuberId}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    
    addVideo(video: VideoModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}campaign/youtube_video/`, video, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Campaign Service. add video')
            );
    }
    
    getVideo(accepted_interest: number, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/youtube_video/${accepted_interest}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    
    getVideoWithYoutuberInfo(youtuber_id: string, campaign_id: number, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/report_for_businessman/${campaign_id}/${youtuber_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    
    getYoutuberSubmittedVideo(youtuber_id: string, campaign_id: number, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}interest/youtuber_submitted_video/${youtuber_id}/${campaign_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }

    getRecommendedYoutuber(category_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}campaign/recommend_youtuber_for_campaign/${category_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Campaign')
            );
    }
    /* ----------- */

    /* Remove Method */

    removeCampaign(campaign_id: string, callback: any = () => { }): void {
        this._http.delete(`${ServerConfig.API}campaign/remove_campaign/${campaign_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Remove Campaign')
            );
    }

    removeCampaignPhoto(campaign_photo_id: string, callback: any = () => { }): void {
        this._http.delete(`${ServerConfig.API}campaign/remove_photo/${campaign_photo_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Remove Campaign Photo')
            );
    }

    removeAdvertisingDetail(advertising_detail_id: string, callback: any = () => { }): void {
        this._http.delete(`${ServerConfig.API}campaign/remove_advertising/${advertising_detail_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Remove Advertising Detail')
            );
    }
    /* ----------- */

    /* Update Method */

    updateCampaign(campaign_id: string, campaign: CampaignModel, callback: any = () => { }): void {
        this._http.put(`${ServerConfig.API}campaign/update_campaign/${campaign_id}`, campaign, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Update Campaign')
            );
    }

    addToOnGoingProduction(campaign_id: string, callback: any = () => { }): void {
        this._http.put(`${ServerConfig.API}campaign/add_to_on_going_production/${campaign_id}`, { campaign_status_id: 2 }, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Update Campaign')
            );
    }

    /* ------------- */
}

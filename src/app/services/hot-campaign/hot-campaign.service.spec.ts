import { TestBed, inject } from '@angular/core/testing';

import { HotCampaignService } from './hot-campaign.service';

describe('HotCampaignService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HotCampaignService]
    });
  });

  it('should be created', inject([HotCampaignService], (service: HotCampaignService) => {
    expect(service).toBeTruthy();
  }));
});

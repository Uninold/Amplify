import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ResponseModel } from '../../models';
import { ServerConfig } from '../../configs/server.config';
import { ResponseHelper } from '../../helpers';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class HotCampaignService {

    constructor(private _http: HttpClient) {
    }
    
    /* GET method */

    displayHotCampaigns(youtuber_id: any, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}youtuber/display_hot_campaigns/${youtuber_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Hot Campaign')
            );
    }

    /* ---------- */
    
}

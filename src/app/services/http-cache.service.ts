import * as storage from 'store';

import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

abstract class HttpCache {
  abstract get(req: HttpRequest<any>): HttpResponse<any> | null;
  abstract put(req: HttpRequest<any>, resp: HttpResponse<any>): void;
}

@Injectable()
export class HttpCacheService implements HttpCache {

  put(req: HttpRequest<any>, resp: HttpResponse<any>): void {
    storage.set(req.urlWithParams, resp);
  }

  get(req: HttpRequest<any>): HttpResponse<any> | null {
    return storage.get(req.urlWithParams);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ResponseModel } from '../../models';
import { ServerConfig } from '../../configs/server.config';
import { ResponseHelper } from '../../helpers';
import {
    CampaignModel,
    AdvertisingDetailModel,
    CampaignPhotoModel,
    InterestedModel,
    BusinessmanCampaignModel } from '../../models/campaign.model';
import { AcceptedInterestModel } from '../../models/interest.model';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class InterestService {

    constructor(private _http: HttpClient) {

     }

    /* GET method */



    /* ---------- */

    /* POST method */

    interested(interestedDetail: InterestedModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}campaign/interested/`, interestedDetail, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Campaign Service. add campaign')
            );
    }

    acceptedInterested(accepted: AcceptedInterestModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}campaign/accept_interest/`, accepted, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Campaign Service. add campaign')
            );
    }

    removeInterestedYoutuber(removedInterested: AcceptedInterestModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}campaign/remove_interested/`, removedInterested, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Campaign Service. add campaign')
            );
    }

    /* ----------- */

    /* Put Method */

    updateAcceptedInterest(youtuber_id: any, campaign_id: any, submission_status: any, with_vid: any ,callback: any = () => { }): void {
        this._http.put(`${ServerConfig.API}interest/updated_accepted_interest/${youtuber_id}/${campaign_id}`, {
            submission_status_id: submission_status,
            with_video: with_vid
        } , { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Update Accepted Interest')
            );
    }

    /* ---------- */
}

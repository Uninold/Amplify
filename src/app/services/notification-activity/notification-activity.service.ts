import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ResponseModel } from '../../models';
import { ServerConfig } from '../../configs/server.config';
import { ResponseHelper } from '../../helpers';
import {
    NotificationModel
       } from '../../models/notification.model';
import { AcceptedInterestModel } from '../../models/interest.model';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class NotificationActivityService {

    constructor(private _http: HttpClient) {
     }

    /* GET method */



    /* ---------- */

    /* POST method */

    notification(notificationModel: NotificationModel, callback: any = () => { }): void {
        this._http.post(`${ServerConfig.API}notification/add_notification/`, notificationModel, { observe: 'response' })
            .subscribe(
                data => {
                    const response = <ResponseModel>data.body;
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Campaign Service. add campaign')
            );
    }

    /* GET methid */

    getNotification(user_id: string, callback: any = () => { }): void {
        this._http.get(`${ServerConfig.API}notification/notification/${user_id}`, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Get Notification')
            );
    }

    updateNotification(notif_id: string, callback: any = () => { }): void {
        this._http.put(`${ServerConfig.API}notification/update_campaign/${notif_id}`, { seen: 1 }, { observe: 'response' })
            .subscribe(
                data => {
                    ResponseHelper.Format(data, callback);
                },
                err => ResponseHelper.Format(err, callback),
                () => ResponseHelper.Log('Update Notifcation')
            );
    }
}

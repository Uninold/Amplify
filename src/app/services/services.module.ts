import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { HttpCacheService, AuthServiceL, UserService, NotificationService } from './index';
import { YoutuberService } from './youtuber';
import { BusinessmanService } from './businessman';
import { CampaignService } from './campaign';
import { YoutubeDataAPI } from './youtube_data_api';
import { InterestService } from './interest';
import { HotCampaignService } from './hot-campaign';
import { NotificationActivityService } from './notification-activity';
@NgModule({
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true,
      maxOpened: 1,
      autoDismiss: true
    })
  ],
  declarations: [],
  providers: [
    HttpCacheService,
    AuthServiceL,
    UserService,
    NotificationService,
    YoutuberService,
    BusinessmanService,
    CampaignService,
    HotCampaignService,
    InterestService,
    NotificationActivityService,
    YoutubeDataAPI
  ]
})
export class ServicesModule { }

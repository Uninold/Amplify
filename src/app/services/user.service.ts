import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ServerConfig } from '../configs/server.config';
import { ResponseHelper } from '../helpers';
import { UserModel, ResponseModel } from '../models';

@Injectable()
export class UserService {

  private _currentUser: BehaviorSubject<UserModel>;
  currentUser: Observable<UserModel>;

  private dataStore: {
    currentUser: UserModel
  };

  constructor(
    private _http: HttpClient
  ) {
    this._setObservables();
  }

  private _setObservables(): void {
    this._currentUser = <BehaviorSubject<UserModel>>new BehaviorSubject({});
    this.currentUser = this._currentUser.asObservable();

    this.dataStore = {
      currentUser: new UserModel()
    };
  }

  getCurrent(callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}businessman/current_businessman`, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          if (response.error === 0) {
            this.dataStore.currentUser = response.data.user;
            this._currentUser.next(Object.assign({}, this.dataStore).currentUser);
          }
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('UserService.getCurrent Complete')
      );
  }

  getuserlist(usergroupid: any, callback: any): any {
    this._http.get(`${ServerConfig.API}user/lists/${usergroupid}`, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          if (response.error === 0) {
            // this.dataStore.currentUser = response.data[0].user;
            // this._currentUser.next(Object.assign({}, this.dataStore).currentUser);
          }
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('UserService.getCurrent Complete')
      );
  }

}

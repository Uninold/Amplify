import { TestBed, inject } from '@angular/core/testing';

import { YoutubeDataAPI } from './youtube_data_api.service';

describe('YoutubeDataAPI', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YoutubeDataAPI]
    });
  });

  it('should be created', inject([YoutubeDataAPI], (service: YoutubeDataAPI) => {
    expect(service).toBeTruthy();
  }));
});

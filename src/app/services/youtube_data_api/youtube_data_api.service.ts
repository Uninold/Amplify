import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { ResponseModel, UserYoutuber, YoutubeAPIKey, YoutubeVideo, YoutuberVideoData } from '../../models';
import { ServerConfig } from '../../configs/server.config';
import { ResponseHelper } from '../../helpers';
@Injectable()
export class YoutubeDataAPI {

  youtube_api_key: YoutubeAPIKey;

  constructor(private _http: HttpClient) {
    this.youtube_api_key = new YoutubeAPIKey();
  }

  // Sync video ---

  getAllDataPerCategoryNoPage(category_id: any, callback: any = () => { }): void {
    const headers = new HttpHeaders({
      'noauth': 'true'
    });
    // tslint:disable-next-line:max-line-length
    this._http.get(`${ServerConfig.YoutubeDataAPI}search?part=snippet&type=video&maxResults=50&videoCategoryId=${category_id}&key=${this.youtube_api_key.api_key}`,
      { headers, observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtube Category Search. get Category')
      );
  }

  getAllDataPerCategoryWithPage(category_id: any, nextPageToken: string, callback: any = () => { }): void {
    const headers = new HttpHeaders({
      'noauth': 'true'
    });
    // tslint:disable-next-line:max-line-length
    this._http.get(`${ServerConfig.YoutubeDataAPI}search?part=snippet&type=video&maxResults=50&pageToken=${nextPageToken}&videoCategoryId=${category_id}&key=${this.youtube_api_key.api_key}`,
      { headers, observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtube Category Search. get Category')
      );
  }

  syncAllDataPerCategory(youtubeVideo: YoutubeVideo, callback: any = () => { }): void {
    this._http.post(`${ServerConfig.API}youtuber/add_sync_youtube_data/`, youtubeVideo, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          ResponseHelper.Format(data, callback);
        },
        err => {
          console.log(youtubeVideo);
          ResponseHelper.Format(err, callback);
        },
        () => ResponseHelper.Log('Youtuber Service. add userYoutuber')
      );
  }

  getYoutuberSubs(channelid: string, callback: any = () => { }): void {
    const headers = new HttpHeaders({
      'noauth': 'true'
    });
    this._http.get(`${ServerConfig.YoutubeDataAPI}channels?part=id%2Csnippet%2Cstatistics` +
      `%2CcontentDetails%2CtopicDetails&id=${channelid}&key=${this.youtube_api_key.api_key}`,
      { headers, observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log(channelid)
      );
  }

  // ----

  // YouTuber Profile ----

  getVideo(videoID: string, callback: any = () => { }): void {
    const headers = new HttpHeaders({
      'noauth': 'true'
    });
    // tslint:disable-next-line:max-line-length
    this._http.get(`${ServerConfig.YoutubeDataAPI}videos?part=snippet%2Cstatistics&id=${videoID}&key=${this.youtube_api_key.api_key}`,
      { headers, observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => {
          console.log(videoID);
          ResponseHelper.Format(err, callback);
        },
        () => ResponseHelper.Log('Youtube Video. get Video')
      );
  }

  getYoutuberChannel(channel_id: string, callback: any = () => { }): void {
    const headers = new HttpHeaders({
      'noauth': 'true'
    });
    this._http.get(`${ServerConfig.YoutubeDataAPI}channels?part=statistics&id=${channel_id}&key=${this.youtube_api_key.api_key}`,
      { headers, observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Channel. get Channel')
      );
  }

  getAllVideoYoutuberNoPageToken(channel_id: string, callback: any = () => { }): void {
    const headers = new HttpHeaders({
      'noauth': 'true'
    });
    this._http.get(`${ServerConfig.YoutubeDataAPI}search?part=snippet&regionCode=PH&channelId=${channel_id}&maxResults=50&type=video&key=${this.youtube_api_key.api_key}`,
      { headers, observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback)
      );
  };

  getAllVideoYoutuberWithPageToken(channel_id: string, nextPageToken: string, callback: any = () => { }): void {
    const headers = new HttpHeaders({
      'noauth': 'true'
    });
    this._http.get(`${ServerConfig.YoutubeDataAPI}search?part=snippet&regionCode=PH&channelId=${channel_id}&maxResults=50&pageToken=${nextPageToken}&type=video&key=${this.youtube_api_key.api_key}`,
      { headers, observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback)
      );
  };

  youtubeChannelExistence(youtube_channel: string, callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}youtuber/youtuber_channel_existence/${youtube_channel}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuber Channel Existence')
      );
  }

  getVideoExisted(video_id: string, callback: any = () => { }) {
    this._http.get(`${ServerConfig.API}youtuber/youtube_data_video_existed/${video_id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuber report details')
      );
  }

  // ----

  // POST Method

  addYoutuberVideoData(youtuber_video_data: YoutuberVideoData, callback: any = () => { }): void {
    this._http.post(`${ServerConfig.API}youtuber/add_youtuber_video/`, youtuber_video_data, { observe: 'response' })
        .subscribe(
            data => {
                const response = <ResponseModel>data.body;
                ResponseHelper.Format(data, callback);
            },
            err => ResponseHelper.Format(err, callback),
            () => ResponseHelper.Log('Youtuber Video Data. add youtuber video data')
        );
  }

  // ----

  /* Remove Method */

  removeYoutubeData(callback: any = () => { }): void {
    this._http.delete(`${ServerConfig.API}youtuber/remove_all_video_youtube_data/`, { observe: 'response' })
        .subscribe(
            data => {
                ResponseHelper.Format(data, callback);
            },
            err => ResponseHelper.Format(err, callback),
            () => ResponseHelper.Log('Remove Campaign')
        );
  }

  /* ------------ */

  /* Update Method */

  updateVideo(video_id, video: any, callback: any = () => { }): void {
    this._http.put(`${ServerConfig.API}youtuber/update_youtuber_video/${video_id}`, video, { observe: 'response' })
        .subscribe(
            data => {
                ResponseHelper.Format(data, callback);
            },
            err => ResponseHelper.Format(err, callback),
            () => ResponseHelper.Log('Update Video')
        );
  }

  /* ------------- */

}

import { TestBed, inject } from '@angular/core/testing';

import { YoutuberService } from './youtuber.service';

describe('YoutuberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YoutuberService]
    });
  });

  it('should be created', inject([YoutuberService], (service: YoutuberService) => {
    expect(service).toBeTruthy();
  }));
});

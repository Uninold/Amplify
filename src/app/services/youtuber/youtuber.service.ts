import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { ResponseModel, UserYoutuber, YoutuberMediakit, YoutuberMediakitInterest, YoutubeVideoStatistics, YoutuberSearch } from '../../models';
import { ServerConfig } from '../../configs/server.config';
import { ResponseHelper } from '../../helpers';
@Injectable()
export class YoutuberService {

  constructor(private _http: HttpClient) { }

  /* GET method */

  getAllYoutuber(callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}youtuber/all_youtuber/`, { observe: 'response' })
    .subscribe(
        data => {
            ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('All YouTuber')
    );
  }

  getUserYoutuber(id: string, callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}youtuber/youtuber/${id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get userYoutuber')
      );
  }

  getYoutuberInfo(id: string, callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}mediakit/mediakit/${id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuberInfo')
      );
  }

  getYoutuberInfoById(id: string, callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}youtuber/youtuber_channel/${id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuber Channel')
      );
  }

  getYoutuberProfile(id: string, callback: any = () => { }) {
    this._http.get(`${ServerConfig.API}youtuber/youtuber_profile/${id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuber Profile')
      );
  }

  getYoutuberReportList(youtuber_id: string, callback: any = () => { }) {
    this._http.get(`${ServerConfig.API}youtuber/youtuber_report_list/${youtuber_id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuber report list')
      );
  }

  getYoutuberReportDetails(accepted_interest_id: any, callback: any = () => { }) {
    this._http.get(`${ServerConfig.API}youtuber/youtuber_report_details/${accepted_interest_id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuber report details')
      );
  }

  getVideoExisted(video_id: string, callback: any = () => { }) {
    this._http.get(`${ServerConfig.API}youtuber/youtuber_video_existed/${video_id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuber report details')
      );
  }

  // === HAROLD

  getallYoutuberInfo(callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}youtuber/all_youtubers_info`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuberInfo')
      );
  }
  getTopInfluencer(callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}youtuber/top_influencer`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get youtuberInfo')
      );
  }

  // === HAROLD


  /* ---------- */

  /* POST method */

  addUserYoutuber(userYoutuber: UserYoutuber, callback: any = () => { }): void {
    this._http.post(`${ServerConfig.API}youtuber/add_youtuber/`, userYoutuber, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. add userYoutuber')
      );
  }

  addYoutuberMediakit(youtuberInfo: YoutuberMediakit, callback: any = () => { }): void {
    this._http.post(`${ServerConfig.API}mediakit/add_mediakit/`, youtuberInfo, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. add youtuber mediakit')
      );
  }

  searchYoutuberMediakit(search: string, callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}mediakit/search_mediakit/${search}`, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. add userYoutuber')
      );
  }

  addYoutuberMediakitInterest(youtuberMediakitInterest: YoutuberMediakitInterest, callback: any = () => { }): void {
    this._http.post(`${ServerConfig.API}mediakit/add_mediakit_interest/`, youtuberMediakitInterest, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. add youtuber mediakit interest')
      );
  }
  addYoutubeVideoStatistics(youtubeVideoStats: YoutubeVideoStatistics, callback: any = () => { }): void {
    this._http.post(`${ServerConfig.API}youtuber/add_youtube_data/`, youtubeVideoStats, { observe: 'response' })
      .subscribe(
        data => {
          const response = <ResponseModel>data.body;
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. add video statistics')
      );
  }
//  for referal
  getReferAllYoutuber(id: string, callback: any = () => { }): void {
    this._http.get(`${ServerConfig.API}mediakit/all_mediakit/${id}`, { observe: 'response' })
      .subscribe(
        data => {
          ResponseHelper.Format(data, callback);
        },
        err => ResponseHelper.Format(err, callback),
        () => ResponseHelper.Log('Youtuber Service. get All youtuber')
      );
  }
  /* ---------- */

  /* Remove Method */

  removeYoutuber(youtuber_id: string, callback: any = () => { }): void {
    this._http.delete(`${ServerConfig.API}youtuber/remove_youtuber/${youtuber_id}`, { observe: 'response' })
        .subscribe(
            data => {
                ResponseHelper.Format(data, callback);
            },
            err => ResponseHelper.Format(err, callback),
            () => ResponseHelper.Log('Remove Youtuber')
        );
  }

  /* ---------- */
}

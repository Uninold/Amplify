//jQuery(function($){
//=== function to check if element existed
jQuery.fn.exists = function() {
  return this.length > 0;
};
jQuery.fn.existNot = function() {
  return this.length == 0;
};

function onResizeWindow() {
  //
}
var scCommonJS = {
  themeInit: function() {
    this.setMenu();
    this.slideWizard();
    this.setLayout2Col();
    this.enableTooltip();
    this.enabledMaterialDesign();
  },
  setMenu: function() {
    $("body").on("click", ".sidebar-nav .side-menu li a", function() {
      if (
        $(this)
          .parent()
          .hasClass(".sub-menu")
      ) {
        $(this)
          .parent()
          .siblings()
          .removeClass("active");
      } else {
        $(this)
          .parent()
          .siblings()
          .removeClass("active");
      }
    });
  },
  slideWizard: function() {
    //console.log('slideWizard is called');
    $("body").on("shown.bs.modal", ".slideWizardModal,.modal ", function() {
      if ($(".slick-initialized").exists()) {
        //refresh slider
        $(".slideWizardContainer").slick("setPosition");
      } else {
        $(".slideWizardContainer").slick({
          dots: true,
          prevArrow: ".btn-slider-prev",
          nextArrow: ".btn-slider-next",
          appendDots: ".slider-dots-wrap",
          speed: 300,
          infinite: false,
          slidesToShow: 1,
          adaptiveHeight: true
        });
      }
      //console.log('modal is called');
    });
    if ($(".slick-initialized").exists()) {
      $(".slideWizardContainer").slick({
        dots: true,
        prevArrow: ".btn-slider-prev",
        nextArrow: ".btn-slider-next",
        appendDots: ".slider-dots-wrap",
        speed: 300,
        infinite: false,
        slidesToShow: 1,
        adaptiveHeight: true
      });
    }
  },
  initDataTable: function() {
    if ($.fn.dataTable.isDataTable("#patientDataTable")) {
      $("#patientDataTable").dataTable({
        paging: false,
        searching: false
      });
    }
    $("#roomsListingTable").DataTable();
  },
  setLayout2Col: function() {
    $("body").on(
      "click",
      ".layout-2-col>.layout-left-panel>.nav-tabs>.nav-item",
      function() {
        $("body").addClass("side-panel-is-open");
        console.log("side-panel-is-open");
      }
    );
    $("body").on("click", ".layout-2-col .close-side-panel-btn", function() {
      $("body").removeClass("side-panel-is-open");
      console.log("side-panel-is-closed");
    });
  },
  enableTooltip: function() {
    $('[data-toggle="tooltip"]').tooltip();
  },
  enabledMaterialDesign: function() {
    var el = $(
      ".form-material input:not([type=checkbox]):not([type=radio]), .form-material textarea"
    );
    if (!el.length) return;

    el.each(function() {
      var $this = $(this),
        self = this;

      var hasValueFunction = function() {
        if (self.value.length > 0) {
          self.parentNode.classList.add("input-has-value");
          $(self)
            .closest(".form-group")
            .addClass("input-has-value");
        } else if (
          $this.has(".form-control:-webkit-autofill") &&
          $this.val() == ""
        ) {
          self.parentNode.classList.add("input-has-value autofill");
          $(self)
            .closest(".form-group")
            .addClass("input-has-value autofill");
        } else {
          self.parentNode.classList.remove("input-has-value");
          $(self)
            .closest(".form-group")
            .removeClass("input-has-value");
        }
      };

      hasValueFunction(this);
      $this.on("change keydown paste input focus", hasValueFunction);

      $this.focusin(function() {
        this.parentNode.classList.add("input-focused");
        $this.closest(".form-group").addClass("input-focused");
      });
      $this.focusout(function() {
        this.parentNode.classList.remove("input-focused");
        $this.closest(".form-group").removeClass("input-focused");
      });
      $this.find(".remove-focus").on("click", function() {
        $this.emit("focusout");
      });
    });
  }
};

$(document).ready(function() {
  //
}); /* END: document.ready */

$(window).on("load", function() {
  setTimeout(function() {
    scCommonJS.themeInit();
    scCommonJS.slideWizard();
    scCommonJS.initDataTable();
  }, 500);
}); /* END: document.load */

$(window).on("resize", function() {
  onResizeWindow();
}); /* END: document.resize */

/*=========================
	MODAL - MULTIPLE OVERLAY
	===========================*/

$(document).on(
  {
    "show.bs.modal": function() {
      var zIndex = 1040 + 10 * $(".modal:visible").length;
      $(this).css("z-index", zIndex);
      $(this)
        .find(".modal-dialog")
        .css("z-index", zIndex - 1);
      setTimeout(function() {
        $(".modal-backdrop")
          .not(".modal-stack")
          .css("z-index", zIndex - 2)
          .addClass("modal-stack");
        //enableFullcalendar();
      }, 10);
    },
    "hidden.bs.modal": function() {
      var this_ = $(this);
      if ($(".modal:visible").length > 0) {
        //restore proper scrolling
        setTimeout(function() {
          $(document.body).addClass("modal-open");
          this_.find(".modal-stack").css({ width: "100%" });
        }, 0);
      }
    }
  },
  ".modal"
);

//});
